# Coin List

[Demo](https://frosty-jones-0b7b3d.netlify.app/)

This project was bootstrapped with [cra-template-quickstart-redux](https://cra-template-quickstart-redux.netlify.app/)

App is a table where up to ten cryptocurrency prices can be tracked at the same time but initially, only five will appear.
The user can add the remaining five through a drop-down, at which point they would get removed from the drop-down.
The user also able to remove individual cryptocurrencies from the table through remove buttons next to the currency until there is only one remaining, of course doing so will add them back to the drop-down.
Each row in the table displays the following information: CMC rank, symbol and price in USD.

The goal here is to create an application that not only functions well, but is also maintainable. Of course as much as it possible for several hours.

In real-life application, it will take much more longer to plan ahead of the architecture. And for real-life application which is going to grow, I will probably choose different stack (react-query, typescript).

Except that it can be done a lot of improvement like: for mobile view it's better to use dialog selector rather than drop-down selector and so on.

# Quickstart Redux Create React App template

Opinionated quickstart [Create React App](https://github.com/facebook/create-react-app) (CRA) template with Redux, React Testing Library, eslint and stylelint configurations.

See [full documentation](https://cra-template-quickstart-redux.netlify.app).

Original Create React App README available [here](./README_CRA.md)
