/* eslint-disable fp/no-mutation */
/* eslint-disable react/prop-types */
import React, {useRef, useEffect} from 'react';
import {gsap} from 'gsap';

// just for fun
// https://www.freecodecamp.org/news/how-to-create-a-reusable-fade-in-animation-component-in-react-with-gsap/
// of course there more efficient technics e.g http://joshwcomeau.github.io/react-flip-move/examples/#/shuffle?_k=ul3vcg
// but lets use this simple one for now
//
const FadeInAnimation = ({
  children,
  wrapperElement = 'div',
  delay = 0,
  duration = 2,
  ...props
}) => {
  const Component = wrapperElement;
  const compRef = useRef(null);
  useEffect(() => {
    gsap.from(compRef.current, duration, {
      opacity: 0,
      delay,
    });
  }, [compRef, duration, delay]);
  return (
    <Component ref={compRef} {...props}>
      {children}
    </Component>
  );
};

export default FadeInAnimation;
