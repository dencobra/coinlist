import React from 'react';
import ReactDOM from 'react-dom';
import MainAppWrapper from './MainAppWrapper';
import App from './components/App';
import './index.css';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <MainAppWrapper>
    <App />
  </MainAppWrapper>,
  document.getElementById('root')
);

/**
 * If you want your app to work offline and load faster,
 * you can change unregister() to register() below.
 * Note this comes with some pitfalls.
 * @see https://bit.ly/CRA-PWA
 */
serviceWorker.unregister();
