/* eslint-disable no-console */
/* eslint-disable no-plusplus */
/// /////////////////////////////////////////////////////////////////////////////

const SYMBOLS = [
  'AAPL',
  'AMZN',
  'MONY',
  'PETS',
  'PZZA',
  'SHOP',
  'TSLA',
  'WIFI',
];

const SUCCESS_RATE = 0.2;

/// /////////////////////////////////////////////////////////////////////////////

const readJSON = () => {
  // Simulate success rate
  if (Math.random() > SUCCESS_RATE) {
    // Call was successful
    return Promise.resolve({
      version: 1,
      payload: {
        AAPL: 1588773600,
        AMZN: 1588690800,
        MONY: 1588428000,
        PETS: 1588532400,
        SHOP: 1588525200,
        TSLA: 1588608000,
      },
    });
  }
  return Promise.reject(new Error('the file could not be opened for reading'));
};

/// /////////////////////////////////////////////////////////////////////////////
const loadState = async () =>
  // Read the previous state
  readJSON()
    .then(state => {
      // If state is compliant
      if (state.version !== 1) {
        console.warn('version mismatch with previous state, rebuilding...');
        return {};
      }

      // > It was sorting here. Sorting is unnecessary because the order of the elements cannot be guaranteed in js object.
      // > Except that, I can see that this sorting is only to later comparison via JSON.stringify
      // > so it's definately redundant

      // Populate primary queue data
      return Object.fromEntries(
        SYMBOLS.map(symbol => [
          symbol,
          symbol in state.payload ? state.payload[symbol] : 0,
        ])
      );
    })
    .catch(error => {
      console.error('failed to load previous state, rebuilding...\n', error);
      return {};
    });
/// /////////////////////////////////////////////////////////////////////////////

/**
 *  Only 1st level of properties.
 *  Not a global function -> specialy for that specific case.
 *  Expecting a anb to be flat, defined object with key: value ->
 *  [key, number].
 *
 *  If it's possible maybe better
 *  to connect lodash (_.isEqual)
 *
 *  @returns true if a equals b
 */
const isEqualObjects = (a, b) => {
  if ((a && !b) || (!a && b)) {
    return false;
  }

  const keysA = Object.keys(a);
  const keysB = Object.keys(b);
  if (keysA.length !== keysB.length) {
    return false;
  }

  // eslint-disable-next-line fp/no-mutation
  for (let i = 0; i < keysA.length; i++) {
    const key = keysA[i];
    if (a[key] !== b[key]) {
      return false;
    }
  }
  return true;
};

const successResopnse = {
  PZZA: 0,
  WIFI: 0,
  MONY: 1588428000,
  SHOP: 1588525200,
  PETS: 1588532400,
  TSLA: 1588608000,
  AMZN: 1588690800,
  AAPL: 1588773600,
};

/* 
We don't need it anymore:
const failureResopnse = {
  AAPL: 0,
  AMZN: 0,
  MONY: 0,
  PETS: 0,
  PZZA: 0,
  SHOP: 0,
  TSLA: 0,
  WIFI: 0,
}; 
 const isFailure = a => isEqualObjects(a, failureResopnse);
*/

const isSuccess = a => isEqualObjects(a, successResopnse);

const functionExample = async () => {
  const state = await loadState();

  // Perform a poor-mans comparison on result
  if (!isSuccess(state)) throw new Error('unrecognized result');
};

functionExample();
