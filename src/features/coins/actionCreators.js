import DataProvider from 'providers/DataProvider';
import {useCallback} from 'react';
import {useDispatch} from 'react-redux';
import {GET_COINS_MAP, GET_COIN_QUOTES} from './actionTypes';

const useActions = () => {
  const dispatch = useDispatch();
  const getCoins = useCallback(
    () =>
      dispatch({
        type: GET_COINS_MAP,
        payload: DataProvider.map(),
      }),
    [dispatch]
  );
  const getCoinQuotes = useCallback(
    id =>
      dispatch({
        type: GET_COIN_QUOTES,
        meta: id,
        payload: DataProvider.quotes(id),
      }),
    [dispatch]
  );
  return {getCoins, getCoinQuotes};
};

export default useActions;
