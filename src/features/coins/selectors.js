import {useSelector} from 'react-redux';

export const useCoins = () => useSelector(state => state.coins);
export const useCoin = id => useSelector(state => state.coins.coins?.[id]);
