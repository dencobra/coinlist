import coinWrapper from 'features/helper';
import {GET_COINS_MAP} from './actionTypes';

const initialState = {
  coins: undefined,
  isLoading: false,
  hasError: false,
  isFulfilled: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case `${GET_COINS_MAP}_PENDING`:
      return {
        isFulfilled: false,
        isLoading: true,
        hasError: false,
        coins: undefined,
      };

    case `${GET_COINS_MAP}_FULFILLED`:
      return {
        isFulfilled: true,
        isLoading: false,
        hasError: false,
        coins: action.payload.data
          ? action.payload.data.data.map(coin => coinWrapper(coin))
          : [], // save only what we will use
      };

    case `${GET_COINS_MAP}_REJECTED`:
      return {
        isFulfilled: false,
        isLoading: false,
        hasError: true,
        coins: undefined,
      };

    default:
      return state;
  }
};

export default reducer;
