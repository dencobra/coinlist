import React from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {renderHook} from '@testing-library/react-hooks';
import {useCoins} from './selectors';

describe('features > coins > getCoins', () => {
  const mockStore = configureStore([]);
  const coins = {
    isFulfilled: true,
    isLoading: false,
    hasError: false,
    coins: [
      {
        id: 1,
        name: 'Bitcoin',
        symbol: 'BTC',
      },
      {
        id: 2,
        name: 'Litecoin',
        symbol: 'LTC',
      },
    ],
  };

  const store = mockStore({
    coins,
  });

  it('returns coin value', () => {
    /**
     * Render hook, using testing-library utility
     * @see https://react-hooks-testing-library.com/reference/api#renderhook
     */
    const {result} = renderHook(() => useCoins(), {
      wrapper: ({children}) => <Provider store={store}>{children}</Provider>,
    });

    expect(result.current.coins).toEqual(coins.coins);
  });
});
