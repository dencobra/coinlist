import {GET_COINS_MAP} from './actionTypes';
import CoinsReducer from './CoinsReducer';

describe('features > coins > CoinsReducer', () => {
  it('returns initial state, if non matched action is dispatched', () => {
    const initialState = {
      isLoading: false,
      hasError: false,
      isFulfilled: false,
    };

    const action = {
      type: 'FOO',
    };

    expect(CoinsReducer(initialState, action)).toBe(initialState);
  });

  /**
   * Provide table of values to run test case against
   * @see https://jestjs.io/docs/en/api#testeachtablename-fn-timeout
   */
  it.each([
    [`${GET_COINS_MAP}_FULFILLED`],
    [`${GET_COINS_MAP}_PENDING`],
    [`${GET_COINS_MAP}_REJECTED`],
  ])(`updates state according to dispatched action`, actionType => {
    const initialState = {
      value: 0,
    };

    const data = {
      coins: undefined,
      isLoading: false,
      hasError: true,
      isFulfilled: false,
    };

    const payload =
      actionType === `${GET_COINS_MAP}_FULFILLED`
        ? {
            data,
          }
        : undefined;

    const action = {
      type: actionType,
      payload,
    };

    expect(CoinsReducer(initialState, action)).toMatchSnapshot();
  });
});
