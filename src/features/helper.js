import getCurrentCurrency from 'helper';

// quick and simple replacement fro redux-ORM
const coinWrapper = coin => {
  // eslint-disable-next-line camelcase
  const {id, name, symbol, cmc_rank, quote} = coin;
  const price = quote?.[getCurrentCurrency()].price;
  return {id, name, symbol, cmc_rank, price};
};

export default coinWrapper;
