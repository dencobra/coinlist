import {useDispatch} from 'react-redux';
import {SELECTED_COINS_ADD, SELECTED_COINS_REMOVE} from './actionTypes';

const useSelectedCoinsActions = () => {
  const dispatch = useDispatch();

  const addSelectedCoin = coin => {
    dispatch({
      type: SELECTED_COINS_ADD,
      payload: coin,
    });
  };
  const removeSelectedCoin = coinID =>
    dispatch({
      type: SELECTED_COINS_REMOVE,
      payload: coinID,
    });
  return {addSelectedCoin, removeSelectedCoin};
};

export default useSelectedCoinsActions;
