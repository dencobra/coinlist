import {GET_COIN_QUOTES} from 'features/coins/actionTypes';
import coinWrapper from 'features/helper';
import {SELECTED_COINS_ADD, SELECTED_COINS_REMOVE} from './actionTypes';

export const MAX_SELECTED_COINS = 10;
export const MIN_SELECTED_COINS = 1;

const initialState = {
  selectedCoins: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SELECTED_COINS_ADD:
      return {
        ...state,
        selectedCoins:
          state.selectedCoins.length === MAX_SELECTED_COINS
            ? state.selectedCoins
            : [...state.selectedCoins, ...[action.payload]],
      };
    case SELECTED_COINS_REMOVE:
      return {
        ...state,
        selectedCoins:
          state.selectedCoins.length === MIN_SELECTED_COINS
            ? state.selectedCoins
            : state.selectedCoins.filter(coin => coin.id !== action.payload),
      };

    case `${GET_COIN_QUOTES}_FULFILLED`:
      return {
        ...state,
        selectedCoins: state.selectedCoins.map(coin =>
          coin.id === action.meta
            ? {...coin, ...coinWrapper(action.payload.data.data[action.meta])}
            : coin
        ),
      };
    default:
      return state;
  }
};

export default reducer;
