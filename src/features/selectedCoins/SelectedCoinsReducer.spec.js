/* eslint-disable fp/no-mutation */
import {SELECTED_COINS_ADD, SELECTED_COINS_REMOVE} from './actionTypes';
import reducer from './SelectedCoinsReducer';

describe('features > selectedCoins > selectedCoinsReducer', () => {
  it('should be empty in initial state', () => {
    expect(reducer(undefined, {}).selectedCoins).toEqual([]);
  });

  it('should handle flow', () => {
    let r = reducer(undefined, {
      type: SELECTED_COINS_ADD,
      payload: {
        id: 1,
        symbol: 'BTC',
      },
    });
    expect(r.selectedCoins.length).toEqual(1);
    r = reducer(r, {
      type: SELECTED_COINS_ADD,
      payload: {
        id: 2,
        symbol: 'LTC',
      },
    });
    expect(r.selectedCoins.length).toEqual(2);
    expect(r.selectedCoins[1].symbol).toEqual('LTC');
    r = reducer(r, {
      type: SELECTED_COINS_REMOVE,
      payload: 1,
    });
    expect(r.selectedCoins.length).toEqual(1);
    expect(r.selectedCoins[0].symbol).toEqual('LTC');

    // removing of non existent item
    r = reducer(r, {
      type: SELECTED_COINS_REMOVE,
      payload: 1,
    });

    expect(r.selectedCoins.length).toEqual(1);
    expect(r.selectedCoins[0].symbol).toEqual('LTC');

    // Removing last element
    r = reducer(r, {
      type: SELECTED_COINS_REMOVE,
      payload: 2,
    });

    expect(r.selectedCoins.length).toEqual(1);
    expect(r.selectedCoins[0].symbol).toEqual('LTC');

    // test MAX_COINS
    // eslint-disable-next-line no-plusplus
    for (let index = 3; index < 15; index++) {
      r = reducer(r, {
        type: SELECTED_COINS_ADD,
        payload: {
          id: index,
          symbol: `LTC ${index}`,
        },
      });
    }
    expect(r.selectedCoins.length).toEqual(10);
  });
});
