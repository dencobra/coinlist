import {combineReducers, createStore, applyMiddleware, compose} from 'redux';
import promise from 'redux-promise-middleware';
import {CoinsReducer} from 'features/coins';
import {SelectedCoinsReducer} from 'features/selectedCoins';
import {persistStore, persistReducer} from 'redux-persist';

import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import React from 'react';

/**
 * Create root reducer, containing
 * all features of the application
 */
const rootReducer = combineReducers({
  coins: CoinsReducer,
  selectedCoins: SelectedCoinsReducer,
});

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

/**
 * Initialize Redux Dev Tools,
 * if they are installed in browser.
 */
/* eslint-disable no-underscore-dangle */
/** Use Redux compose, if browser doesn't have Redux devtools */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/* eslint-enable */

/** Create Redux store with root reducer and middleware included */
export const store = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(promise))
);

const persistor = persistStore(store);

// eslint-disable-next-line react/prop-types
const MainAppWrapper = ({children}) => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      {children}
    </PersistGate>
  </Provider>
);

export default MainAppWrapper;
