import CoinList from 'components/CoinList/CoinList';
import React from 'react';
import classes from './App.module.css';

const App = () => (
  <div className={classes.container}>
    <CoinList />
  </div>
);

export default App;
