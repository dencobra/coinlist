import PropTypes from 'prop-types';
import React from 'react';
import AddMoreCoins from './AddMoreCoins/AddMoreCoins';
import classes from './CoinList.module.css';
import CoinTable from './CoinTable';

const CoinListView = ({selectedCoins, coins, isLoading, isError, getCoins}) => {
  if (isLoading) {
    return 'Loading...';
  }

  if (isError) {
    return (
      <div className={classes.errorHolder}>
        <span>Oops, some error occurred</span>
        <br />
        <button type="button" className={classes.btn} onClick={getCoins}>
          try again
        </button>
      </div>
    );
  }

  return (
    <div>
      <CoinTable coins={selectedCoins} />
      <AddMoreCoins coins={coins} />
    </div>
  );
};

CoinListView.propTypes = {
  coins: PropTypes.arrayOf(PropTypes.any),
  selectedCoins: PropTypes.arrayOf(PropTypes.any),
  isLoading: PropTypes.bool,
  isError: PropTypes.bool,
  getCoins: PropTypes.func,
};

CoinListView.defaultProps = {
  coins: undefined,
  selectedCoins: undefined,
  isLoading: true,
  isError: false,
  getCoins: () => {}, // noop
};
export default CoinListView;
