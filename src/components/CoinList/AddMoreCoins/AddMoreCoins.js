import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {
  useSelectedCoins,
  useSelectedCoinsActions,
} from 'features/selectedCoins';
import SelectSearch from 'react-select-search';
import {MAX_SELECTED_COINS} from 'features/selectedCoins/SelectedCoinsReducer';

const DEF_VALUE = '';

const AddMoreCoins = ({coins}) => {
  const {addSelectedCoin} = useSelectedCoinsActions();
  const {selectedCoins} = useSelectedCoins();

  const [defValue, setDefValue] = useState(DEF_VALUE);

  if (!coins || !selectedCoins) {
    return '';
  }

  // I am not using useCallback cause https://kentcdodds.com/blog/usememo-and-usecallback
  const onSelect = coinID => {
    addSelectedCoin(coins.find(coin => coin.id === coinID));
    // hack to clean up selector
    setDefValue(-Math.random());
  };

  // transform coins to select options
  const options = coins
    .map(coin => ({name: `${coin.symbol} (${coin.name})`, value: coin.id}))
    // should include all coins except already selected
    .filter(({value}) => !selectedCoins.find(coin => coin.id === value));

  if (selectedCoins.length >= MAX_SELECTED_COINS) {
    return ''; // `You can have no more than ${MAX_SELECTED_COINS} in the list`;
  }

  /* want to try smth new: simple react-select is to casual */
  return (
    <SelectSearch
      /* autoFocus */
      value={defValue}
      options={options}
      search
      emptyMessage="Not found"
      placeholder="Add another coin"
      onChange={onSelect}
    />
  );
};

AddMoreCoins.propTypes = {
  coins: PropTypes.arrayOf(PropTypes.any),
};

AddMoreCoins.defaultProps = {
  coins: null,
};
export default AddMoreCoins;
