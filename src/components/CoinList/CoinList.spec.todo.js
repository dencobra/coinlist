import React from 'react';
import {Provider} from 'react-redux';
import {render, fireEvent} from '@testing-library/react';
import configureStore from 'redux-mock-store';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import promise from 'redux-promise-middleware';
import CoinList from './CoinList';

describe('components > CoinList', () => {
  /**
   * Initialize axios mock adapter to mock API responses
   * @see https://github.com/ctimmerm/axios-mock-adapter
   */
  const mockAxios = new MockAdapter(axios);

  beforeEach(() => {
    mockAxios.resetHandlers();
  });

  /** Create mock store with the count value */
  const mockStore = configureStore([promise]);
  const store = mockStore({
    // todo: yeah naming is not perfect
    selectedCoins: {
      selectedCoins: [
        {
          id: 1,
          name: 'Bitcoin',
          symbol: 'BTC',
          cmc_rank: 1,
          price: 1.0436269793308763,
        },
        {
          id: 2,
          name: 'Litecoin',
          symbol: 'LTC',
          cmc_rank: 2,
          price: 0.0000436269793308763,
        },
        {
          id: 3,
          name: 'Namecoin',
          symbol: 'NMC',
          cmc_rank: 3,
          price: 1.0436269793308763,
        },
        {
          id: 4,
          name: 'Terracoin',
          symbol: 'TRC',
          cmc_rank: 4,
          price: 1.0436269793308763,
        },
        {
          id: 5,
          name: 'Peercoin',
          symbol: 'PPC',
          cmc_rank: 5,
          price: 1.0436269793308763,
        },
        {
          id: 6,
          name: 'Novacoin',
          symbol: 'NVC',
          cmc_rank: 6,
          price: 0.000436269793308763,
        },
        {
          id: 8,
          name: 'Feathercoin',
          symbol: 'FTC',
          cmc_rank: 8,
          price: 21.0436269793308763,
        },
      ],
    },
    coins: {
      isFulfilled: true,
      isLoading: false,
      hasError: false,
      coins: [
        {
          id: 1,
          name: 'Bitcoin',
          symbol: 'BTC',
        },
        {
          id: 2,
          name: 'Litecoin',
          symbol: 'LTC',
        },
        {
          id: 3,
          name: 'Namecoin',
          symbol: 'NMC',
        },
        {
          id: 4,
          name: 'Terracoin',
          symbol: 'TRC',
        },
        {
          id: 5,
          name: 'Peercoin',
          symbol: 'PPC',
        },
        {
          id: 6,
          name: 'Novacoin',
          symbol: 'NVC',
        },
        {
          id: 8,
          name: 'Feathercoin',
          symbol: 'FTC',
        },
        {
          id: 10,
          name: 'Freicoin',
          symbol: 'FRC',
        },
      ],
    },
  });

  /**
   * Add spy to watch for store.dispatch method.
   * @see https://jestjs.io/docs/en/jest-object#jestspyonobject-methodname
   */
  jest.spyOn(store, 'dispatch');

  /**
   * Jest hook which runs before each test,
   * @see https://jestjs.io/docs/en/api#beforeeachfn-timeout
   */
  beforeEach(() => {
    /**
     * Clear any saved mock data from previous tests,
     * because jest saves calls data for spies and mocks.
     * @see https://jestjs.io/docs/en/mock-function-api#mockfnmockclear
     */
    store.dispatch.mockClear();
  });

  it('renders without crashing', () => {
    /**
     * `asFragment`:
     * @see https://testing-library.com/docs/react-testing-library/api#asfragment
     * `qetByText`:
     * @see https://testing-library.com/docs/dom-testing-library/api-queries#bytext
     * `wrapper`
     * @see https://testing-library.com/docs/react-testing-library/api#wrapper
     */
    const {getByText} = render(<CoinList />, {
      // eslint-disable-next-line react/prop-types
      wrapper: ({children}) => <Provider store={store}>{children}</Provider>,
    });

    /**
     * Basic snapshot test to make sure, that rendered component
     * matches expected footprint.
     */
    // todo will not work cause: Jest encountered an unexpected token: SelectSearch
    // expect(asFragment()).toMatchSnapshot();

    /** More precise test for counter value */
    // TODO get price
    expect(getByText(/6/i).textContent).toBe('6'); // 6 is value we expect, we need to convert Number to String, because HTMLElement textContent method returns string value
  });

  it('dispatches an action on button click', () => {
    /** Mock response from API */
    const response = 6; // todo
    mockAxios
      .onGet(`${process.env.REACT_APP_API_URL}/map`)
      .reply(200, response);

    /**
     * `getByRole`:
     * @see https://testing-library.com/docs/dom-testing-library/api-queries#byrole
     */
    const {getByTestId} = render(<CoinList />, {
      // eslint-disable-next-line react/prop-types
      wrapper: ({children}) => <Provider store={store}>{children}</Provider>,
    });

    /**
     * Search for the button and make testing library click on it
     * @see https://testing-library.com/docs/react-testing-library/cheatsheet#events
     */
    fireEvent.click(getByTestId('add-button'));

    /** Check if store.dispatch method was run */
    expect(store.dispatch).toHaveBeenCalledTimes(1);

    /** Check if store.dispatch was run with correct action */
    expect(store.dispatch).toHaveBeenCalledWith({
      type: 'todo ',
      value: 7,
    });
  });
});
