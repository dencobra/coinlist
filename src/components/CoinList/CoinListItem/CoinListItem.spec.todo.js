import {render} from '@testing-library/react';
import React from 'react';
import configureStore from 'redux-mock-store';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import promise from 'redux-promise-middleware';

import {Provider} from 'react-redux';
import CoinListItem from './CoinListItem';

// todo I have weird error:  "messageParent" can only be used inside a worker
describe('components > CoinList', () => {
  /**
   * Initialize axios mock adapter to mock API responses
   * @see https://github.com/ctimmerm/axios-mock-adapter
   */
  const mockAxios = new MockAdapter(axios);

  beforeEach(() => {
    mockAxios.resetHandlers();
  });

  /** Create mock store with the count value */
  const mockStore = configureStore([promise]);
  const store = mockStore({});

  /**
   * Add spy to watch for store.dispatch method.
   * @see https://jestjs.io/docs/en/jest-object#jestspyonobject-methodname
   */
  jest.spyOn(store, 'dispatch');

  /**
   * Jest hook which runs before each test,
   * @see https://jestjs.io/docs/en/api#beforeeachfn-timeout
   */
  beforeEach(() => {
    /**
     * Clear any saved mock data from previous tests,
     * because jest saves calls data for spies and mocks.
     * @see https://jestjs.io/docs/en/mock-function-api#mockfnmockclear
     */
    store.dispatch.mockClear();
  });
  it('CoinListItem is rendering', () => {
    // A priori
    // const mockOnClick = jest.fn(_.noop);

    const coin = {
      id: 1,
      symbol: 'BTC',
      price: 0.00004044,
      cmc_rank: 2,
    };

    // Action
    const {getByTestId} = render(<CoinListItem coin={coin} />, {
      // eslint-disable-next-line react/prop-types
      wrapper: ({children}) => <Provider store={store}>{children}</Provider>,
    });

    // A posteriory
    expect(getByTestId('price')).toBeInTheDocument();
    expect(getByTestId('price')).toEqual('$0.0004');
  });
});
