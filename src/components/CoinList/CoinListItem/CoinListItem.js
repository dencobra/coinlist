import React from 'react';
import PropTypes from 'prop-types';
import {useSelectedCoinsActions} from 'features/selectedCoins';
import classes from '../CoinList.module.css';
import CoinPrice from './CoinPrice';

const CoinListItem = ({coin, hideRemoveButton}) => {
  const {removeSelectedCoin} = useSelectedCoinsActions();

  const remove = () => {
    removeSelectedCoin(coin.id);
  };

  if (!coin) {
    return (
      <>
        <td>Loading...</td>
        <td />
        <td className={classes.price} />
        <td className={classes.actions} />
      </>
    );
  }

  const {symbol, price} = coin;
  return (
    <>
      <td>{coin.cmc_rank || '...'}</td>
      <td>{symbol || '...'}</td>
      <td className={classes.price} data-testid="price">
        <CoinPrice price={price} />
      </td>
      <td className={classes.actions}>
        {!hideRemoveButton && (
          <button type="button" className={classes.btn} onClick={remove}>
            remove
          </button>
        )}
      </td>
    </>
  );
};

CoinListItem.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  coin: PropTypes.any.isRequired,
  hideRemoveButton: PropTypes.bool,
};

CoinListItem.defaultProps = {
  hideRemoveButton: false,
};

export default CoinListItem;
