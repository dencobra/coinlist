import PropTypes from 'prop-types';
import getCurrentCurrency from 'helper';

const formatter = (currency, minimumFractionDigits) =>
  new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency,
    minimumFractionDigits,
  });

const CoinPrice = ({price}) => {
  const currency = getCurrentCurrency();
  if (!price) {
    return '...';
  }
  // e.g. if price 0.00002
  if (price < 0.01) {
    const displayPrice = formatter(currency, 6).format(price.toFixed(6));
    return displayPrice.replace(/\.?0+$/, ''); // to remove trailing zeros
  }
  return formatter(currency, 2).format(price);
};

CoinPrice.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  price: PropTypes.number,
};

CoinPrice.defaultProps = {
  price: null,
};

export default CoinPrice;
