import React from 'react';
import PropTypes from 'prop-types';
import {MIN_SELECTED_COINS} from 'features/selectedCoins/SelectedCoinsReducer';
import getCurrentCurrency from 'helper';
import FadeInAnimation from 'animation/FadeInAnimation';
import classes from './CoinList.module.css';
import CoinListItem from './CoinListItem/CoinListItem';

const CoinTable = ({coins}) => (
  <table className={classes['styled-table']}>
    {/* We can use react-table or smth likt that
      but for simplicity of potential scalability let's leave it as
      a simple table
    */}
    <thead>
      <tr>
        <th>#Rank</th>
        <th>Symbol</th>
        <th className={classes.price}>Price ({getCurrentCurrency()})</th>
        <th className={classes.actions}>{/* for actions */}</th>
      </tr>
    </thead>
    <tbody>
      {coins.map(coin => (
        <FadeInAnimation wrapperElement="tr" key={coin.id} direction="down">
          <CoinListItem
            coin={coin}
            hideRemoveButton={coins.length === MIN_SELECTED_COINS}
          />
        </FadeInAnimation>
      ))}
    </tbody>
  </table>
);

CoinTable.propTypes = {
  coins: PropTypes.arrayOf(PropTypes.any).isRequired,
};

export default CoinTable;
