/* eslint-disable no-plusplus */
import {useActions, useCoins} from 'features/coins';
import {
  useSelectedCoins,
  useSelectedCoinsActions,
} from 'features/selectedCoins';
import React, {useEffect} from 'react';
import CoinListView from './CoinListView';

const CoinList = () => {
  const {getCoins, getCoinQuotes} = useActions();
  const {selectedCoins} = useSelectedCoins();
  const {hasError, isLoading, coins} = useCoins();
  const {addSelectedCoin} = useSelectedCoinsActions();

  // - - - Initial Logic:

  useEffect(() => {
    // get all coins during first load
    getCoins();
  }, [getCoins]);

  useEffect(() => {
    // all coins are loaded but selected list is empty -> first time initialization
    if (!isLoading && coins && !selectedCoins.length) {
      // ...but initially, only five will appear. Lets take first 5.
      const START_NUM__OF_COINS = 5;
      const START_NUM =
        coins.length < START_NUM__OF_COINS ? coins.length : START_NUM__OF_COINS;
      // eslint-disable-next-line fp/no-mutation
      for (let i = 0; i < START_NUM; i++) {
        addSelectedCoin(coins[i]);
      }
    }
  }, [isLoading, coins, selectedCoins, addSelectedCoin]);

  useEffect(() => {
    // ask for quotes if no price
    selectedCoins?.forEach(coin => {
      if (!coin.price) {
        getCoinQuotes(coin.id);
      }
    });
  }, [getCoinQuotes, selectedCoins]);

  // - - - View:

  const coinListViewProps = {
    selectedCoins,
    coins,
    isLoading: !selectedCoins && (isLoading || (!hasError && !coins)),
    isError: hasError,
    getCoins,
  };

  return <CoinListView {...coinListViewProps} />;
};

export default CoinList;
