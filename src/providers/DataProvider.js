import axios from 'axios';
/* import mockBtcResopnse from './__mocks__/btc.mock';
import mockLtcResopnse from './__mocks__/ltc.mock';
import mockMapResponse from './__mocks__/map.mock'; */

const DataProvider = {
  map: () => axios.get(`${process.env.REACT_APP_API_URL}/map`),
  quotes: id => axios.get(`${process.env.REACT_APP_API_URL}/quotes?id=${id}`),
};

// mock, for dev purpose
/* const DataProvider = {
  map: async () => {
    const delay = ms => new Promise(res => setTimeout(res, ms));
    await delay(300);
    return Promise.resolve({data: mockMapResponse});
  },
  quotes: async id => {
    const delay = ms => new Promise(res => setTimeout(res, ms));
    await delay(1000);
    return Promise.resolve({
      data:
        id === 1
          ? mockBtcResopnse
          : {
              ...mockLtcResopnse,
              data: {
                [id]: {...mockLtcResopnse.data[2], cmc_rank: id, id},
              },
            },
    });
  },
}; */

export default DataProvider;
