const mockBtcResopnse = {
  status: {
    timestamp: '2020-11-26T16:21:10.302Z',
    error_code: 0,
    error_message: null,
    elapsed: 0,
    credit_count: 1,
    notice: null,
  },
  data: {
    1: {
      id: 1,
      name: 'Bitcoin',
      symbol: 'BTC',
      slug: 'bitcoin',
      is_active: 1,
      is_fiat: 0,
      cmc_rank: 1,
      num_market_pairs: 4895,
      circulating_supply: 7700000,
      total_supply: 8000000,
      max_supply: 26000000,
      date_added: '2020-03-17T23:06:56.176Z',
      tags: [],
      platform: null,
      last_updated: '2020-08-18T14:18:46.626Z',
      quote: {
        USD: {
          price: 1.0436269793308763,
          volume_24h: 17713818.415502753,
          market_cap: 574178433.4197428,
          percent_change_1h: 0.3112109800956718,
          percent_change_24h: 1.7005689413620395,
          percent_change_7d: -9.910719452839366,
          last_updated: '2020-11-26T16:20:34.108Z',
        },
      },
    },
  },
};

export default mockBtcResopnse;
