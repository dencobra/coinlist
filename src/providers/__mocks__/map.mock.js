const mockMapResponse = {
    "status": {
      "timestamp": "2020-11-26T22:51:22.356Z",
      "error_code": 0,
      "error_message": null,
      "elapsed": 2,
      "credit_count": 1,
      "notice": null
    },
    "data": [
      {
        "id": 1,
        "name": "Bitcoin",
        "symbol": "BTC",
        "slug": "bitcoin",
        "is_active": 1,
        "first_historical_data": "2020-03-17T23:06:56.176Z",
        "last_historical_data": "2020-08-17T21:54:30.962Z",
        "platform": null
      },
      {
        "id": 2,
        "name": "Litecoin",
        "symbol": "LTC",
        "slug": "litecoin",
        "is_active": 1,
        "first_historical_data": "2020-02-13T15:18:50.069Z",
        "last_historical_data": "2020-08-18T08:00:24.826Z",
        "platform": null
      },
      {
        "id": 3,
        "name": "Namecoin",
        "symbol": "NMC",
        "slug": "namecoin",
        "is_active": 1,
        "first_historical_data": "2019-09-26T09:45:58.375Z",
        "last_historical_data": "2020-08-18T01:14:10.238Z",
        "platform": null
      },
      {
        "id": 4,
        "name": "Terracoin",
        "symbol": "TRC",
        "slug": "terracoin",
        "is_active": 1,
        "first_historical_data": "2020-06-03T22:28:47.743Z",
        "last_historical_data": "2020-08-18T13:17:52.388Z",
        "platform": null
      },
      {
        "id": 5,
        "name": "Peercoin",
        "symbol": "PPC",
        "slug": "peercoin",
        "is_active": 1,
        "first_historical_data": "2020-05-25T09:56:09.329Z",
        "last_historical_data": "2020-08-18T14:03:36.885Z",
        "platform": null
      },
      {
        "id": 6,
        "name": "Novacoin",
        "symbol": "NVC",
        "slug": "novacoin",
        "is_active": 1,
        "first_historical_data": "2020-04-07T11:09:23.923Z",
        "last_historical_data": "2020-08-18T06:02:27.896Z",
        "platform": null
      },
      {
        "id": 8,
        "name": "Feathercoin",
        "symbol": "FTC",
        "slug": "feathercoin",
        "is_active": 1,
        "first_historical_data": "2020-08-07T07:31:35.413Z",
        "last_historical_data": "2020-08-17T22:13:24.747Z",
        "platform": null
      },
      {
        "id": 10,
        "name": "Freicoin",
        "symbol": "FRC",
        "slug": "freicoin",
        "is_active": 1,
        "first_historical_data": "2020-01-17T20:19:35.925Z",
        "last_historical_data": "2020-08-17T18:48:52.056Z",
        "platform": null
      },
      {
        "id": 13,
        "name": "Ixcoin",
        "symbol": "IXC",
        "slug": "ixcoin",
        "is_active": 1,
        "first_historical_data": "2019-10-29T18:25:28.588Z",
        "last_historical_data": "2020-08-17T19:21:07.323Z",
        "platform": null
      },
      {
        "id": 14,
        "name": "BitBar",
        "symbol": "BTB",
        "slug": "bitbar",
        "is_active": 1,
        "first_historical_data": "2019-12-27T19:52:28.811Z",
        "last_historical_data": "2020-08-17T19:09:29.548Z",
        "platform": null
      },
      {
        "id": 18,
        "name": "Digitalcoin",
        "symbol": "DGC",
        "slug": "digitalcoin",
        "is_active": 1,
        "first_historical_data": "2020-05-13T14:25:23.253Z",
        "last_historical_data": "2020-08-18T12:31:24.842Z",
        "platform": null
      },
      {
        "id": 25,
        "name": "Goldcoin",
        "symbol": "GLC",
        "slug": "goldcoin",
        "is_active": 1,
        "first_historical_data": "2020-02-28T10:28:35.788Z",
        "last_historical_data": "2020-08-18T03:17:28.225Z",
        "platform": null
      },
      {
        "id": 35,
        "name": "Phoenixcoin",
        "symbol": "PXC",
        "slug": "phoenixcoin",
        "is_active": 1,
        "first_historical_data": "2020-08-12T16:29:39.195Z",
        "last_historical_data": "2020-08-18T00:57:41.515Z",
        "platform": null
      },
      {
        "id": 37,
        "name": "Megacoin",
        "symbol": "MEC",
        "slug": "megacoin",
        "is_active": 1,
        "first_historical_data": "2019-09-12T04:03:36.835Z",
        "last_historical_data": "2020-08-18T13:33:05.616Z",
        "platform": null
      },
      {
        "id": 41,
        "name": "Infinitecoin",
        "symbol": "IFC",
        "slug": "infinitecoin",
        "is_active": 1,
        "first_historical_data": "2019-10-31T14:13:28.873Z",
        "last_historical_data": "2020-08-18T16:26:09.899Z",
        "platform": null
      },
      {
        "id": 42,
        "name": "Primecoin",
        "symbol": "XPM",
        "slug": "primecoin",
        "is_active": 1,
        "first_historical_data": "2019-10-11T21:40:12.615Z",
        "last_historical_data": "2020-08-18T16:14:39.709Z",
        "platform": null
      },
      {
        "id": 43,
        "name": "Anoncoin",
        "symbol": "ANC",
        "slug": "anoncoin",
        "is_active": 1,
        "first_historical_data": "2020-08-13T06:12:00.310Z",
        "last_historical_data": "2020-08-17T18:27:25.861Z",
        "platform": null
      },
      {
        "id": 45,
        "name": "CasinoCoin",
        "symbol": "CSC",
        "slug": "casinocoin",
        "is_active": 1,
        "first_historical_data": "2019-12-11T00:55:10.381Z",
        "last_historical_data": "2020-08-18T16:09:49.256Z",
        "platform": null
      },
      {
        "id": 50,
        "name": "Emerald Crypto",
        "symbol": "EMD",
        "slug": "emerald",
        "is_active": 1,
        "first_historical_data": "2020-02-14T04:11:24.339Z",
        "last_historical_data": "2020-08-18T13:50:57.794Z",
        "platform": null
      },
      {
        "id": 52,
        "name": "XRP",
        "symbol": "XRP",
        "slug": "xrp",
        "is_active": 1,
        "first_historical_data": "2019-10-30T16:44:23.365Z",
        "last_historical_data": "2020-08-17T18:59:38.096Z",
        "platform": null
      },
      {
        "id": 53,
        "name": "Quark",
        "symbol": "QRK",
        "slug": "quark",
        "is_active": 1,
        "first_historical_data": "2020-08-17T12:42:02.819Z",
        "last_historical_data": "2020-08-18T16:44:30.013Z",
        "platform": null
      },
      {
        "id": 56,
        "name": "Zetacoin",
        "symbol": "ZET",
        "slug": "zetacoin",
        "is_active": 1,
        "first_historical_data": "2020-01-04T13:34:59.026Z",
        "last_historical_data": "2020-08-18T11:06:57.616Z",
        "platform": null
      },
      {
        "id": 58,
        "name": "Sexcoin",
        "symbol": "SXC",
        "slug": "sexcoin",
        "is_active": 1,
        "first_historical_data": "2020-05-02T16:41:28.992Z",
        "last_historical_data": "2020-08-18T08:15:28.420Z",
        "platform": null
      },
      {
        "id": 61,
        "name": "TagCoin",
        "symbol": "TAG",
        "slug": "tagcoin",
        "is_active": 1,
        "first_historical_data": "2020-04-09T14:42:41.636Z",
        "last_historical_data": "2020-08-18T03:50:47.573Z",
        "platform": null
      },
      {
        "id": 64,
        "name": "FLO",
        "symbol": "FLO",
        "slug": "flo",
        "is_active": 1,
        "first_historical_data": "2020-06-12T14:26:01.546Z",
        "last_historical_data": "2020-08-17T22:49:13.047Z",
        "platform": null
      },
      {
        "id": 66,
        "name": "Nxt",
        "symbol": "NXT",
        "slug": "nxt",
        "is_active": 1,
        "first_historical_data": "2020-06-08T12:46:11.990Z",
        "last_historical_data": "2020-08-18T11:00:39.501Z",
        "platform": null
      },
      {
        "id": 67,
        "name": "Unobtanium",
        "symbol": "UNO",
        "slug": "unobtanium",
        "is_active": 1,
        "first_historical_data": "2020-02-24T16:55:22.270Z",
        "last_historical_data": "2020-08-18T15:51:15.241Z",
        "platform": null
      },
      {
        "id": 69,
        "name": "Datacoin",
        "symbol": "DTC",
        "slug": "datacoin",
        "is_active": 1,
        "first_historical_data": "2020-06-23T00:55:11.080Z",
        "last_historical_data": "2020-08-17T23:56:36.921Z",
        "platform": null
      },
      {
        "id": 72,
        "name": "Deutsche eMark",
        "symbol": "DEM",
        "slug": "deutsche-emark",
        "is_active": 1,
        "first_historical_data": "2020-02-09T01:57:55.268Z",
        "last_historical_data": "2020-08-17T18:50:16.912Z",
        "platform": null
      },
      {
        "id": 74,
        "name": "Dogecoin",
        "symbol": "DOGE",
        "slug": "dogecoin",
        "is_active": 1,
        "first_historical_data": "2019-09-16T12:08:05.940Z",
        "last_historical_data": "2020-08-18T07:41:54.377Z",
        "platform": null
      },
      {
        "id": 77,
        "name": "Diamond",
        "symbol": "DMD",
        "slug": "diamond",
        "is_active": 1,
        "first_historical_data": "2020-07-05T00:12:53.931Z",
        "last_historical_data": "2020-08-18T13:34:30.505Z",
        "platform": null
      },
      {
        "id": 78,
        "name": "HoboNickels",
        "symbol": "HBN",
        "slug": "hobonickels",
        "is_active": 1,
        "first_historical_data": "2019-11-14T02:40:45.359Z",
        "last_historical_data": "2020-08-17T20:29:03.229Z",
        "platform": null
      },
      {
        "id": 80,
        "name": "Orbitcoin",
        "symbol": "ORB",
        "slug": "orbitcoin",
        "is_active": 1,
        "first_historical_data": "2020-05-14T15:06:42.860Z",
        "last_historical_data": "2020-08-18T03:49:21.744Z",
        "platform": null
      },
      {
        "id": 83,
        "name": "Omni",
        "symbol": "OMNI",
        "slug": "omni",
        "is_active": 1,
        "first_historical_data": "2020-07-24T10:25:43.372Z",
        "last_historical_data": "2020-08-18T13:20:35.895Z",
        "platform": null
      },
      {
        "id": 87,
        "name": "FedoraCoin",
        "symbol": "TIPS",
        "slug": "fedoracoin",
        "is_active": 1,
        "first_historical_data": "2019-10-11T23:14:56.230Z",
        "last_historical_data": "2020-08-18T05:01:26.015Z",
        "platform": null
      },
      {
        "id": 89,
        "name": "Mooncoin",
        "symbol": "MOON",
        "slug": "mooncoin",
        "is_active": 1,
        "first_historical_data": "2020-08-12T02:57:18.156Z",
        "last_historical_data": "2020-08-18T01:44:20.828Z",
        "platform": null
      },
      {
        "id": 90,
        "name": "Dimecoin",
        "symbol": "DIME",
        "slug": "dimecoin",
        "is_active": 1,
        "first_historical_data": "2019-12-22T15:33:33.982Z",
        "last_historical_data": "2020-08-18T09:24:43.159Z",
        "platform": null
      },
      {
        "id": 93,
        "name": "42-coin",
        "symbol": "42",
        "slug": "42-coin",
        "is_active": 1,
        "first_historical_data": "2019-11-14T22:30:56.269Z",
        "last_historical_data": "2020-08-18T02:43:22.926Z",
        "platform": null
      },
      {
        "id": 99,
        "name": "Vertcoin",
        "symbol": "VTC",
        "slug": "vertcoin",
        "is_active": 1,
        "first_historical_data": "2019-08-26T14:40:28.573Z",
        "last_historical_data": "2020-08-18T17:06:30.967Z",
        "platform": null
      },
      {
        "id": 109,
        "name": "DigiByte",
        "symbol": "DGB",
        "slug": "digibyte",
        "is_active": 1,
        "first_historical_data": "2019-12-27T11:16:47.107Z",
        "last_historical_data": "2020-08-18T03:28:49.644Z",
        "platform": null
      },
      {
        "id": 113,
        "name": "SmartCoin",
        "symbol": "SMC",
        "slug": "smartcoin",
        "is_active": 1,
        "first_historical_data": "2020-04-02T13:05:59.861Z",
        "last_historical_data": "2020-08-18T00:59:40.741Z",
        "platform": null
      },
      {
        "id": 118,
        "name": "ReddCoin",
        "symbol": "RDD",
        "slug": "reddcoin",
        "is_active": 1,
        "first_historical_data": "2020-08-03T07:25:21.555Z",
        "last_historical_data": "2020-08-18T02:12:15.374Z",
        "platform": null
      },
      {
        "id": 122,
        "name": "PotCoin",
        "symbol": "POT",
        "slug": "potcoin",
        "is_active": 1,
        "first_historical_data": "2019-09-03T16:14:01.786Z",
        "last_historical_data": "2020-08-18T16:12:39.967Z",
        "platform": null
      },
      {
        "id": 125,
        "name": "Blakecoin",
        "symbol": "BLC",
        "slug": "blakecoin",
        "is_active": 1,
        "first_historical_data": "2020-01-10T21:42:04.138Z",
        "last_historical_data": "2020-08-18T08:36:39.796Z",
        "platform": null
      },
      {
        "id": 128,
        "name": "Maxcoin",
        "symbol": "MAX",
        "slug": "maxcoin",
        "is_active": 1,
        "first_historical_data": "2020-06-17T06:23:49.435Z",
        "last_historical_data": "2020-08-18T14:26:15.180Z",
        "platform": null
      },
      {
        "id": 131,
        "name": "Dash",
        "symbol": "DASH",
        "slug": "dash",
        "is_active": 1,
        "first_historical_data": "2020-08-10T08:49:13.407Z",
        "last_historical_data": "2020-08-18T09:04:18.380Z",
        "platform": null
      },
      {
        "id": 132,
        "name": "Counterparty",
        "symbol": "XCP",
        "slug": "counterparty",
        "is_active": 1,
        "first_historical_data": "2020-01-20T02:21:23.189Z",
        "last_historical_data": "2020-08-18T02:12:57.380Z",
        "platform": null
      },
      {
        "id": 141,
        "name": "MintCoin",
        "symbol": "MINT",
        "slug": "mintcoin",
        "is_active": 1,
        "first_historical_data": "2020-03-13T20:05:37.375Z",
        "last_historical_data": "2020-08-17T19:43:47.870Z",
        "platform": null
      },
      {
        "id": 142,
        "name": "Aricoin",
        "symbol": "ARI",
        "slug": "aricoin",
        "is_active": 1,
        "first_historical_data": "2020-01-04T12:32:23.461Z",
        "last_historical_data": "2020-08-17T23:53:59.677Z",
        "platform": null
      },
      {
        "id": 145,
        "name": "DopeCoin",
        "symbol": "DOPE",
        "slug": "dopecoin",
        "is_active": 1,
        "first_historical_data": "2019-12-20T03:52:56.331Z",
        "last_historical_data": "2020-08-18T17:40:06.175Z",
        "platform": null
      },
      {
        "id": 148,
        "name": "Auroracoin",
        "symbol": "AUR",
        "slug": "auroracoin",
        "is_active": 1,
        "first_historical_data": "2020-07-23T09:32:51.004Z",
        "last_historical_data": "2020-08-18T07:06:35.429Z",
        "platform": null
      },
      {
        "id": 151,
        "name": "Pesetacoin",
        "symbol": "PTC",
        "slug": "pesetacoin",
        "is_active": 1,
        "first_historical_data": "2019-09-22T21:26:43.725Z",
        "last_historical_data": "2020-08-18T11:01:48.307Z",
        "platform": null
      },
      {
        "id": 161,
        "name": "Pandacoin",
        "symbol": "PND",
        "slug": "pandacoin-pnd",
        "is_active": 1,
        "first_historical_data": "2020-06-24T12:10:34.267Z",
        "last_historical_data": "2020-08-17T19:35:20.490Z",
        "platform": null
      },
      {
        "id": 170,
        "name": "BlackCoin",
        "symbol": "BLK",
        "slug": "blackcoin",
        "is_active": 1,
        "first_historical_data": "2019-08-20T20:41:17.959Z",
        "last_historical_data": "2020-08-18T03:57:35.045Z",
        "platform": null
      },
      {
        "id": 175,
        "name": "Photon",
        "symbol": "PHO",
        "slug": "photon",
        "is_active": 1,
        "first_historical_data": "2019-11-25T05:53:30.366Z",
        "last_historical_data": "2020-08-18T12:13:33.442Z",
        "platform": null
      },
      {
        "id": 181,
        "name": "Zeitcoin",
        "symbol": "ZEIT",
        "slug": "zeitcoin",
        "is_active": 1,
        "first_historical_data": "2019-12-28T11:04:55.716Z",
        "last_historical_data": "2020-08-17T23:41:07.419Z",
        "platform": null
      },
      {
        "id": 182,
        "name": "Myriad",
        "symbol": "XMY",
        "slug": "myriad",
        "is_active": 1,
        "first_historical_data": "2020-01-24T11:21:30.083Z",
        "last_historical_data": "2020-08-18T04:16:42.323Z",
        "platform": null
      },
      {
        "id": 184,
        "name": "DNotes",
        "symbol": "NOTE",
        "slug": "dnotes",
        "is_active": 1,
        "first_historical_data": "2019-12-19T16:19:27.593Z",
        "last_historical_data": "2020-08-18T07:39:12.492Z",
        "platform": null
      },
      {
        "id": 201,
        "name": "Einsteinium",
        "symbol": "EMC2",
        "slug": "einsteinium",
        "is_active": 1,
        "first_historical_data": "2019-09-01T21:23:51.155Z",
        "last_historical_data": "2020-08-17T18:50:03.719Z",
        "platform": null
      },
      {
        "id": 212,
        "name": "ECC",
        "symbol": "ECC",
        "slug": "eccoin",
        "is_active": 1,
        "first_historical_data": "2020-03-30T05:04:37.683Z",
        "last_historical_data": "2020-08-18T04:55:39.118Z",
        "platform": null
      },
      {
        "id": 213,
        "name": "MonaCoin",
        "symbol": "MONA",
        "slug": "monacoin",
        "is_active": 1,
        "first_historical_data": "2019-10-13T02:18:35.655Z",
        "last_historical_data": "2020-08-18T06:16:13.084Z",
        "platform": null
      },
      {
        "id": 215,
        "name": "Rubycoin",
        "symbol": "RBY",
        "slug": "rubycoin",
        "is_active": 1,
        "first_historical_data": "2019-10-07T07:47:57.675Z",
        "last_historical_data": "2020-08-18T02:18:24.464Z",
        "platform": null
      },
      {
        "id": 217,
        "name": "Bela",
        "symbol": "BELA",
        "slug": "belacoin",
        "is_active": 1,
        "first_historical_data": "2019-10-07T22:21:03.472Z",
        "last_historical_data": "2020-08-18T07:50:51.243Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2e98a6804e4b6c832ed0ca876a943abd3400b224"
        }
      },
      {
        "id": 233,
        "name": "SolarCoin",
        "symbol": "SLR",
        "slug": "solarcoin",
        "is_active": 1,
        "first_historical_data": "2020-02-26T12:45:48.872Z",
        "last_historical_data": "2020-08-18T03:20:58.164Z",
        "platform": null
      },
      {
        "id": 234,
        "name": "e-Gulden",
        "symbol": "EFL",
        "slug": "e-gulden",
        "is_active": 1,
        "first_historical_data": "2020-07-29T19:32:19.937Z",
        "last_historical_data": "2020-08-18T07:07:04.586Z",
        "platform": null
      },
      {
        "id": 254,
        "name": "Gulden",
        "symbol": "NLG",
        "slug": "gulden",
        "is_active": 1,
        "first_historical_data": "2019-10-10T08:49:25.115Z",
        "last_historical_data": "2020-08-18T16:06:31.961Z",
        "platform": null
      },
      {
        "id": 258,
        "name": "Groestlcoin",
        "symbol": "GRS",
        "slug": "groestlcoin",
        "is_active": 1,
        "first_historical_data": "2020-02-10T22:27:08.128Z",
        "last_historical_data": "2020-08-18T08:02:36.992Z",
        "platform": null
      },
      {
        "id": 260,
        "name": "PetroDollar",
        "symbol": "XPD",
        "slug": "petrodollar",
        "is_active": 1,
        "first_historical_data": "2020-01-07T09:10:35.322Z",
        "last_historical_data": "2020-08-18T17:10:32.640Z",
        "platform": null
      },
      {
        "id": 263,
        "name": "PLNcoin",
        "symbol": "PLNC",
        "slug": "plncoin",
        "is_active": 1,
        "first_historical_data": "2020-04-25T03:17:08.448Z",
        "last_historical_data": "2020-08-18T13:50:22.385Z",
        "platform": null
      },
      {
        "id": 268,
        "name": "WhiteCoin",
        "symbol": "XWC",
        "slug": "whitecoin",
        "is_active": 1,
        "first_historical_data": "2020-01-25T15:00:56.791Z",
        "last_historical_data": "2020-08-18T13:54:11.260Z",
        "platform": null
      },
      {
        "id": 275,
        "name": "PopularCoin",
        "symbol": "POP",
        "slug": "popularcoin",
        "is_active": 1,
        "first_historical_data": "2020-03-15T00:06:22.684Z",
        "last_historical_data": "2020-08-18T11:50:18.720Z",
        "platform": null
      },
      {
        "id": 276,
        "name": "Bitstar",
        "symbol": "BITS",
        "slug": "bitstar",
        "is_active": 1,
        "first_historical_data": "2019-10-26T05:34:59.306Z",
        "last_historical_data": "2020-08-18T16:17:30.960Z",
        "platform": null
      },
      {
        "id": 278,
        "name": "Quebecoin",
        "symbol": "QBC",
        "slug": "quebecoin",
        "is_active": 1,
        "first_historical_data": "2019-09-13T05:33:25.216Z",
        "last_historical_data": "2020-08-18T07:51:27.120Z",
        "platform": null
      },
      {
        "id": 290,
        "name": "BlueCoin",
        "symbol": "BLU",
        "slug": "bluecoin",
        "is_active": 1,
        "first_historical_data": "2020-01-12T07:13:56.073Z",
        "last_historical_data": "2020-08-18T01:40:56.194Z",
        "platform": null
      },
      {
        "id": 291,
        "name": "MaidSafeCoin",
        "symbol": "MAID",
        "slug": "maidsafecoin",
        "is_active": 1,
        "first_historical_data": "2020-03-28T00:14:44.747Z",
        "last_historical_data": "2020-08-17T21:33:31.471Z",
        "platform": {
          "id": 83,
          "name": "Omni",
          "symbol": "OMNI",
          "slug": "omni",
          "token_address": "3"
        }
      },
      {
        "id": 293,
        "name": "Bitcoin Plus",
        "symbol": "XBC",
        "slug": "bitcoin-plus",
        "is_active": 1,
        "first_historical_data": "2019-08-21T08:40:18.793Z",
        "last_historical_data": "2020-08-17T21:44:59.963Z",
        "platform": null
      },
      {
        "id": 298,
        "name": "NewYorkCoin",
        "symbol": "NYC",
        "slug": "newyorkcoin",
        "is_active": 1,
        "first_historical_data": "2020-06-09T07:37:55.191Z",
        "last_historical_data": "2020-08-18T09:04:43.441Z",
        "platform": null
      },
      {
        "id": 313,
        "name": "Pinkcoin",
        "symbol": "PINK",
        "slug": "pinkcoin",
        "is_active": 1,
        "first_historical_data": "2020-02-22T22:09:48.382Z",
        "last_historical_data": "2020-08-18T13:37:05.383Z",
        "platform": null
      },
      {
        "id": 316,
        "name": "Dreamcoin",
        "symbol": "DRM",
        "slug": "dreamcoin",
        "is_active": 1,
        "first_historical_data": "2020-08-01T15:58:06.756Z",
        "last_historical_data": "2020-08-18T07:08:50.739Z",
        "platform": null
      },
      {
        "id": 322,
        "name": "Energycoin",
        "symbol": "ENRG",
        "slug": "energycoin",
        "is_active": 1,
        "first_historical_data": "2020-02-27T21:22:48.695Z",
        "last_historical_data": "2020-08-18T04:58:02.026Z",
        "platform": null
      },
      {
        "id": 323,
        "name": "VeriCoin",
        "symbol": "VRC",
        "slug": "vericoin",
        "is_active": 1,
        "first_historical_data": "2020-07-02T08:50:04.809Z",
        "last_historical_data": "2020-08-18T15:22:38.702Z",
        "platform": null
      },
      {
        "id": 328,
        "name": "Monero",
        "symbol": "XMR",
        "slug": "monero",
        "is_active": 1,
        "first_historical_data": "2019-10-29T18:38:32.606Z",
        "last_historical_data": "2020-08-18T07:42:33.187Z",
        "platform": null
      },
      {
        "id": 331,
        "name": "Litecoin Plus",
        "symbol": "LCP",
        "slug": "litecoin-plus",
        "is_active": 1,
        "first_historical_data": "2020-02-11T11:22:45.970Z",
        "last_historical_data": "2020-08-17T18:51:04.409Z",
        "platform": null
      },
      {
        "id": 333,
        "name": "Curecoin",
        "symbol": "CURE",
        "slug": "curecoin",
        "is_active": 1,
        "first_historical_data": "2020-04-16T04:04:02.617Z",
        "last_historical_data": "2020-08-18T07:18:59.797Z",
        "platform": null
      },
      {
        "id": 341,
        "name": "SuperCoin",
        "symbol": "SUPER",
        "slug": "supercoin",
        "is_active": 1,
        "first_historical_data": "2020-07-27T04:43:57.534Z",
        "last_historical_data": "2020-08-18T07:03:14.729Z",
        "platform": null
      },
      {
        "id": 350,
        "name": "BoostCoin",
        "symbol": "BOST",
        "slug": "boostcoin",
        "is_active": 1,
        "first_historical_data": "2019-11-16T19:53:59.366Z",
        "last_historical_data": "2020-08-18T16:07:00.744Z",
        "platform": null
      },
      {
        "id": 360,
        "name": "Motocoin",
        "symbol": "MOTO",
        "slug": "motocoin",
        "is_active": 1,
        "first_historical_data": "2020-07-04T03:11:38.731Z",
        "last_historical_data": "2020-08-17T19:50:02.611Z",
        "platform": null
      },
      {
        "id": 362,
        "name": "CloakCoin",
        "symbol": "CLOAK",
        "slug": "cloakcoin",
        "is_active": 1,
        "first_historical_data": "2020-07-19T22:25:26.937Z",
        "last_historical_data": "2020-08-17T22:13:34.504Z",
        "platform": null
      },
      {
        "id": 366,
        "name": "BitSend",
        "symbol": "BSD",
        "slug": "bitsend",
        "is_active": 1,
        "first_historical_data": "2019-11-05T15:26:43.168Z",
        "last_historical_data": "2020-08-18T02:25:28.999Z",
        "platform": null
      },
      {
        "id": 367,
        "name": "Coin2.1",
        "symbol": "C2",
        "slug": "coin2-1",
        "is_active": 1,
        "first_historical_data": "2020-06-26T07:18:55.371Z",
        "last_historical_data": "2020-08-18T05:17:53.464Z",
        "platform": null
      },
      {
        "id": 372,
        "name": "Bytecoin",
        "symbol": "BCN",
        "slug": "bytecoin-bcn",
        "is_active": 1,
        "first_historical_data": "2020-08-11T06:42:05.919Z",
        "last_historical_data": "2020-08-17T23:19:36.895Z",
        "platform": null
      },
      {
        "id": 377,
        "name": "NavCoin",
        "symbol": "NAV",
        "slug": "nav-coin",
        "is_active": 1,
        "first_historical_data": "2020-01-27T09:09:18.878Z",
        "last_historical_data": "2020-08-18T16:04:10.779Z",
        "platform": null
      },
      {
        "id": 389,
        "name": "Startcoin",
        "symbol": "START",
        "slug": "startcoin",
        "is_active": 1,
        "first_historical_data": "2020-05-20T09:01:27.341Z",
        "last_historical_data": "2020-08-18T08:02:42.814Z",
        "platform": null
      },
      {
        "id": 405,
        "name": "DigitalNote",
        "symbol": "XDN",
        "slug": "digitalnote",
        "is_active": 1,
        "first_historical_data": "2019-12-03T06:39:52.750Z",
        "last_historical_data": "2020-08-17T18:46:01.698Z",
        "platform": null
      },
      {
        "id": 406,
        "name": "Boolberry",
        "symbol": "BBR",
        "slug": "boolberry",
        "is_active": 1,
        "first_historical_data": "2020-06-03T18:23:15.287Z",
        "last_historical_data": "2020-08-18T17:19:56.679Z",
        "platform": null
      },
      {
        "id": 416,
        "name": "HempCoin",
        "symbol": "THC",
        "slug": "hempcoin",
        "is_active": 1,
        "first_historical_data": "2019-11-27T14:40:44.278Z",
        "last_historical_data": "2020-08-17T19:08:46.419Z",
        "platform": null
      },
      {
        "id": 448,
        "name": "Stealth",
        "symbol": "XST",
        "slug": "stealth",
        "is_active": 1,
        "first_historical_data": "2020-02-14T15:11:38.112Z",
        "last_historical_data": "2020-08-18T10:18:57.017Z",
        "platform": null
      },
      {
        "id": 460,
        "name": "Clams",
        "symbol": "CLAM",
        "slug": "clams",
        "is_active": 1,
        "first_historical_data": "2019-09-04T15:43:52.329Z",
        "last_historical_data": "2020-08-17T18:33:06.037Z",
        "platform": null
      },
      {
        "id": 463,
        "name": "BitShares",
        "symbol": "BTS",
        "slug": "bitshares",
        "is_active": 1,
        "first_historical_data": "2020-07-25T10:08:54.797Z",
        "last_historical_data": "2020-08-18T05:54:59.812Z",
        "platform": null
      },
      {
        "id": 470,
        "name": "Viacoin",
        "symbol": "VIA",
        "slug": "viacoin",
        "is_active": 1,
        "first_historical_data": "2020-06-05T20:27:54.001Z",
        "last_historical_data": "2020-08-18T11:37:17.780Z",
        "platform": null
      },
      {
        "id": 495,
        "name": "I/O Coin",
        "symbol": "IOC",
        "slug": "iocoin",
        "is_active": 1,
        "first_historical_data": "2020-01-13T18:51:24.313Z",
        "last_historical_data": "2020-08-17T22:48:11.509Z",
        "platform": null
      },
      {
        "id": 501,
        "name": "Cryptonite",
        "symbol": "XCN",
        "slug": "cryptonite",
        "is_active": 1,
        "first_historical_data": "2020-06-05T12:00:13.636Z",
        "last_historical_data": "2020-08-18T05:43:13.033Z",
        "platform": null
      },
      {
        "id": 502,
        "name": "Carboncoin",
        "symbol": "CARBON",
        "slug": "carboncoin",
        "is_active": 1,
        "first_historical_data": "2020-06-25T17:12:09.383Z",
        "last_historical_data": "2020-08-18T15:16:55.300Z",
        "platform": null
      },
      {
        "id": 506,
        "name": "CannabisCoin",
        "symbol": "CANN",
        "slug": "cannabiscoin",
        "is_active": 1,
        "first_historical_data": "2019-10-27T00:31:03.587Z",
        "last_historical_data": "2020-08-18T11:21:43.293Z",
        "platform": null
      },
      {
        "id": 512,
        "name": "Stellar",
        "symbol": "XLM",
        "slug": "stellar",
        "is_active": 1,
        "first_historical_data": "2020-03-30T04:18:48.718Z",
        "last_historical_data": "2020-08-18T06:50:24.929Z",
        "platform": null
      },
      {
        "id": 513,
        "name": "Titcoin",
        "symbol": "TIT",
        "slug": "titcoin",
        "is_active": 1,
        "first_historical_data": "2019-09-07T02:44:39.601Z",
        "last_historical_data": "2020-08-17T23:56:50.593Z",
        "platform": null
      },
      {
        "id": 541,
        "name": "Syscoin",
        "symbol": "SYS",
        "slug": "syscoin",
        "is_active": 1,
        "first_historical_data": "2019-11-28T06:02:04.231Z",
        "last_historical_data": "2020-08-17T22:29:14.119Z",
        "platform": null
      },
      {
        "id": 551,
        "name": "Donu",
        "symbol": "DONU",
        "slug": "donu",
        "is_active": 1,
        "first_historical_data": "2019-09-10T18:49:37.410Z",
        "last_historical_data": "2020-08-18T13:13:05.331Z",
        "platform": null
      },
      {
        "id": 558,
        "name": "Emercoin",
        "symbol": "EMC",
        "slug": "emercoin",
        "is_active": 1,
        "first_historical_data": "2019-08-25T04:13:21.029Z",
        "last_historical_data": "2020-08-17T19:40:07.432Z",
        "platform": null
      },
      {
        "id": 572,
        "name": "RabbitCoin",
        "symbol": "RBBT",
        "slug": "rabbitcoin",
        "is_active": 1,
        "first_historical_data": "2020-01-29T14:42:21.078Z",
        "last_historical_data": "2020-08-18T15:08:12.287Z",
        "platform": null
      },
      {
        "id": 573,
        "name": "Burst",
        "symbol": "BURST",
        "slug": "burst",
        "is_active": 1,
        "first_historical_data": "2020-03-19T17:24:34.652Z",
        "last_historical_data": "2020-08-18T17:14:28.735Z",
        "platform": null
      },
      {
        "id": 576,
        "name": "GameCredits",
        "symbol": "GAME",
        "slug": "gamecredits",
        "is_active": 1,
        "first_historical_data": "2020-05-02T00:42:21.552Z",
        "last_historical_data": "2020-08-18T16:27:55.026Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x63f88a2298a5c4aee3c216aa6d926b184a4b2437"
        }
      },
      {
        "id": 584,
        "name": "NativeCoin",
        "symbol": "N8V",
        "slug": "native-coin",
        "is_active": 1,
        "first_historical_data": "2020-05-22T03:49:33.716Z",
        "last_historical_data": "2020-08-18T10:54:37.193Z",
        "platform": null
      },
      {
        "id": 588,
        "name": "Ubiq",
        "symbol": "UBQ",
        "slug": "ubiq",
        "is_active": 1,
        "first_historical_data": "2019-11-04T08:40:22.846Z",
        "last_historical_data": "2020-08-18T14:59:34.365Z",
        "platform": null
      },
      {
        "id": 597,
        "name": "Opal",
        "symbol": "OPAL",
        "slug": "opal",
        "is_active": 1,
        "first_historical_data": "2019-11-08T12:36:52.816Z",
        "last_historical_data": "2020-08-17T19:55:46.635Z",
        "platform": null
      },
      {
        "id": 601,
        "name": "Acoin",
        "symbol": "ACOIN",
        "slug": "acoin",
        "is_active": 1,
        "first_historical_data": "2019-10-23T10:59:43.068Z",
        "last_historical_data": "2020-08-18T06:44:19.240Z",
        "platform": null
      },
      {
        "id": 623,
        "name": "bitUSD",
        "symbol": "BITUSD",
        "slug": "bitusd",
        "is_active": 1,
        "first_historical_data": "2020-01-23T19:40:11.741Z",
        "last_historical_data": "2020-08-18T05:53:33.111Z",
        "platform": {
          "id": 463,
          "name": "BitShares",
          "symbol": "BTS",
          "slug": "bitshares",
          "token_address": "USD"
        }
      },
      {
        "id": 624,
        "name": "bitCNY",
        "symbol": "BITCNY",
        "slug": "bitcny",
        "is_active": 1,
        "first_historical_data": "2020-06-26T07:39:14.709Z",
        "last_historical_data": "2020-08-18T11:41:48.615Z",
        "platform": {
          "id": 463,
          "name": "BitShares",
          "symbol": "BTS",
          "slug": "bitshares",
          "token_address": "CNY"
        }
      },
      {
        "id": 626,
        "name": "NuBits",
        "symbol": "USNBT",
        "slug": "nubits",
        "is_active": 1,
        "first_historical_data": "2020-01-09T09:45:51.882Z",
        "last_historical_data": "2020-08-17T20:09:21.735Z",
        "platform": null
      },
      {
        "id": 633,
        "name": "ExclusiveCoin",
        "symbol": "EXCL",
        "slug": "exclusivecoin",
        "is_active": 1,
        "first_historical_data": "2020-04-21T02:24:23.628Z",
        "last_historical_data": "2020-08-17T19:59:30.670Z",
        "platform": null
      },
      {
        "id": 638,
        "name": "Trollcoin",
        "symbol": "TROLL",
        "slug": "trollcoin",
        "is_active": 1,
        "first_historical_data": "2019-11-08T22:49:19.505Z",
        "last_historical_data": "2020-08-18T01:03:38.232Z",
        "platform": null
      },
      {
        "id": 644,
        "name": "GlobalBoost-Y",
        "symbol": "BSTY",
        "slug": "globalboost-y",
        "is_active": 1,
        "first_historical_data": "2020-01-26T16:59:52.203Z",
        "last_historical_data": "2020-08-18T12:33:44.556Z",
        "platform": null
      },
      {
        "id": 656,
        "name": "Prime-XI",
        "symbol": "PXI",
        "slug": "prime-xi",
        "is_active": 1,
        "first_historical_data": "2020-04-05T14:28:01.284Z",
        "last_historical_data": "2020-08-18T02:58:57.555Z",
        "platform": null
      },
      {
        "id": 659,
        "name": "Bitswift",
        "symbol": "BITS",
        "slug": "bitswift",
        "is_active": 1,
        "first_historical_data": "2020-07-31T21:33:45.028Z",
        "last_historical_data": "2020-08-17T22:04:14.802Z",
        "platform": {
          "id": 1320,
          "name": "Ardor",
          "symbol": "ARDR",
          "slug": "ardor",
          "token_address": "1.77888E+19"
        }
      },
      {
        "id": 693,
        "name": "Verge",
        "symbol": "XVG",
        "slug": "verge",
        "is_active": 1,
        "first_historical_data": "2019-11-27T07:05:35.745Z",
        "last_historical_data": "2020-08-17T20:36:52.444Z",
        "platform": null
      },
      {
        "id": 699,
        "name": "NuShares",
        "symbol": "NSR",
        "slug": "nushares",
        "is_active": 1,
        "first_historical_data": "2020-07-13T00:29:54.194Z",
        "last_historical_data": "2020-08-18T17:01:58.656Z",
        "platform": {
          "id": 626,
          "name": "NuBits",
          "symbol": "USNBT",
          "slug": "nubits",
          "token_address": ""
        }
      },
      {
        "id": 702,
        "name": "SpreadCoin",
        "symbol": "SPR",
        "slug": "spreadcoin",
        "is_active": 1,
        "first_historical_data": "2019-10-10T08:43:34.643Z",
        "last_historical_data": "2020-08-18T06:39:09.627Z",
        "platform": null
      },
      {
        "id": 703,
        "name": "Rimbit",
        "symbol": "RBT",
        "slug": "rimbit",
        "is_active": 1,
        "first_historical_data": "2020-06-11T06:38:45.129Z",
        "last_historical_data": "2020-08-18T13:30:36.283Z",
        "platform": null
      },
      {
        "id": 706,
        "name": "MonetaryUnit",
        "symbol": "MUE",
        "slug": "monetaryunit",
        "is_active": 1,
        "first_historical_data": "2020-01-21T04:59:44.507Z",
        "last_historical_data": "2020-08-18T07:52:11.005Z",
        "platform": null
      },
      {
        "id": 707,
        "name": "Blocknet",
        "symbol": "BLOCK",
        "slug": "blocknet",
        "is_active": 1,
        "first_historical_data": "2020-06-01T12:34:43.876Z",
        "last_historical_data": "2020-08-18T06:37:54.729Z",
        "platform": null
      },
      {
        "id": 719,
        "name": "Limitless VIP",
        "symbol": "VIP",
        "slug": "limitless-vip",
        "is_active": 1,
        "first_historical_data": "2020-07-23T23:23:19.037Z",
        "last_historical_data": "2020-08-17T20:45:08.021Z",
        "platform": null
      },
      {
        "id": 720,
        "name": "Crown",
        "symbol": "CRW",
        "slug": "crown",
        "is_active": 1,
        "first_historical_data": "2020-04-24T17:00:36.385Z",
        "last_historical_data": "2020-08-17T23:02:27.053Z",
        "platform": null
      },
      {
        "id": 730,
        "name": "GCN Coin",
        "symbol": "GCN",
        "slug": "gcn-coin",
        "is_active": 1,
        "first_historical_data": "2019-09-15T07:20:50.139Z",
        "last_historical_data": "2020-08-18T13:13:44.612Z",
        "platform": null
      },
      {
        "id": 733,
        "name": "Quotient",
        "symbol": "XQN",
        "slug": "quotient",
        "is_active": 1,
        "first_historical_data": "2019-10-29T05:00:57.030Z",
        "last_historical_data": "2020-08-18T04:15:24.610Z",
        "platform": null
      },
      {
        "id": 760,
        "name": "OKCash",
        "symbol": "OK",
        "slug": "okcash",
        "is_active": 1,
        "first_historical_data": "2019-12-21T08:22:49.662Z",
        "last_historical_data": "2020-08-18T09:44:33.061Z",
        "platform": null
      },
      {
        "id": 764,
        "name": "PayCoin",
        "symbol": "XPY",
        "slug": "paycoin2",
        "is_active": 1,
        "first_historical_data": "2019-10-12T00:10:50.884Z",
        "last_historical_data": "2020-08-18T03:03:29.850Z",
        "platform": null
      },
      {
        "id": 778,
        "name": "bitGold",
        "symbol": "BITGOLD",
        "slug": "bitgold",
        "is_active": 1,
        "first_historical_data": "2019-11-20T07:32:13.738Z",
        "last_historical_data": "2020-08-17T22:22:46.490Z",
        "platform": {
          "id": 463,
          "name": "BitShares",
          "symbol": "BTS",
          "slug": "bitshares",
          "token_address": "GOLD"
        }
      },
      {
        "id": 789,
        "name": "Nexus",
        "symbol": "NXS",
        "slug": "nexus",
        "is_active": 1,
        "first_historical_data": "2020-02-18T06:08:01.379Z",
        "last_historical_data": "2020-08-18T00:18:16.540Z",
        "platform": null
      },
      {
        "id": 799,
        "name": "SmileyCoin",
        "symbol": "SMLY",
        "slug": "smileycoin",
        "is_active": 1,
        "first_historical_data": "2019-09-26T12:01:46.045Z",
        "last_historical_data": "2020-08-17T21:13:10.726Z",
        "platform": null
      },
      {
        "id": 813,
        "name": "bitSilver",
        "symbol": "BITSILVER",
        "slug": "bitsilver",
        "is_active": 1,
        "first_historical_data": "2019-09-30T12:48:44.539Z",
        "last_historical_data": "2020-08-18T11:18:18.289Z",
        "platform": {
          "id": 463,
          "name": "BitShares",
          "symbol": "BTS",
          "slug": "bitshares",
          "token_address": "SILVER"
        }
      },
      {
        "id": 815,
        "name": "Kobocoin",
        "symbol": "KOBO",
        "slug": "kobocoin",
        "is_active": 1,
        "first_historical_data": "2020-04-19T23:48:24.992Z",
        "last_historical_data": "2020-08-18T03:03:50.608Z",
        "platform": null
      },
      {
        "id": 819,
        "name": "Bean Cash",
        "symbol": "BITB",
        "slug": "bean-cash",
        "is_active": 1,
        "first_historical_data": "2019-09-06T07:33:48.748Z",
        "last_historical_data": "2020-08-18T14:53:13.763Z",
        "platform": null
      },
      {
        "id": 823,
        "name": "GeoCoin",
        "symbol": "GEO",
        "slug": "geocoin",
        "is_active": 1,
        "first_historical_data": "2019-10-11T00:34:18.981Z",
        "last_historical_data": "2020-08-17T20:03:55.859Z",
        "platform": {
          "id": 588,
          "name": "Ubiq",
          "symbol": "UBQ",
          "slug": "ubiq",
          "token_address": "0x500684ce0d4f04abedff3e54fcf8acc5e6cfc4bd"
        }
      },
      {
        "id": 825,
        "name": "Tether",
        "symbol": "USDT",
        "slug": "tether",
        "is_active": 1,
        "first_historical_data": "2020-07-01T23:16:49.907Z",
        "last_historical_data": "2020-08-17T21:51:35.213Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xdac17f958d2ee523a2206206994597c13d831ec7"
        }
      },
      {
        "id": 831,
        "name": "Wild Beast Block",
        "symbol": "WBB",
        "slug": "wild-beast-block",
        "is_active": 1,
        "first_historical_data": "2020-01-29T17:00:30.432Z",
        "last_historical_data": "2020-08-17T21:04:25.824Z",
        "platform": null
      },
      {
        "id": 833,
        "name": "GridCoin",
        "symbol": "GRC",
        "slug": "gridcoin",
        "is_active": 1,
        "first_historical_data": "2020-01-14T08:18:48.302Z",
        "last_historical_data": "2020-08-17T20:32:45.999Z",
        "platform": null
      },
      {
        "id": 837,
        "name": "X-Coin",
        "symbol": "XCO",
        "slug": "x-coin",
        "is_active": 1,
        "first_historical_data": "2020-01-04T19:55:56.058Z",
        "last_historical_data": "2020-08-18T14:09:38.211Z",
        "platform": null
      },
      {
        "id": 853,
        "name": "LiteDoge",
        "symbol": "LDOGE",
        "slug": "litedoge",
        "is_active": 1,
        "first_historical_data": "2019-11-15T11:43:17.204Z",
        "last_historical_data": "2020-08-18T13:30:50.687Z",
        "platform": null
      },
      {
        "id": 857,
        "name": "SongCoin",
        "symbol": "SONG",
        "slug": "songcoin",
        "is_active": 1,
        "first_historical_data": "2019-11-12T10:26:23.956Z",
        "last_historical_data": "2020-08-18T16:01:08.800Z",
        "platform": null
      },
      {
        "id": 873,
        "name": "NEM",
        "symbol": "XEM",
        "slug": "nem",
        "is_active": 1,
        "first_historical_data": "2019-09-02T07:59:56.649Z",
        "last_historical_data": "2020-08-18T13:57:55.301Z",
        "platform": null
      },
      {
        "id": 894,
        "name": "Neutron",
        "symbol": "NTRN",
        "slug": "neutron",
        "is_active": 1,
        "first_historical_data": "2019-10-12T10:47:43.050Z",
        "last_historical_data": "2020-08-18T09:55:19.813Z",
        "platform": null
      },
      {
        "id": 895,
        "name": "Xaurum",
        "symbol": "XAUR",
        "slug": "xaurum",
        "is_active": 1,
        "first_historical_data": "2020-05-17T15:47:56.027Z",
        "last_historical_data": "2020-08-17T23:54:22.675Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4DF812F6064def1e5e029f1ca858777CC98D2D81"
        }
      },
      {
        "id": 898,
        "name": "Californium",
        "symbol": "CF",
        "slug": "californium",
        "is_active": 1,
        "first_historical_data": "2019-12-11T15:22:54.260Z",
        "last_historical_data": "2020-08-18T07:12:55.072Z",
        "platform": null
      },
      {
        "id": 911,
        "name": "Advanced Internet Blocks",
        "symbol": "AIB",
        "slug": "advanced-internet-blocks",
        "is_active": 1,
        "first_historical_data": "2020-05-16T13:09:24.925Z",
        "last_historical_data": "2020-08-18T00:05:22.253Z",
        "platform": null
      },
      {
        "id": 914,
        "name": "Sphere",
        "symbol": "SPHR",
        "slug": "sphere",
        "is_active": 1,
        "first_historical_data": "2020-04-24T12:20:09.428Z",
        "last_historical_data": "2020-08-17T22:37:20.297Z",
        "platform": null
      },
      {
        "id": 916,
        "name": "MedicCoin",
        "symbol": "MEDIC",
        "slug": "mediccoin",
        "is_active": 1,
        "first_historical_data": "2019-12-23T16:17:42.049Z",
        "last_historical_data": "2020-08-18T01:11:41.753Z",
        "platform": null
      },
      {
        "id": 918,
        "name": "Bubble",
        "symbol": "BUB",
        "slug": "bubble",
        "is_active": 1,
        "first_historical_data": "2019-09-07T23:41:44.631Z",
        "last_historical_data": "2020-08-18T08:50:04.781Z",
        "platform": null
      },
      {
        "id": 920,
        "name": "SounDAC",
        "symbol": "XSD",
        "slug": "bitshares-music",
        "is_active": 1,
        "first_historical_data": "2020-05-09T20:04:24.056Z",
        "last_historical_data": "2020-08-17T20:32:19.658Z",
        "platform": null
      },
      {
        "id": 921,
        "name": "Universal Currency",
        "symbol": "UNIT",
        "slug": "universal-currency",
        "is_active": 1,
        "first_historical_data": "2020-03-18T16:12:11.288Z",
        "last_historical_data": "2020-08-18T04:25:38.762Z",
        "platform": null
      },
      {
        "id": 934,
        "name": "ParkByte",
        "symbol": "PKB",
        "slug": "parkbyte",
        "is_active": 1,
        "first_historical_data": "2019-10-22T04:18:16.836Z",
        "last_historical_data": "2020-08-18T04:53:13.509Z",
        "platform": null
      },
      {
        "id": 938,
        "name": "ARbit",
        "symbol": "ARB",
        "slug": "arbit",
        "is_active": 1,
        "first_historical_data": "2019-11-24T05:45:39.554Z",
        "last_historical_data": "2020-08-18T17:05:41.313Z",
        "platform": null
      },
      {
        "id": 945,
        "name": "Bata",
        "symbol": "BTA",
        "slug": "bata",
        "is_active": 1,
        "first_historical_data": "2019-09-05T04:37:40.564Z",
        "last_historical_data": "2020-08-18T10:04:42.802Z",
        "platform": null
      },
      {
        "id": 948,
        "name": "AudioCoin",
        "symbol": "ADC",
        "slug": "audiocoin",
        "is_active": 1,
        "first_historical_data": "2020-04-26T12:07:16.441Z",
        "last_historical_data": "2020-08-18T17:13:06.956Z",
        "platform": null
      },
      {
        "id": 951,
        "name": "Synergy",
        "symbol": "SNRG",
        "slug": "synergy",
        "is_active": 1,
        "first_historical_data": "2020-06-15T11:46:11.717Z",
        "last_historical_data": "2020-08-18T02:56:36.539Z",
        "platform": null
      },
      {
        "id": 954,
        "name": "bitEUR",
        "symbol": "BITEUR",
        "slug": "biteur",
        "is_active": 1,
        "first_historical_data": "2020-08-06T13:28:41.973Z",
        "last_historical_data": "2020-08-18T10:52:09.194Z",
        "platform": {
          "id": 463,
          "name": "BitShares",
          "symbol": "BTS",
          "slug": "bitshares",
          "token_address": "EUR"
        }
      },
      {
        "id": 960,
        "name": "FujiCoin",
        "symbol": "FJC",
        "slug": "fujicoin",
        "is_active": 1,
        "first_historical_data": "2019-10-22T12:52:52.746Z",
        "last_historical_data": "2020-08-18T11:42:11.488Z",
        "platform": null
      },
      {
        "id": 977,
        "name": "GravityCoin",
        "symbol": "GXX",
        "slug": "gravitycoin",
        "is_active": 1,
        "first_historical_data": "2020-04-17T08:20:53.754Z",
        "last_historical_data": "2020-08-18T12:38:08.756Z",
        "platform": null
      },
      {
        "id": 978,
        "name": "Ratecoin",
        "symbol": "XRA",
        "slug": "ratecoin",
        "is_active": 1,
        "first_historical_data": "2019-10-02T01:23:38.415Z",
        "last_historical_data": "2020-08-18T09:58:20.972Z",
        "platform": null
      },
      {
        "id": 986,
        "name": "CrevaCoin",
        "symbol": "CREVA",
        "slug": "crevacoin",
        "is_active": 1,
        "first_historical_data": "2019-11-26T15:29:12.262Z",
        "last_historical_data": "2020-08-18T08:40:28.041Z",
        "platform": null
      },
      {
        "id": 990,
        "name": "Bitzeny",
        "symbol": "ZNY",
        "slug": "bitzeny",
        "is_active": 1,
        "first_historical_data": "2020-04-08T00:52:06.979Z",
        "last_historical_data": "2020-08-18T02:09:19.316Z",
        "platform": null
      },
      {
        "id": 993,
        "name": "BowsCoin",
        "symbol": "BSC",
        "slug": "bowscoin",
        "is_active": 1,
        "first_historical_data": "2019-12-22T01:46:29.357Z",
        "last_historical_data": "2020-08-18T14:40:40.700Z",
        "platform": null
      },
      {
        "id": 1004,
        "name": "Hellenic Coin",
        "symbol": "HNC",
        "slug": "helleniccoin",
        "is_active": 1,
        "first_historical_data": "2020-08-13T02:05:34.060Z",
        "last_historical_data": "2020-08-18T10:30:59.502Z",
        "platform": null
      },
      {
        "id": 1008,
        "name": "Capricoin+",
        "symbol": "CPS",
        "slug": "capricoin",
        "is_active": 1,
        "first_historical_data": "2020-04-01T12:34:38.900Z",
        "last_historical_data": "2020-08-17T19:58:28.939Z",
        "platform": null
      },
      {
        "id": 1019,
        "name": "Manna",
        "symbol": "MANNA",
        "slug": "manna",
        "is_active": 1,
        "first_historical_data": "2019-12-04T18:51:17.675Z",
        "last_historical_data": "2020-08-18T13:58:25.421Z",
        "platform": null
      },
      {
        "id": 1020,
        "name": "Axiom",
        "symbol": "AXIOM",
        "slug": "axiom",
        "is_active": 1,
        "first_historical_data": "2019-10-25T00:26:32.931Z",
        "last_historical_data": "2020-08-18T02:54:28.981Z",
        "platform": null
      },
      {
        "id": 1022,
        "name": "LEOcoin",
        "symbol": "LC4",
        "slug": "leocoin",
        "is_active": 1,
        "first_historical_data": "2019-12-26T03:25:34.198Z",
        "last_historical_data": "2020-08-18T16:49:15.862Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa83af809975619477af73b179e05e04a1ccea953"
        }
      },
      {
        "id": 1026,
        "name": "Aeon",
        "symbol": "AEON",
        "slug": "aeon",
        "is_active": 1,
        "first_historical_data": "2020-01-13T00:36:45.954Z",
        "last_historical_data": "2020-08-18T04:41:47.466Z",
        "platform": null
      },
      {
        "id": 1027,
        "name": "Ethereum",
        "symbol": "ETH",
        "slug": "ethereum",
        "is_active": 1,
        "first_historical_data": "2020-05-14T18:10:17.645Z",
        "last_historical_data": "2020-08-18T01:24:35.221Z",
        "platform": null
      },
      {
        "id": 1032,
        "name": "TransferCoin",
        "symbol": "TX",
        "slug": "transfercoin",
        "is_active": 1,
        "first_historical_data": "2020-03-16T19:34:18.373Z",
        "last_historical_data": "2020-08-18T08:50:05.985Z",
        "platform": null
      },
      {
        "id": 1033,
        "name": "GuccioneCoin",
        "symbol": "GCC",
        "slug": "guccionecoin",
        "is_active": 1,
        "first_historical_data": "2020-03-07T13:58:08.151Z",
        "last_historical_data": "2020-08-18T06:24:07.956Z",
        "platform": null
      },
      {
        "id": 1035,
        "name": "AmsterdamCoin",
        "symbol": "AMS",
        "slug": "amsterdamcoin",
        "is_active": 1,
        "first_historical_data": "2020-01-14T03:13:01.931Z",
        "last_historical_data": "2020-08-17T20:40:19.777Z",
        "platform": null
      },
      {
        "id": 1037,
        "name": "Agoras Tokens",
        "symbol": "AGRS",
        "slug": "agoras-tokens",
        "is_active": 1,
        "first_historical_data": "2020-06-14T13:15:35.926Z",
        "last_historical_data": "2020-08-18T05:22:17.521Z",
        "platform": {
          "id": 83,
          "name": "Omni",
          "symbol": "OMNI",
          "slug": "omni",
          "token_address": "58"
        }
      },
      {
        "id": 1038,
        "name": "Eurocoin",
        "symbol": "EUC",
        "slug": "eurocoin",
        "is_active": 1,
        "first_historical_data": "2019-11-26T21:14:00.601Z",
        "last_historical_data": "2020-08-18T13:46:29.701Z",
        "platform": null
      },
      {
        "id": 1042,
        "name": "Siacoin",
        "symbol": "SC",
        "slug": "siacoin",
        "is_active": 1,
        "first_historical_data": "2020-01-08T11:11:04.154Z",
        "last_historical_data": "2020-08-18T06:10:43.168Z",
        "platform": null
      },
      {
        "id": 1044,
        "name": "Global Currency Reserve",
        "symbol": "GCR",
        "slug": "global-currency-reserve",
        "is_active": 1,
        "first_historical_data": "2020-07-13T01:29:18.242Z",
        "last_historical_data": "2020-08-18T11:13:19.568Z",
        "platform": null
      },
      {
        "id": 1050,
        "name": "Shift",
        "symbol": "SHIFT",
        "slug": "shift",
        "is_active": 1,
        "first_historical_data": "2020-04-19T04:58:10.128Z",
        "last_historical_data": "2020-08-18T06:25:39.892Z",
        "platform": null
      },
      {
        "id": 1052,
        "name": "VectorAI",
        "symbol": "VEC2",
        "slug": "vector",
        "is_active": 1,
        "first_historical_data": "2020-02-19T13:17:34.552Z",
        "last_historical_data": "2020-08-18T02:14:32.885Z",
        "platform": null
      },
      {
        "id": 1053,
        "name": "Bolivarcoin",
        "symbol": "BOLI",
        "slug": "bolivarcoin",
        "is_active": 1,
        "first_historical_data": "2020-06-26T10:28:43.639Z",
        "last_historical_data": "2020-08-18T10:16:32.078Z",
        "platform": null
      },
      {
        "id": 1063,
        "name": "BitCrystals",
        "symbol": "BCY",
        "slug": "bitcrystals",
        "is_active": 1,
        "first_historical_data": "2019-09-21T20:58:59.209Z",
        "last_historical_data": "2020-08-18T05:47:35.494Z",
        "platform": {
          "id": 132,
          "name": "Counterparty",
          "symbol": "XCP",
          "slug": "counterparty",
          "token_address": "BITCRYSTALS"
        }
      },
      {
        "id": 1066,
        "name": "Pakcoin",
        "symbol": "PAK",
        "slug": "pakcoin",
        "is_active": 1,
        "first_historical_data": "2019-08-23T10:23:57.189Z",
        "last_historical_data": "2020-08-18T01:52:54.424Z",
        "platform": null
      },
      {
        "id": 1070,
        "name": "Expanse",
        "symbol": "EXP",
        "slug": "expanse",
        "is_active": 1,
        "first_historical_data": "2020-04-16T19:23:55.948Z",
        "last_historical_data": "2020-08-18T10:04:31.127Z",
        "platform": null
      },
      {
        "id": 1082,
        "name": "SIBCoin",
        "symbol": "SIB",
        "slug": "sibcoin",
        "is_active": 1,
        "first_historical_data": "2020-07-29T16:55:34.686Z",
        "last_historical_data": "2020-08-17T22:23:04.524Z",
        "platform": null
      },
      {
        "id": 1085,
        "name": "Swing",
        "symbol": "SWING",
        "slug": "swing",
        "is_active": 1,
        "first_historical_data": "2020-01-31T22:26:11.192Z",
        "last_historical_data": "2020-08-18T17:22:16.128Z",
        "platform": null
      },
      {
        "id": 1087,
        "name": "Factom",
        "symbol": "FCT",
        "slug": "factom",
        "is_active": 1,
        "first_historical_data": "2020-01-05T22:35:33.937Z",
        "last_historical_data": "2020-08-17T19:37:06.334Z",
        "platform": null
      },
      {
        "id": 1089,
        "name": "ParallelCoin",
        "symbol": "DUO",
        "slug": "parallelcoin",
        "is_active": 1,
        "first_historical_data": "2020-03-06T03:01:48.421Z",
        "last_historical_data": "2020-08-17T19:51:37.234Z",
        "platform": null
      },
      {
        "id": 1090,
        "name": "Save and Gain",
        "symbol": "SANDG",
        "slug": "save-and-gain",
        "is_active": 1,
        "first_historical_data": "2020-02-18T00:33:03.916Z",
        "last_historical_data": "2020-08-18T12:10:37.172Z",
        "platform": null
      },
      {
        "id": 1104,
        "name": "Augur",
        "symbol": "REP",
        "slug": "augur",
        "is_active": 1,
        "first_historical_data": "2019-09-13T06:22:28.295Z",
        "last_historical_data": "2020-08-17T19:30:44.616Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1985365e9f78359a9b6ad760e32412f4a445e862"
        }
      },
      {
        "id": 1106,
        "name": "StrongHands",
        "symbol": "SHND",
        "slug": "stronghands",
        "is_active": 1,
        "first_historical_data": "2020-07-18T14:55:10.389Z",
        "last_historical_data": "2020-08-18T03:05:44.998Z",
        "platform": null
      },
      {
        "id": 1107,
        "name": "PAC Global",
        "symbol": "PAC",
        "slug": "pac-global",
        "is_active": 1,
        "first_historical_data": "2020-04-07T15:45:50.433Z",
        "last_historical_data": "2020-08-18T08:25:24.900Z",
        "platform": null
      },
      {
        "id": 1120,
        "name": "DraftCoin",
        "symbol": "DFT",
        "slug": "draftcoin",
        "is_active": 1,
        "first_historical_data": "2020-01-06T04:36:32.558Z",
        "last_historical_data": "2020-08-18T05:17:11.159Z",
        "platform": null
      },
      {
        "id": 1123,
        "name": "OBITS",
        "symbol": "OBITS",
        "slug": "obits",
        "is_active": 1,
        "first_historical_data": "2020-04-17T13:46:29.053Z",
        "last_historical_data": "2020-08-17T19:49:50.697Z",
        "platform": {
          "id": 463,
          "name": "BitShares",
          "symbol": "BTS",
          "slug": "bitshares",
          "token_address": "OBITS"
        }
      },
      {
        "id": 1135,
        "name": "ClubCoin",
        "symbol": "CLUB",
        "slug": "clubcoin",
        "is_active": 1,
        "first_historical_data": "2020-01-09T17:06:36.562Z",
        "last_historical_data": "2020-08-18T16:02:54.831Z",
        "platform": null
      },
      {
        "id": 1136,
        "name": "Adzcoin",
        "symbol": "ADZ",
        "slug": "adzcoin",
        "is_active": 1,
        "first_historical_data": "2020-05-14T17:34:12.405Z",
        "last_historical_data": "2020-08-18T10:43:55.620Z",
        "platform": null
      },
      {
        "id": 1141,
        "name": "Moin",
        "symbol": "MOIN",
        "slug": "moin",
        "is_active": 1,
        "first_historical_data": "2019-10-31T01:29:33.236Z",
        "last_historical_data": "2020-08-18T13:22:09.169Z",
        "platform": null
      },
      {
        "id": 1146,
        "name": "AvatarCoin",
        "symbol": "AV",
        "slug": "avatarcoin",
        "is_active": 1,
        "first_historical_data": "2019-10-24T07:02:15.105Z",
        "last_historical_data": "2020-08-17T23:30:04.352Z",
        "platform": null
      },
      {
        "id": 1148,
        "name": "EverGreenCoin",
        "symbol": "EGC",
        "slug": "evergreencoin",
        "is_active": 1,
        "first_historical_data": "2020-04-27T13:31:39.499Z",
        "last_historical_data": "2020-08-17T21:42:45.344Z",
        "platform": null
      },
      {
        "id": 1154,
        "name": "Radium",
        "symbol": "RADS",
        "slug": "radium",
        "is_active": 1,
        "first_historical_data": "2020-06-10T09:41:01.246Z",
        "last_historical_data": "2020-08-17T20:18:47.292Z",
        "platform": null
      },
      {
        "id": 1155,
        "name": "Litecred",
        "symbol": "LTCR",
        "slug": "litecred",
        "is_active": 1,
        "first_historical_data": "2020-06-01T23:18:08.713Z",
        "last_historical_data": "2020-08-18T09:10:17.253Z",
        "platform": null
      },
      {
        "id": 1156,
        "name": "Yocoin",
        "symbol": "YOC",
        "slug": "yocoin",
        "is_active": 1,
        "first_historical_data": "2020-02-07T03:20:45.584Z",
        "last_historical_data": "2020-08-18T09:26:40.015Z",
        "platform": null
      },
      {
        "id": 1159,
        "name": "SaluS",
        "symbol": "SLS",
        "slug": "salus",
        "is_active": 1,
        "first_historical_data": "2019-11-18T19:03:10.590Z",
        "last_historical_data": "2020-08-17T19:30:43.318Z",
        "platform": null
      },
      {
        "id": 1164,
        "name": "Francs",
        "symbol": "FRN",
        "slug": "francs",
        "is_active": 1,
        "first_historical_data": "2019-10-01T19:37:55.028Z",
        "last_historical_data": "2020-08-18T13:29:06.922Z",
        "platform": null
      },
      {
        "id": 1165,
        "name": "Evil Coin",
        "symbol": "EVIL",
        "slug": "evil-coin",
        "is_active": 1,
        "first_historical_data": "2020-05-03T23:16:06.019Z",
        "last_historical_data": "2020-08-18T17:23:29.996Z",
        "platform": null
      },
      {
        "id": 1168,
        "name": "Decred",
        "symbol": "DCR",
        "slug": "decred",
        "is_active": 1,
        "first_historical_data": "2020-03-11T18:05:59.558Z",
        "last_historical_data": "2020-08-18T13:54:50.951Z",
        "platform": null
      },
      {
        "id": 1169,
        "name": "PIVX",
        "symbol": "PIVX",
        "slug": "pivx",
        "is_active": 1,
        "first_historical_data": "2019-12-27T14:37:29.485Z",
        "last_historical_data": "2020-08-18T12:42:27.981Z",
        "platform": null
      },
      {
        "id": 1172,
        "name": "Safex Token",
        "symbol": "SFT",
        "slug": "safex-token",
        "is_active": 1,
        "first_historical_data": "2020-05-18T11:03:42.792Z",
        "last_historical_data": "2020-08-18T01:58:41.326Z",
        "platform": null
      },
      {
        "id": 1175,
        "name": "Rubies",
        "symbol": "RBIES",
        "slug": "rubies",
        "is_active": 1,
        "first_historical_data": "2020-07-31T05:29:29.287Z",
        "last_historical_data": "2020-08-18T05:13:19.501Z",
        "platform": null
      },
      {
        "id": 1185,
        "name": "TrumpCoin",
        "symbol": "TRUMP",
        "slug": "trumpcoin",
        "is_active": 1,
        "first_historical_data": "2019-10-30T23:01:08.298Z",
        "last_historical_data": "2020-08-18T02:45:39.129Z",
        "platform": null
      },
      {
        "id": 1191,
        "name": "Memetic / PepeCoin",
        "symbol": "MEME",
        "slug": "memetic",
        "is_active": 1,
        "first_historical_data": "2020-06-20T10:51:51.446Z",
        "last_historical_data": "2020-08-17T17:59:24.001Z",
        "platform": null
      },
      {
        "id": 1194,
        "name": "Independent Money System",
        "symbol": "IMS",
        "slug": "independent-money-system",
        "is_active": 1,
        "first_historical_data": "2020-03-08T17:41:48.834Z",
        "last_historical_data": "2020-08-18T16:34:51.058Z",
        "platform": null
      },
      {
        "id": 1200,
        "name": "NevaCoin",
        "symbol": "NEVA",
        "slug": "nevacoin",
        "is_active": 1,
        "first_historical_data": "2020-08-16T18:55:56.216Z",
        "last_historical_data": "2020-08-17T18:58:38.613Z",
        "platform": null
      },
      {
        "id": 1209,
        "name": "PosEx",
        "symbol": "PEX",
        "slug": "posex",
        "is_active": 1,
        "first_historical_data": "2020-06-11T06:23:59.408Z",
        "last_historical_data": "2020-08-18T08:48:27.462Z",
        "platform": null
      },
      {
        "id": 1210,
        "name": "Cabbage",
        "symbol": "CAB",
        "slug": "cabbage",
        "is_active": 1,
        "first_historical_data": "2019-11-07T10:28:28.467Z",
        "last_historical_data": "2020-08-18T15:21:13.841Z",
        "platform": null
      },
      {
        "id": 1212,
        "name": "MojoCoin",
        "symbol": "MOJO",
        "slug": "mojocoin",
        "is_active": 1,
        "first_historical_data": "2020-01-18T15:50:49.812Z",
        "last_historical_data": "2020-08-18T02:19:46.100Z",
        "platform": null
      },
      {
        "id": 1214,
        "name": "Lisk",
        "symbol": "LSK",
        "slug": "lisk",
        "is_active": 1,
        "first_historical_data": "2019-11-10T20:32:11.467Z",
        "last_historical_data": "2020-08-18T04:42:35.533Z",
        "platform": null
      },
      {
        "id": 1216,
        "name": "EDRCoin",
        "symbol": "EDRC",
        "slug": "edrcoin",
        "is_active": 1,
        "first_historical_data": "2020-04-26T09:36:40.760Z",
        "last_historical_data": "2020-08-18T01:32:05.998Z",
        "platform": null
      },
      {
        "id": 1218,
        "name": "PostCoin",
        "symbol": "POST",
        "slug": "postcoin",
        "is_active": 1,
        "first_historical_data": "2020-07-22T14:15:40.444Z",
        "last_historical_data": "2020-08-18T03:58:42.473Z",
        "platform": null
      },
      {
        "id": 1223,
        "name": "BERNcash",
        "symbol": "BERN",
        "slug": "berncash",
        "is_active": 1,
        "first_historical_data": "2020-04-27T02:42:39.323Z",
        "last_historical_data": "2020-08-18T06:55:56.099Z",
        "platform": null
      },
      {
        "id": 1229,
        "name": "DigixDAO",
        "symbol": "DGD",
        "slug": "digixdao",
        "is_active": 1,
        "first_historical_data": "2020-07-17T07:24:27.043Z",
        "last_historical_data": "2020-08-18T11:43:47.508Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe0b7927c4af23765cb51314a0e0521a9645f0e2a"
        }
      },
      {
        "id": 1230,
        "name": "Steem",
        "symbol": "STEEM",
        "slug": "steem",
        "is_active": 1,
        "first_historical_data": "2020-07-25T07:24:19.213Z",
        "last_historical_data": "2020-08-18T12:58:28.122Z",
        "platform": null
      },
      {
        "id": 1238,
        "name": "Espers",
        "symbol": "ESP",
        "slug": "espers",
        "is_active": 1,
        "first_historical_data": "2019-11-14T22:25:06.154Z",
        "last_historical_data": "2020-08-17T21:56:35.033Z",
        "platform": null
      },
      {
        "id": 1241,
        "name": "FuzzBalls",
        "symbol": "FUZZ",
        "slug": "fuzzballs",
        "is_active": 1,
        "first_historical_data": "2019-12-14T05:06:14.393Z",
        "last_historical_data": "2020-08-18T16:26:44.711Z",
        "platform": null
      },
      {
        "id": 1244,
        "name": "HiCoin",
        "symbol": "XHI",
        "slug": "hicoin",
        "is_active": 1,
        "first_historical_data": "2019-10-14T04:05:08.903Z",
        "last_historical_data": "2020-08-18T17:48:19.767Z",
        "platform": null
      },
      {
        "id": 1247,
        "name": "AquariusCoin",
        "symbol": "ARCO",
        "slug": "aquariuscoin",
        "is_active": 1,
        "first_historical_data": "2020-03-07T14:01:51.964Z",
        "last_historical_data": "2020-08-18T11:41:08.105Z",
        "platform": null
      },
      {
        "id": 1248,
        "name": "Bitcoin 21",
        "symbol": "XBTC21",
        "slug": "bitcoin-21",
        "is_active": 1,
        "first_historical_data": "2020-01-22T07:17:02.325Z",
        "last_historical_data": "2020-08-17T22:16:22.388Z",
        "platform": null
      },
      {
        "id": 1249,
        "name": "Elcoin",
        "symbol": "EL",
        "slug": "elcoin-el",
        "is_active": 1,
        "first_historical_data": "2019-11-24T04:55:31.056Z",
        "last_historical_data": "2020-08-18T12:46:54.743Z",
        "platform": null
      },
      {
        "id": 1250,
        "name": "Zurcoin",
        "symbol": "ZUR",
        "slug": "zurcoin",
        "is_active": 1,
        "first_historical_data": "2019-09-15T04:31:08.569Z",
        "last_historical_data": "2020-08-18T05:01:28.510Z",
        "platform": null
      },
      {
        "id": 1252,
        "name": "2GIVE",
        "symbol": "2GIVE",
        "slug": "2give",
        "is_active": 1,
        "first_historical_data": "2020-03-16T10:14:48.011Z",
        "last_historical_data": "2020-08-18T11:03:32.246Z",
        "platform": null
      },
      {
        "id": 1254,
        "name": "PlatinumBAR",
        "symbol": "XPTX",
        "slug": "platinumbar",
        "is_active": 1,
        "first_historical_data": "2020-03-09T11:32:20.942Z",
        "last_historical_data": "2020-08-17T22:00:52.865Z",
        "platform": null
      },
      {
        "id": 1257,
        "name": "LanaCoin",
        "symbol": "LANA",
        "slug": "lanacoin",
        "is_active": 1,
        "first_historical_data": "2020-05-28T05:00:37.341Z",
        "last_historical_data": "2020-08-18T12:34:01.411Z",
        "platform": null
      },
      {
        "id": 1259,
        "name": "PonziCoin",
        "symbol": "PONZI",
        "slug": "ponzicoin",
        "is_active": 1,
        "first_historical_data": "2020-02-22T23:50:02.865Z",
        "last_historical_data": "2020-08-18T16:05:58.503Z",
        "platform": null
      },
      {
        "id": 1266,
        "name": "MarteXcoin",
        "symbol": "MXT",
        "slug": "martexcoin",
        "is_active": 1,
        "first_historical_data": "2020-05-23T16:18:53.241Z",
        "last_historical_data": "2020-08-18T00:28:36.931Z",
        "platform": null
      },
      {
        "id": 1273,
        "name": "Citadel",
        "symbol": "CTL",
        "slug": "citadel",
        "is_active": 1,
        "first_historical_data": "2020-03-02T15:21:25.004Z",
        "last_historical_data": "2020-08-17T19:06:08.276Z",
        "platform": null
      },
      {
        "id": 1274,
        "name": "Waves",
        "symbol": "WAVES",
        "slug": "waves",
        "is_active": 1,
        "first_historical_data": "2020-05-13T02:55:17.456Z",
        "last_historical_data": "2020-08-17T21:56:18.312Z",
        "platform": null
      },
      {
        "id": 1279,
        "name": "PWR Coin",
        "symbol": "PWR",
        "slug": "powercoin",
        "is_active": 1,
        "first_historical_data": "2020-04-29T09:58:45.634Z",
        "last_historical_data": "2020-08-18T01:15:06.698Z",
        "platform": null
      },
      {
        "id": 1281,
        "name": "ION",
        "symbol": "ION",
        "slug": "ion",
        "is_active": 1,
        "first_historical_data": "2019-09-21T11:23:01.775Z",
        "last_historical_data": "2020-08-18T15:39:01.957Z",
        "platform": null
      },
      {
        "id": 1282,
        "name": "High Voltage",
        "symbol": "HVCO",
        "slug": "high-voltage",
        "is_active": 1,
        "first_historical_data": "2020-01-03T19:21:31.234Z",
        "last_historical_data": "2020-08-18T14:56:45.675Z",
        "platform": null
      },
      {
        "id": 1285,
        "name": "GoldBlocks",
        "symbol": "GB",
        "slug": "goldblocks",
        "is_active": 1,
        "first_historical_data": "2020-08-16T19:59:02.271Z",
        "last_historical_data": "2020-08-18T00:51:17.107Z",
        "platform": null
      },
      {
        "id": 1291,
        "name": "Comet",
        "symbol": "CMT",
        "slug": "comet",
        "is_active": 1,
        "first_historical_data": "2019-10-06T23:07:32.818Z",
        "last_historical_data": "2020-08-18T13:12:31.469Z",
        "platform": null
      },
      {
        "id": 1294,
        "name": "Rise",
        "symbol": "RISE",
        "slug": "rise",
        "is_active": 1,
        "first_historical_data": "2020-02-07T15:09:32.613Z",
        "last_historical_data": "2020-08-18T11:38:48.146Z",
        "platform": null
      },
      {
        "id": 1297,
        "name": "ChessCoin",
        "symbol": "CHESS",
        "slug": "chesscoin",
        "is_active": 1,
        "first_historical_data": "2019-09-08T03:19:11.171Z",
        "last_historical_data": "2020-08-17T21:27:12.176Z",
        "platform": null
      },
      {
        "id": 1298,
        "name": "LBRY Credits",
        "symbol": "LBC",
        "slug": "library-credit",
        "is_active": 1,
        "first_historical_data": "2020-04-17T15:46:14.590Z",
        "last_historical_data": "2020-08-17T21:25:18.868Z",
        "platform": null
      },
      {
        "id": 1299,
        "name": "PutinCoin",
        "symbol": "PUT",
        "slug": "putincoin",
        "is_active": 1,
        "first_historical_data": "2020-06-06T02:18:54.317Z",
        "last_historical_data": "2020-08-17T18:42:59.228Z",
        "platform": null
      },
      {
        "id": 1306,
        "name": "Cryptojacks",
        "symbol": "CJ",
        "slug": "cryptojacks",
        "is_active": 1,
        "first_historical_data": "2020-01-04T07:31:35.570Z",
        "last_historical_data": "2020-08-17T22:38:50.870Z",
        "platform": null
      },
      {
        "id": 1308,
        "name": "HEAT",
        "symbol": "HEAT",
        "slug": "heat-ledger",
        "is_active": 1,
        "first_historical_data": "2020-02-02T00:11:09.077Z",
        "last_historical_data": "2020-08-18T14:44:31.525Z",
        "platform": null
      },
      {
        "id": 1312,
        "name": "Steem Dollars",
        "symbol": "SBD",
        "slug": "steem-dollars",
        "is_active": 1,
        "first_historical_data": "2019-12-12T09:33:45.696Z",
        "last_historical_data": "2020-08-18T04:24:38.645Z",
        "platform": null
      },
      {
        "id": 1320,
        "name": "Ardor",
        "symbol": "ARDR",
        "slug": "ardor",
        "is_active": 1,
        "first_historical_data": "2019-09-25T00:23:35.673Z",
        "last_historical_data": "2020-08-18T00:48:43.705Z",
        "platform": null
      },
      {
        "id": 1321,
        "name": "Ethereum Classic",
        "symbol": "ETC",
        "slug": "ethereum-classic",
        "is_active": 1,
        "first_historical_data": "2019-11-05T03:37:39.627Z",
        "last_historical_data": "2020-08-18T01:26:47.622Z",
        "platform": null
      },
      {
        "id": 1323,
        "name": "First Bitcoin",
        "symbol": "BIT",
        "slug": "first-bitcoin",
        "is_active": 1,
        "first_historical_data": "2020-02-22T05:30:04.898Z",
        "last_historical_data": "2020-08-17T20:54:13.291Z",
        "platform": null
      },
      {
        "id": 1334,
        "name": "Elementrem",
        "symbol": "ELE",
        "slug": "elementrem",
        "is_active": 1,
        "first_historical_data": "2020-07-31T22:44:58.933Z",
        "last_historical_data": "2020-08-18T14:34:28.299Z",
        "platform": null
      },
      {
        "id": 1340,
        "name": "Karbo",
        "symbol": "KRB",
        "slug": "karbo",
        "is_active": 1,
        "first_historical_data": "2020-01-15T15:14:27.743Z",
        "last_historical_data": "2020-08-18T00:35:34.005Z",
        "platform": null
      },
      {
        "id": 1343,
        "name": "Stratis",
        "symbol": "STRAT",
        "slug": "stratis",
        "is_active": 1,
        "first_historical_data": "2020-03-03T06:27:21.183Z",
        "last_historical_data": "2020-08-17T18:22:02.584Z",
        "platform": null
      },
      {
        "id": 1351,
        "name": "Aces",
        "symbol": "ACES",
        "slug": "aces",
        "is_active": 1,
        "first_historical_data": "2020-05-24T09:03:10.766Z",
        "last_historical_data": "2020-08-17T20:21:20.239Z",
        "platform": null
      },
      {
        "id": 1353,
        "name": "TajCoin",
        "symbol": "TAJ",
        "slug": "tajcoin",
        "is_active": 1,
        "first_historical_data": "2019-09-05T20:11:47.159Z",
        "last_historical_data": "2020-08-18T10:11:03.148Z",
        "platform": null
      },
      {
        "id": 1358,
        "name": "EDC Blockchain [old]",
        "symbol": "EDC",
        "slug": "edc-blockchain-old",
        "is_active": 1,
        "first_historical_data": "2020-05-06T19:20:11.910Z",
        "last_historical_data": "2020-08-18T03:35:58.497Z",
        "platform": null
      },
      {
        "id": 1367,
        "name": "Experience Points",
        "symbol": "XP",
        "slug": "experience-points",
        "is_active": 1,
        "first_historical_data": "2020-01-04T12:36:32.265Z",
        "last_historical_data": "2020-08-17T18:02:51.124Z",
        "platform": null
      },
      {
        "id": 1368,
        "name": "Veltor",
        "symbol": "VLT",
        "slug": "veltor",
        "is_active": 1,
        "first_historical_data": "2020-06-04T10:08:45.734Z",
        "last_historical_data": "2020-08-18T04:32:13.909Z",
        "platform": null
      },
      {
        "id": 1376,
        "name": "Neo",
        "symbol": "NEO",
        "slug": "neo",
        "is_active": 1,
        "first_historical_data": "2019-10-16T09:33:15.647Z",
        "last_historical_data": "2020-08-18T13:32:11.766Z",
        "platform": null
      },
      {
        "id": 1381,
        "name": "Bitcloud",
        "symbol": "BTDX",
        "slug": "bitcloud",
        "is_active": 1,
        "first_historical_data": "2019-10-19T11:01:43.000Z",
        "last_historical_data": "2020-08-17T22:35:00.359Z",
        "platform": null
      },
      {
        "id": 1382,
        "name": "NoLimitCoin",
        "symbol": "NLC2",
        "slug": "nolimitcoin",
        "is_active": 1,
        "first_historical_data": "2019-11-16T12:43:23.323Z",
        "last_historical_data": "2020-08-18T16:52:17.941Z",
        "platform": null
      },
      {
        "id": 1387,
        "name": "VeriumReserve",
        "symbol": "VRM",
        "slug": "veriumreserve",
        "is_active": 1,
        "first_historical_data": "2020-01-29T15:00:28.359Z",
        "last_historical_data": "2020-08-18T08:36:57.329Z",
        "platform": null
      },
      {
        "id": 1389,
        "name": "Zayedcoin",
        "symbol": "ZYD",
        "slug": "zayedcoin",
        "is_active": 1,
        "first_historical_data": "2019-09-18T00:09:11.922Z",
        "last_historical_data": "2020-08-18T10:49:21.777Z",
        "platform": null
      },
      {
        "id": 1392,
        "name": "Pluton",
        "symbol": "PLU",
        "slug": "pluton",
        "is_active": 1,
        "first_historical_data": "2020-07-02T16:27:34.260Z",
        "last_historical_data": "2020-08-18T13:50:29.181Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xD8912C10681D8B21Fd3742244f44658dBA12264E"
        }
      },
      {
        "id": 1395,
        "name": "Dollarcoin",
        "symbol": "DLC",
        "slug": "dollarcoin",
        "is_active": 1,
        "first_historical_data": "2020-08-07T00:03:45.791Z",
        "last_historical_data": "2020-08-18T07:14:21.314Z",
        "platform": null
      },
      {
        "id": 1396,
        "name": "MustangCoin",
        "symbol": "MST",
        "slug": "mustangcoin",
        "is_active": 1,
        "first_historical_data": "2020-07-01T11:39:47.270Z",
        "last_historical_data": "2020-08-17T19:19:12.437Z",
        "platform": null
      },
      {
        "id": 1398,
        "name": "PROUD Money",
        "symbol": "PROUD",
        "slug": "proud-money",
        "is_active": 1,
        "first_historical_data": "2019-11-18T15:06:47.825Z",
        "last_historical_data": "2020-08-18T11:22:34.397Z",
        "platform": null
      },
      {
        "id": 1403,
        "name": "FirstBlood",
        "symbol": "1ST",
        "slug": "firstblood",
        "is_active": 1,
        "first_historical_data": "2020-05-24T12:33:14.864Z",
        "last_historical_data": "2020-08-18T12:29:32.951Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xaf30d2a7e90d7dc361c8c4585e9bb7d2f6f15bc7"
        }
      },
      {
        "id": 1409,
        "name": "SingularDTV",
        "symbol": "SNGLS",
        "slug": "singulardtv",
        "is_active": 1,
        "first_historical_data": "2020-07-17T10:02:26.517Z",
        "last_historical_data": "2020-08-18T00:56:15.668Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xaec2e87e0a235266d9c5adc9deb4b2e29b54d009"
        }
      },
      {
        "id": 1414,
        "name": "Zcoin",
        "symbol": "XZC",
        "slug": "zcoin",
        "is_active": 1,
        "first_historical_data": "2019-09-24T04:42:47.710Z",
        "last_historical_data": "2020-08-18T12:16:38.230Z",
        "platform": null
      },
      {
        "id": 1434,
        "name": "Advanced Technology Coin",
        "symbol": "ARC",
        "slug": "arcticcoin",
        "is_active": 1,
        "first_historical_data": "2020-01-28T08:55:42.718Z",
        "last_historical_data": "2020-08-18T14:45:19.974Z",
        "platform": null
      },
      {
        "id": 1437,
        "name": "Zcash",
        "symbol": "ZEC",
        "slug": "zcash",
        "is_active": 1,
        "first_historical_data": "2020-07-06T14:46:43.986Z",
        "last_historical_data": "2020-08-18T15:41:09.634Z",
        "platform": null
      },
      {
        "id": 1439,
        "name": "AllSafe",
        "symbol": "ASAFE",
        "slug": "allsafe",
        "is_active": 1,
        "first_historical_data": "2020-08-15T08:13:11.960Z",
        "last_historical_data": "2020-08-18T15:37:07.178Z",
        "platform": null
      },
      {
        "id": 1447,
        "name": "ZClassic",
        "symbol": "ZCL",
        "slug": "zclassic",
        "is_active": 1,
        "first_historical_data": "2020-01-15T18:52:42.803Z",
        "last_historical_data": "2020-08-18T05:49:22.594Z",
        "platform": null
      },
      {
        "id": 1454,
        "name": "Lykke",
        "symbol": "LKK",
        "slug": "lykke",
        "is_active": 1,
        "first_historical_data": "2020-05-08T11:34:29.276Z",
        "last_historical_data": "2020-08-18T03:55:05.780Z",
        "platform": null
      },
      {
        "id": 1455,
        "name": "Golem",
        "symbol": "GNT",
        "slug": "golem-network-tokens",
        "is_active": 1,
        "first_historical_data": "2020-01-23T11:51:15.876Z",
        "last_historical_data": "2020-08-18T10:27:56.824Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa74476443119A942dE498590Fe1f2454d7D4aC0d"
        }
      },
      {
        "id": 1464,
        "name": "Internet of People",
        "symbol": "IOP",
        "slug": "internet-of-people",
        "is_active": 1,
        "first_historical_data": "2020-01-12T16:38:20.439Z",
        "last_historical_data": "2020-08-18T01:43:38.971Z",
        "platform": null
      },
      {
        "id": 1465,
        "name": "Veros",
        "symbol": "VRS",
        "slug": "veros",
        "is_active": 1,
        "first_historical_data": "2019-12-16T14:25:09.483Z",
        "last_historical_data": "2020-08-18T16:17:45.084Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xAbC430136A4dE71c9998242de8c1b4B97D2b9045"
        }
      },
      {
        "id": 1466,
        "name": "Hush",
        "symbol": "HUSH",
        "slug": "hush",
        "is_active": 1,
        "first_historical_data": "2020-04-10T12:01:52.969Z",
        "last_historical_data": "2020-08-18T03:25:16.578Z",
        "platform": null
      },
      {
        "id": 1468,
        "name": "Kurrent",
        "symbol": "KURT",
        "slug": "kurrent",
        "is_active": 1,
        "first_historical_data": "2020-05-30T10:16:58.937Z",
        "last_historical_data": "2020-08-18T13:45:08.482Z",
        "platform": null
      },
      {
        "id": 1473,
        "name": "Pascal",
        "symbol": "PASC",
        "slug": "pascal",
        "is_active": 1,
        "first_historical_data": "2020-06-27T02:08:09.400Z",
        "last_historical_data": "2020-08-18T14:04:25.417Z",
        "platform": null
      },
      {
        "id": 1474,
        "name": "Eternity",
        "symbol": "ENT",
        "slug": "eternity",
        "is_active": 1,
        "first_historical_data": "2019-09-02T19:23:56.087Z",
        "last_historical_data": "2020-08-18T00:46:31.677Z",
        "platform": null
      },
      {
        "id": 1475,
        "name": "Incent",
        "symbol": "INCNT",
        "slug": "incent",
        "is_active": 1,
        "first_historical_data": "2020-02-17T23:13:27.767Z",
        "last_historical_data": "2020-08-18T00:52:36.465Z",
        "platform": {
          "id": 1274,
          "name": "Waves",
          "symbol": "WAVES",
          "slug": "waves",
          "token_address": ""
        }
      },
      {
        "id": 1478,
        "name": "DECENT",
        "symbol": "DCT",
        "slug": "decent",
        "is_active": 1,
        "first_historical_data": "2019-09-13T18:58:50.236Z",
        "last_historical_data": "2020-08-18T13:25:27.689Z",
        "platform": null
      },
      {
        "id": 1483,
        "name": "vSlice",
        "symbol": "VSL",
        "slug": "vslice",
        "is_active": 1,
        "first_historical_data": "2019-10-24T21:57:42.701Z",
        "last_historical_data": "2020-08-18T11:36:35.092Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5c543e7ae0a1104f78406c340e9c64fd9fce5170"
        }
      },
      {
        "id": 1485,
        "name": "Dollar International",
        "symbol": "DOLLAR",
        "slug": "dollar-international",
        "is_active": 1,
        "first_historical_data": "2020-03-08T18:36:07.278Z",
        "last_historical_data": "2020-08-17T19:09:03.556Z",
        "platform": {
          "id": 512,
          "name": "Stellar",
          "symbol": "XLM",
          "slug": "stellar",
          "token_address": "GCBAQPS2KAF2CHAAAWZSO6XWKMEMU6Q2MCJ47AOKJOMFTY5BSZ3D5GRK"
        }
      },
      {
        "id": 1492,
        "name": "Obyte",
        "symbol": "GBYTE",
        "slug": "obyte",
        "is_active": 1,
        "first_historical_data": "2020-07-16T16:10:21.343Z",
        "last_historical_data": "2020-08-18T17:55:36.378Z",
        "platform": null
      },
      {
        "id": 1495,
        "name": "PoSW Coin",
        "symbol": "POSW",
        "slug": "posw-coin",
        "is_active": 1,
        "first_historical_data": "2020-05-17T12:53:00.999Z",
        "last_historical_data": "2020-08-18T12:33:33.682Z",
        "platform": null
      },
      {
        "id": 1496,
        "name": "Luna Coin",
        "symbol": "LUNA",
        "slug": "luna-coin",
        "is_active": 1,
        "first_historical_data": "2020-05-31T17:49:30.978Z",
        "last_historical_data": "2020-08-17T19:56:35.060Z",
        "platform": null
      },
      {
        "id": 1500,
        "name": "Wings",
        "symbol": "WINGS",
        "slug": "wings",
        "is_active": 1,
        "first_historical_data": "2019-11-22T21:49:09.635Z",
        "last_historical_data": "2020-08-18T14:22:51.664Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x667088b212ce3d06a1b553a7221E1fD19000d9aF"
        }
      },
      {
        "id": 1503,
        "name": "Jupiter",
        "symbol": "JUP",
        "slug": "jupiter",
        "is_active": 1,
        "first_historical_data": "2020-05-24T23:33:29.118Z",
        "last_historical_data": "2020-08-17T18:55:41.577Z",
        "platform": null
      },
      {
        "id": 1504,
        "name": "InflationCoin",
        "symbol": "IFLT",
        "slug": "inflationcoin",
        "is_active": 1,
        "first_historical_data": "2020-01-21T03:32:19.520Z",
        "last_historical_data": "2020-08-17T20:37:57.697Z",
        "platform": null
      },
      {
        "id": 1505,
        "name": "Spectrecoin",
        "symbol": "XSPEC",
        "slug": "spectrecoin",
        "is_active": 1,
        "first_historical_data": "2020-03-17T03:26:13.161Z",
        "last_historical_data": "2020-08-18T05:22:08.105Z",
        "platform": null
      },
      {
        "id": 1509,
        "name": "BenjiRolls",
        "symbol": "BENJI",
        "slug": "benjirolls",
        "is_active": 1,
        "first_historical_data": "2019-09-30T00:33:53.938Z",
        "last_historical_data": "2020-08-18T09:38:01.390Z",
        "platform": null
      },
      {
        "id": 1510,
        "name": "CryptoCarbon",
        "symbol": "CCRB",
        "slug": "cryptocarbon",
        "is_active": 1,
        "first_historical_data": "2019-11-09T06:21:24.970Z",
        "last_historical_data": "2020-08-18T11:28:46.654Z",
        "platform": null
      },
      {
        "id": 1511,
        "name": "PureVidz",
        "symbol": "VIDZ",
        "slug": "purevidz",
        "is_active": 1,
        "first_historical_data": "2020-07-30T03:46:21.787Z",
        "last_historical_data": "2020-08-18T14:49:06.289Z",
        "platform": null
      },
      {
        "id": 1514,
        "name": "ICOBID",
        "symbol": "ICOB",
        "slug": "icobid",
        "is_active": 1,
        "first_historical_data": "2020-05-04T12:07:24.922Z",
        "last_historical_data": "2020-08-18T13:36:19.773Z",
        "platform": null
      },
      {
        "id": 1515,
        "name": "iBank",
        "symbol": "IBANK",
        "slug": "ibank",
        "is_active": 1,
        "first_historical_data": "2020-01-01T20:25:19.950Z",
        "last_historical_data": "2020-08-18T03:28:13.726Z",
        "platform": null
      },
      {
        "id": 1518,
        "name": "Maker",
        "symbol": "MKR",
        "slug": "maker",
        "is_active": 1,
        "first_historical_data": "2020-08-06T17:01:06.802Z",
        "last_historical_data": "2020-08-18T17:23:13.157Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2"
        }
      },
      {
        "id": 1521,
        "name": "Komodo",
        "symbol": "KMD",
        "slug": "komodo",
        "is_active": 1,
        "first_historical_data": "2019-10-20T04:16:34.883Z",
        "last_historical_data": "2020-08-18T06:02:38.553Z",
        "platform": null
      },
      {
        "id": 1522,
        "name": "FirstCoin",
        "symbol": "FRST",
        "slug": "firstcoin",
        "is_active": 1,
        "first_historical_data": "2019-12-27T17:53:31.391Z",
        "last_historical_data": "2020-08-18T17:21:40.084Z",
        "platform": null
      },
      {
        "id": 1527,
        "name": "Waves Community Token",
        "symbol": "WCT",
        "slug": "waves-community-token",
        "is_active": 1,
        "first_historical_data": "2020-04-28T09:43:43.482Z",
        "last_historical_data": "2020-08-18T03:43:11.594Z",
        "platform": {
          "id": 1274,
          "name": "Waves",
          "symbol": "WAVES",
          "slug": "waves",
          "token_address": ""
        }
      },
      {
        "id": 1528,
        "name": "Iconic",
        "symbol": "ICON",
        "slug": "iconic",
        "is_active": 1,
        "first_historical_data": "2020-01-31T04:17:33.703Z",
        "last_historical_data": "2020-08-18T14:35:00.971Z",
        "platform": null
      },
      {
        "id": 1531,
        "name": "Global Cryptocurrency",
        "symbol": "GCC",
        "slug": "global-cryptocurrency",
        "is_active": 1,
        "first_historical_data": "2020-07-25T03:39:37.296Z",
        "last_historical_data": "2020-08-17T18:17:11.221Z",
        "platform": null
      },
      {
        "id": 1546,
        "name": "Centurion",
        "symbol": "CNT",
        "slug": "centurion",
        "is_active": 1,
        "first_historical_data": "2020-04-03T08:57:04.390Z",
        "last_historical_data": "2020-08-17T18:43:40.769Z",
        "platform": null
      },
      {
        "id": 1552,
        "name": "Melon",
        "symbol": "MLN",
        "slug": "melon",
        "is_active": 1,
        "first_historical_data": "2020-07-29T10:06:38.166Z",
        "last_historical_data": "2020-08-18T06:58:55.882Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xec67005c4e498ec7f55e092bd1d35cbc47c91892"
        }
      },
      {
        "id": 1556,
        "name": "Chrono.tech",
        "symbol": "TIME",
        "slug": "chrono-tech",
        "is_active": 1,
        "first_historical_data": "2020-08-03T00:25:39.003Z",
        "last_historical_data": "2020-08-18T14:08:19.110Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6531f133e6deebe7f2dce5a0441aa7ef330b4e53"
        }
      },
      {
        "id": 1558,
        "name": "Argus",
        "symbol": "ARGUS",
        "slug": "argus",
        "is_active": 1,
        "first_historical_data": "2020-03-20T10:48:22.605Z",
        "last_historical_data": "2020-08-18T13:02:46.685Z",
        "platform": null
      },
      {
        "id": 1562,
        "name": "Swarm City",
        "symbol": "SWT",
        "slug": "swarm-city",
        "is_active": 1,
        "first_historical_data": "2020-06-16T17:05:33.456Z",
        "last_historical_data": "2020-08-18T09:48:14.423Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb9e7f8568e08d5659f5d29c4997173d84cdf2607"
        }
      },
      {
        "id": 1567,
        "name": "Nano",
        "symbol": "NANO",
        "slug": "nano",
        "is_active": 1,
        "first_historical_data": "2020-04-07T23:31:42.629Z",
        "last_historical_data": "2020-08-18T17:42:49.092Z",
        "platform": null
      },
      {
        "id": 1576,
        "name": "MiloCoin",
        "symbol": "MILO",
        "slug": "milocoin",
        "is_active": 1,
        "first_historical_data": "2020-01-13T03:03:10.904Z",
        "last_historical_data": "2020-08-18T01:38:50.541Z",
        "platform": null
      },
      {
        "id": 1578,
        "name": "Zero",
        "symbol": "ZER",
        "slug": "zero",
        "is_active": 1,
        "first_historical_data": "2020-02-16T11:29:23.518Z",
        "last_historical_data": "2020-08-18T14:38:34.480Z",
        "platform": null
      },
      {
        "id": 1582,
        "name": "Netko",
        "symbol": "NETKO",
        "slug": "netko",
        "is_active": 1,
        "first_historical_data": "2019-08-31T03:49:18.862Z",
        "last_historical_data": "2020-08-18T15:38:46.705Z",
        "platform": null
      },
      {
        "id": 1586,
        "name": "Ark",
        "symbol": "ARK",
        "slug": "ark",
        "is_active": 1,
        "first_historical_data": "2020-01-17T02:39:40.436Z",
        "last_historical_data": "2020-08-18T03:23:25.688Z",
        "platform": null
      },
      {
        "id": 1587,
        "name": "Dynamic",
        "symbol": "DYN",
        "slug": "dynamic",
        "is_active": 1,
        "first_historical_data": "2019-12-20T20:27:41.800Z",
        "last_historical_data": "2020-08-18T16:27:39.030Z",
        "platform": null
      },
      {
        "id": 1590,
        "name": "Mercury",
        "symbol": "MER",
        "slug": "mercury",
        "is_active": 1,
        "first_historical_data": "2020-02-26T18:13:30.476Z",
        "last_historical_data": "2020-08-18T11:54:33.416Z",
        "platform": {
          "id": 1274,
          "name": "Waves",
          "symbol": "WAVES",
          "slug": "waves",
          "token_address": "HzfaJp8YQWLvQG4FkUxq2Q7iYWMYQ2k8UF89vVJAjWPj"
        }
      },
      {
        "id": 1592,
        "name": "TaaS",
        "symbol": "TAAS",
        "slug": "taas",
        "is_active": 1,
        "first_historical_data": "2020-04-22T05:51:57.635Z",
        "last_historical_data": "2020-08-18T05:52:24.451Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe7775a6e9bcf904eb39da2b68c5efb4f9360e08c"
        }
      },
      {
        "id": 1596,
        "name": "Edgeless",
        "symbol": "EDG",
        "slug": "edgeless",
        "is_active": 1,
        "first_historical_data": "2019-12-04T15:12:28.867Z",
        "last_historical_data": "2020-08-17T18:30:52.714Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x08711d3b02c8758f2fb3ab4e80228418a7f8e39c"
        }
      },
      {
        "id": 1599,
        "name": "Mavro",
        "symbol": "MAVRO",
        "slug": "mavro",
        "is_active": 1,
        "first_historical_data": "2020-04-04T19:17:05.162Z",
        "last_historical_data": "2020-08-18T14:57:20.805Z",
        "platform": null
      },
      {
        "id": 1605,
        "name": "Universe",
        "symbol": "UNI",
        "slug": "universe",
        "is_active": 1,
        "first_historical_data": "2020-06-28T08:21:01.466Z",
        "last_historical_data": "2020-08-18T06:44:36.233Z",
        "platform": null
      },
      {
        "id": 1606,
        "name": "Solaris",
        "symbol": "XLR",
        "slug": "solaris",
        "is_active": 1,
        "first_historical_data": "2020-02-24T19:51:45.668Z",
        "last_historical_data": "2020-08-17T21:20:58.134Z",
        "platform": null
      },
      {
        "id": 1609,
        "name": "Asch",
        "symbol": "XAS",
        "slug": "asch",
        "is_active": 1,
        "first_historical_data": "2020-07-23T10:21:40.156Z",
        "last_historical_data": "2020-08-17T18:30:05.203Z",
        "platform": null
      },
      {
        "id": 1611,
        "name": "DubaiCoin",
        "symbol": "DBIX",
        "slug": "dubaicoin-dbix",
        "is_active": 1,
        "first_historical_data": "2019-12-29T04:10:44.217Z",
        "last_historical_data": "2020-08-17T18:22:00.749Z",
        "platform": null
      },
      {
        "id": 1616,
        "name": "Matchpool",
        "symbol": "GUP",
        "slug": "guppy",
        "is_active": 1,
        "first_historical_data": "2020-05-06T17:34:33.613Z",
        "last_historical_data": "2020-08-17T19:53:37.869Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf7b098298f7c69fc14610bf71d5e02c60792894c"
        }
      },
      {
        "id": 1617,
        "name": "Ultimate Secure Cash",
        "symbol": "USC",
        "slug": "ultimate-secure-cash",
        "is_active": 1,
        "first_historical_data": "2019-11-22T02:30:39.976Z",
        "last_historical_data": "2020-08-18T03:44:24.599Z",
        "platform": null
      },
      {
        "id": 1619,
        "name": "Skycoin",
        "symbol": "SKY",
        "slug": "skycoin",
        "is_active": 1,
        "first_historical_data": "2020-03-17T14:22:42.842Z",
        "last_historical_data": "2020-08-18T12:56:40.908Z",
        "platform": null
      },
      {
        "id": 1623,
        "name": "BlazerCoin",
        "symbol": "BLAZR",
        "slug": "blazercoin",
        "is_active": 1,
        "first_historical_data": "2020-03-28T08:16:13.091Z",
        "last_historical_data": "2020-08-18T16:13:19.628Z",
        "platform": null
      },
      {
        "id": 1629,
        "name": "Zennies",
        "symbol": "ZENI",
        "slug": "zennies",
        "is_active": 1,
        "first_historical_data": "2020-01-20T06:34:13.344Z",
        "last_historical_data": "2020-08-18T07:28:26.015Z",
        "platform": null
      },
      {
        "id": 1630,
        "name": "Coinonat",
        "symbol": "CXT",
        "slug": "coinonat",
        "is_active": 1,
        "first_historical_data": "2019-11-02T07:40:03.640Z",
        "last_historical_data": "2020-08-18T00:02:49.114Z",
        "platform": null
      },
      {
        "id": 1632,
        "name": "Concoin",
        "symbol": "CONX",
        "slug": "concoin",
        "is_active": 1,
        "first_historical_data": "2020-01-29T22:05:58.765Z",
        "last_historical_data": "2020-08-18T09:07:55.582Z",
        "platform": null
      },
      {
        "id": 1636,
        "name": "XTRABYTES",
        "symbol": "XBY",
        "slug": "xtrabytes",
        "is_active": 1,
        "first_historical_data": "2020-07-29T12:21:28.015Z",
        "last_historical_data": "2020-08-18T02:39:23.692Z",
        "platform": null
      },
      {
        "id": 1637,
        "name": "iExec RLC",
        "symbol": "RLC",
        "slug": "rlc",
        "is_active": 1,
        "first_historical_data": "2020-03-26T13:48:26.565Z",
        "last_historical_data": "2020-08-18T09:05:44.178Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x607f4c5bb672230e8672085532f7e901544a7375"
        }
      },
      {
        "id": 1638,
        "name": "WeTrust",
        "symbol": "TRST",
        "slug": "trust",
        "is_active": 1,
        "first_historical_data": "2020-01-26T15:19:14.578Z",
        "last_historical_data": "2020-08-18T02:39:10.281Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xcb94be6f13a1182e4a4b6140cb7bf2025d28e41b"
        }
      },
      {
        "id": 1651,
        "name": "SpeedCash",
        "symbol": "SCS",
        "slug": "speedcash",
        "is_active": 1,
        "first_historical_data": "2020-02-04T22:06:21.708Z",
        "last_historical_data": "2020-08-18T00:49:38.561Z",
        "platform": null
      },
      {
        "id": 1654,
        "name": "Bitcore",
        "symbol": "BTX",
        "slug": "bitcore",
        "is_active": 1,
        "first_historical_data": "2020-02-03T12:03:34.356Z",
        "last_historical_data": "2020-08-18T10:21:47.640Z",
        "platform": null
      },
      {
        "id": 1657,
        "name": "Bitvolt",
        "symbol": "VOLT",
        "slug": "bitvolt",
        "is_active": 1,
        "first_historical_data": "2020-02-16T16:40:40.153Z",
        "last_historical_data": "2020-08-17T23:57:40.188Z",
        "platform": null
      },
      {
        "id": 1658,
        "name": "Lunyr",
        "symbol": "LUN",
        "slug": "lunyr",
        "is_active": 1,
        "first_historical_data": "2020-08-10T21:00:19.667Z",
        "last_historical_data": "2020-08-18T06:05:40.981Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xfa05A73FfE78ef8f1a739473e462c54bae6567D9"
        }
      },
      {
        "id": 1659,
        "name": "Gnosis",
        "symbol": "GNO",
        "slug": "gnosis-gno",
        "is_active": 1,
        "first_historical_data": "2020-04-15T06:07:57.309Z",
        "last_historical_data": "2020-08-18T08:44:07.663Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6810e776880c02933d47db1b9fc05908e5386b96"
        }
      },
      {
        "id": 1660,
        "name": "Monolith",
        "symbol": "TKN",
        "slug": "monolith",
        "is_active": 1,
        "first_historical_data": "2020-07-03T07:11:29.715Z",
        "last_historical_data": "2020-08-17T21:50:22.941Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xaaaf91d9b90df800df4f55c205fd6989c977e73a"
        }
      },
      {
        "id": 1669,
        "name": "Humaniq",
        "symbol": "HMQ",
        "slug": "humaniq",
        "is_active": 1,
        "first_historical_data": "2020-05-18T04:11:02.460Z",
        "last_historical_data": "2020-08-18T15:19:54.405Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xcbcc0f036ed4788f63fc0fee32873d6a7487b908"
        }
      },
      {
        "id": 1671,
        "name": "iTicoin",
        "symbol": "ITI",
        "slug": "iticoin",
        "is_active": 1,
        "first_historical_data": "2020-07-29T21:19:38.407Z",
        "last_historical_data": "2020-08-18T01:26:15.165Z",
        "platform": null
      },
      {
        "id": 1673,
        "name": "Minereum",
        "symbol": "MNE",
        "slug": "minereum",
        "is_active": 1,
        "first_historical_data": "2019-10-04T12:15:02.135Z",
        "last_historical_data": "2020-08-17T18:48:36.137Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1a95b271b0535d15fa49932daba31ba612b52946"
        }
      },
      {
        "id": 1674,
        "name": "Cannation",
        "symbol": "CNNC",
        "slug": "cannation",
        "is_active": 1,
        "first_historical_data": "2020-02-02T11:32:43.007Z",
        "last_historical_data": "2020-08-18T17:39:21.794Z",
        "platform": null
      },
      {
        "id": 1677,
        "name": "Etheroll",
        "symbol": "DICE",
        "slug": "etheroll",
        "is_active": 1,
        "first_historical_data": "2020-07-21T08:26:19.513Z",
        "last_historical_data": "2020-08-18T00:30:19.343Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2e071D2966Aa7D8dECB1005885bA1977D6038A65"
        }
      },
      {
        "id": 1678,
        "name": "InsaneCoin",
        "symbol": "INSN",
        "slug": "insanecoin-insn",
        "is_active": 1,
        "first_historical_data": "2020-06-04T09:42:18.337Z",
        "last_historical_data": "2020-08-17T19:01:47.710Z",
        "platform": null
      },
      {
        "id": 1680,
        "name": "Aragon",
        "symbol": "ANT",
        "slug": "aragon",
        "is_active": 1,
        "first_historical_data": "2019-09-10T20:46:48.339Z",
        "last_historical_data": "2020-08-18T00:09:34.843Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x960b236a07cf122663c4303350609a66a7b288c0"
        }
      },
      {
        "id": 1681,
        "name": "PRIZM",
        "symbol": "PZM",
        "slug": "prizm",
        "is_active": 1,
        "first_historical_data": "2019-10-21T13:40:59.760Z",
        "last_historical_data": "2020-08-17T19:05:20.020Z",
        "platform": null
      },
      {
        "id": 1684,
        "name": "Qtum",
        "symbol": "QTUM",
        "slug": "qtum",
        "is_active": 1,
        "first_historical_data": "2020-06-19T17:05:07.689Z",
        "last_historical_data": "2020-08-18T06:12:37.966Z",
        "platform": null
      },
      {
        "id": 1687,
        "name": "Digital Money Bits",
        "symbol": "DMB",
        "slug": "digital-money-bits",
        "is_active": 1,
        "first_historical_data": "2019-11-24T07:04:04.047Z",
        "last_historical_data": "2020-08-18T03:47:33.592Z",
        "platform": null
      },
      {
        "id": 1691,
        "name": "Project-X",
        "symbol": "NANOX",
        "slug": "project-x",
        "is_active": 1,
        "first_historical_data": "2019-12-09T20:52:28.032Z",
        "last_historical_data": "2020-08-18T16:57:11.195Z",
        "platform": null
      },
      {
        "id": 1693,
        "name": "Theresa May Coin",
        "symbol": "MAY",
        "slug": "theresa-may-coin",
        "is_active": 1,
        "first_historical_data": "2019-12-26T01:53:49.950Z",
        "last_historical_data": "2020-08-18T12:03:43.164Z",
        "platform": null
      },
      {
        "id": 1694,
        "name": "Sumokoin",
        "symbol": "SUMO",
        "slug": "sumokoin",
        "is_active": 1,
        "first_historical_data": "2019-11-15T09:15:49.199Z",
        "last_historical_data": "2020-08-18T00:24:32.034Z",
        "platform": null
      },
      {
        "id": 1697,
        "name": "Basic Attention Token",
        "symbol": "BAT",
        "slug": "basic-attention-token",
        "is_active": 1,
        "first_historical_data": "2020-03-13T15:12:37.274Z",
        "last_historical_data": "2020-08-18T06:00:01.558Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0d8775f648430679a709e98d2b0cb6250d2887ef"
        }
      },
      {
        "id": 1698,
        "name": "Horizen",
        "symbol": "ZEN",
        "slug": "horizen",
        "is_active": 1,
        "first_historical_data": "2019-09-26T01:03:25.750Z",
        "last_historical_data": "2020-08-18T05:22:04.722Z",
        "platform": null
      },
      {
        "id": 1700,
        "name": "Aeternity",
        "symbol": "AE",
        "slug": "aeternity",
        "is_active": 1,
        "first_historical_data": "2019-10-19T22:52:17.415Z",
        "last_historical_data": "2020-08-18T05:13:35.960Z",
        "platform": null
      },
      {
        "id": 1702,
        "name": "Version",
        "symbol": "V",
        "slug": "version",
        "is_active": 1,
        "first_historical_data": "2019-09-08T00:10:00.626Z",
        "last_historical_data": "2020-08-18T01:22:18.688Z",
        "platform": null
      },
      {
        "id": 1703,
        "name": "Metaverse ETP",
        "symbol": "ETP",
        "slug": "metaverse",
        "is_active": 1,
        "first_historical_data": "2019-10-21T15:47:30.406Z",
        "last_historical_data": "2020-08-18T11:02:18.638Z",
        "platform": null
      },
      {
        "id": 1704,
        "name": "eBoost",
        "symbol": "EBST",
        "slug": "eboostcoin",
        "is_active": 1,
        "first_historical_data": "2019-08-21T03:49:28.259Z",
        "last_historical_data": "2020-08-18T15:50:24.562Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "EBST-783"
        }
      },
      {
        "id": 1706,
        "name": "Aidos Kuneen",
        "symbol": "ADK",
        "slug": "aidos-kuneen",
        "is_active": 1,
        "first_historical_data": "2019-11-13T14:06:09.995Z",
        "last_historical_data": "2020-08-18T07:55:37.751Z",
        "platform": null
      },
      {
        "id": 1708,
        "name": "Patientory",
        "symbol": "PTOY",
        "slug": "patientory",
        "is_active": 1,
        "first_historical_data": "2020-04-01T01:25:33.566Z",
        "last_historical_data": "2020-08-18T02:54:26.179Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8ae4bf2c33a8e667de34b54938b0ccd03eb8cc06"
        }
      },
      {
        "id": 1710,
        "name": "Veritaseum",
        "symbol": "VERI",
        "slug": "veritaseum",
        "is_active": 1,
        "first_historical_data": "2020-03-10T05:04:02.306Z",
        "last_historical_data": "2020-08-17T19:29:08.283Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8f3470A7388c05eE4e7AF3d01D8C722b0FF52374"
        }
      },
      {
        "id": 1711,
        "name": "Electra",
        "symbol": "ECA",
        "slug": "electra",
        "is_active": 1,
        "first_historical_data": "2019-11-08T00:52:42.898Z",
        "last_historical_data": "2020-08-18T12:09:51.134Z",
        "platform": null
      },
      {
        "id": 1712,
        "name": "Quantum Resistant Ledger",
        "symbol": "QRL",
        "slug": "quantum-resistant-ledger",
        "is_active": 1,
        "first_historical_data": "2020-04-30T20:29:27.519Z",
        "last_historical_data": "2020-08-18T04:44:28.296Z",
        "platform": null
      },
      {
        "id": 1714,
        "name": "EncryptoTel [WAVES]",
        "symbol": "ETT",
        "slug": "encryptotel",
        "is_active": 1,
        "first_historical_data": "2020-06-15T04:47:55.727Z",
        "last_historical_data": "2020-08-18T14:21:44.134Z",
        "platform": {
          "id": 1274,
          "name": "Waves",
          "symbol": "WAVES",
          "slug": "waves",
          "token_address": "8ofu3VpEaVCFjRqLLqzTMNs5URKUUQMrPp3k6oFmiCc6"
        }
      },
      {
        "id": 1715,
        "name": "MobileGo",
        "symbol": "MGO",
        "slug": "mobilego",
        "is_active": 1,
        "first_historical_data": "2019-10-31T18:03:39.337Z",
        "last_historical_data": "2020-08-18T07:56:41.719Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x40395044Ac3c0C57051906dA938B54BD6557F212"
        }
      },
      {
        "id": 1719,
        "name": "Peerplays",
        "symbol": "PPY",
        "slug": "peerplays-ppy",
        "is_active": 1,
        "first_historical_data": "2019-11-19T12:42:00.031Z",
        "last_historical_data": "2020-08-17T20:32:38.520Z",
        "platform": null
      },
      {
        "id": 1720,
        "name": "IOTA",
        "symbol": "MIOTA",
        "slug": "iota",
        "is_active": 1,
        "first_historical_data": "2020-02-10T21:10:41.699Z",
        "last_historical_data": "2020-08-18T02:21:48.434Z",
        "platform": null
      },
      {
        "id": 1721,
        "name": "Mysterium",
        "symbol": "MYST",
        "slug": "mysterium",
        "is_active": 1,
        "first_historical_data": "2019-09-16T07:00:20.418Z",
        "last_historical_data": "2020-08-18T12:52:56.789Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa645264c5603e96c3b0b078cdab68733794b0a71"
        }
      },
      {
        "id": 1722,
        "name": "More Coin",
        "symbol": "MORE",
        "slug": "more-coin",
        "is_active": 1,
        "first_historical_data": "2020-04-18T09:51:07.977Z",
        "last_historical_data": "2020-08-18T13:33:52.093Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x59061b6f26BB4A9cE5828A19d35CFD5A4B80F056"
        }
      },
      {
        "id": 1723,
        "name": "SONM",
        "symbol": "SNM",
        "slug": "sonm",
        "is_active": 1,
        "first_historical_data": "2020-05-30T19:26:37.013Z",
        "last_historical_data": "2020-08-18T07:50:01.729Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x983f6d60db79ea8ca4eb9968c6aff8cfa04b3c63"
        }
      },
      {
        "id": 1725,
        "name": "Adelphoi",
        "symbol": "ADL",
        "slug": "adelphoi",
        "is_active": 1,
        "first_historical_data": "2020-07-23T05:41:57.107Z",
        "last_historical_data": "2020-08-18T11:35:44.850Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x660e71483785f66133548B10f6926dC332b06e61"
        }
      },
      {
        "id": 1726,
        "name": "ZrCoin",
        "symbol": "ZRC",
        "slug": "zrcoin",
        "is_active": 1,
        "first_historical_data": "2020-03-28T06:18:07.300Z",
        "last_historical_data": "2020-08-18T14:26:34.896Z",
        "platform": {
          "id": 1274,
          "name": "Waves",
          "symbol": "WAVES",
          "slug": "waves",
          "token_address": ""
        }
      },
      {
        "id": 1727,
        "name": "Bancor",
        "symbol": "BNT",
        "slug": "bancor",
        "is_active": 1,
        "first_historical_data": "2020-02-16T08:19:06.659Z",
        "last_historical_data": "2020-08-18T17:31:06.180Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c"
        }
      },
      {
        "id": 1731,
        "name": "GlobalToken",
        "symbol": "GLT",
        "slug": "globaltoken",
        "is_active": 1,
        "first_historical_data": "2019-12-08T20:24:08.806Z",
        "last_historical_data": "2020-08-18T10:42:14.909Z",
        "platform": null
      },
      {
        "id": 1732,
        "name": "Numeraire",
        "symbol": "NMR",
        "slug": "numeraire",
        "is_active": 1,
        "first_historical_data": "2019-12-29T13:13:55.053Z",
        "last_historical_data": "2020-08-18T02:35:29.239Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1776e1f26f98b1a5df9cd347953a26dd3cb46671"
        }
      },
      {
        "id": 1736,
        "name": "Unify",
        "symbol": "UNIFY",
        "slug": "unify",
        "is_active": 1,
        "first_historical_data": "2019-09-23T07:00:33.785Z",
        "last_historical_data": "2020-08-18T08:03:33.325Z",
        "platform": null
      },
      {
        "id": 1737,
        "name": "XEL",
        "symbol": "XEL",
        "slug": "xel",
        "is_active": 1,
        "first_historical_data": "2020-01-04T14:40:39.910Z",
        "last_historical_data": "2020-08-18T06:16:56.026Z",
        "platform": null
      },
      {
        "id": 1745,
        "name": "Dinastycoin",
        "symbol": "DCY",
        "slug": "dinastycoin",
        "is_active": 1,
        "first_historical_data": "2019-11-02T07:09:48.396Z",
        "last_historical_data": "2020-08-18T13:44:39.259Z",
        "platform": null
      },
      {
        "id": 1747,
        "name": "Onix",
        "symbol": "ONX",
        "slug": "onix",
        "is_active": 1,
        "first_historical_data": "2020-03-26T21:56:07.345Z",
        "last_historical_data": "2020-08-17T23:47:48.484Z",
        "platform": null
      },
      {
        "id": 1750,
        "name": "GXChain",
        "symbol": "GXC",
        "slug": "gxchain",
        "is_active": 1,
        "first_historical_data": "2020-08-02T08:34:15.638Z",
        "last_historical_data": "2020-08-18T00:33:25.165Z",
        "platform": null
      },
      {
        "id": 1751,
        "name": "ATC Coin",
        "symbol": "ATCC",
        "slug": "atc-coin",
        "is_active": 1,
        "first_historical_data": "2020-01-19T15:43:48.307Z",
        "last_historical_data": "2020-08-18T17:01:04.483Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xddaaf4a0702a03a4505f2352a1aba001ffc344be"
        }
      },
      {
        "id": 1754,
        "name": "Bitradio",
        "symbol": "BRO",
        "slug": "bitradio",
        "is_active": 1,
        "first_historical_data": "2019-12-19T18:22:24.665Z",
        "last_historical_data": "2020-08-18T06:07:31.819Z",
        "platform": null
      },
      {
        "id": 1755,
        "name": "Flash",
        "symbol": "FLASH",
        "slug": "flash",
        "is_active": 1,
        "first_historical_data": "2019-11-03T03:47:50.711Z",
        "last_historical_data": "2020-08-18T16:39:21.153Z",
        "platform": null
      },
      {
        "id": 1757,
        "name": "FunFair",
        "symbol": "FUN",
        "slug": "funfair",
        "is_active": 1,
        "first_historical_data": "2019-12-02T08:52:21.188Z",
        "last_historical_data": "2020-08-18T01:59:43.184Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x419d0d8bdd9af5e606ae2232ed285aff190e711b"
        }
      },
      {
        "id": 1758,
        "name": "TenX",
        "symbol": "PAY",
        "slug": "tenx",
        "is_active": 1,
        "first_historical_data": "2019-09-19T08:04:43.809Z",
        "last_historical_data": "2020-08-18T04:45:48.477Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb97048628db6b661d4c2aa833e95dbe1a905b280"
        }
      },
      {
        "id": 1759,
        "name": "Status",
        "symbol": "SNT",
        "slug": "status",
        "is_active": 1,
        "first_historical_data": "2019-09-14T05:50:38.194Z",
        "last_historical_data": "2020-08-18T00:56:14.847Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x744d70fdbe2ba4cf95131626614a1763df805b9e"
        }
      },
      {
        "id": 1762,
        "name": "Ergo",
        "symbol": "ERG",
        "slug": "ergo",
        "is_active": 1,
        "first_historical_data": "2019-12-03T09:30:19.065Z",
        "last_historical_data": "2020-08-17T19:37:46.449Z",
        "platform": null
      },
      {
        "id": 1763,
        "name": "BriaCoin",
        "symbol": "BRIA",
        "slug": "briacoin",
        "is_active": 1,
        "first_historical_data": "2019-11-30T03:34:27.454Z",
        "last_historical_data": "2020-08-17T23:16:01.079Z",
        "platform": null
      },
      {
        "id": 1765,
        "name": "EOS",
        "symbol": "EOS",
        "slug": "eos",
        "is_active": 1,
        "first_historical_data": "2020-06-17T07:31:57.179Z",
        "last_historical_data": "2020-08-18T08:48:24.325Z",
        "platform": null
      },
      {
        "id": 1768,
        "name": "AdEx",
        "symbol": "ADX",
        "slug": "adx-net",
        "is_active": 1,
        "first_historical_data": "2020-01-06T02:11:01.664Z",
        "last_historical_data": "2020-08-18T13:31:32.242Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4470bb87d77b963a013db939be332f927f2b992e"
        }
      },
      {
        "id": 1769,
        "name": "Denarius",
        "symbol": "D",
        "slug": "denarius-d",
        "is_active": 1,
        "first_historical_data": "2019-10-02T16:31:49.336Z",
        "last_historical_data": "2020-08-18T07:22:53.370Z",
        "platform": null
      },
      {
        "id": 1771,
        "name": "DAOBet",
        "symbol": "BET",
        "slug": "daobet",
        "is_active": 1,
        "first_historical_data": "2019-09-08T21:57:33.620Z",
        "last_historical_data": "2020-08-18T08:25:11.763Z",
        "platform": null
      },
      {
        "id": 1772,
        "name": "Storj",
        "symbol": "STORJ",
        "slug": "storj",
        "is_active": 1,
        "first_historical_data": "2020-04-02T05:48:34.434Z",
        "last_historical_data": "2020-08-18T16:31:50.043Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb64ef51c888972c908cfacf59b47c1afbc0ab8ac"
        }
      },
      {
        "id": 1774,
        "name": "SocialCoin",
        "symbol": "SOCC",
        "slug": "socialcoin-socc",
        "is_active": 1,
        "first_historical_data": "2019-10-15T03:34:02.995Z",
        "last_historical_data": "2020-08-18T05:36:29.488Z",
        "platform": null
      },
      {
        "id": 1775,
        "name": "adToken",
        "symbol": "ADT",
        "slug": "adtoken",
        "is_active": 1,
        "first_historical_data": "2019-09-01T05:46:46.454Z",
        "last_historical_data": "2020-08-17T22:39:02.121Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd0d6d6c5fe4a677d343cc433536bb717bae167dd"
        }
      },
      {
        "id": 1776,
        "name": "MCO",
        "symbol": "MCO",
        "slug": "crypto-com",
        "is_active": 1,
        "first_historical_data": "2019-11-15T21:57:38.664Z",
        "last_historical_data": "2020-08-18T14:24:43.717Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb63b606ac810a52cca15e44bb630fd42d8d1d83d"
        }
      },
      {
        "id": 1777,
        "name": "CryptoPing",
        "symbol": "PING",
        "slug": "cryptoping",
        "is_active": 1,
        "first_historical_data": "2020-03-06T11:56:51.775Z",
        "last_historical_data": "2020-08-18T01:18:45.351Z",
        "platform": {
          "id": 1274,
          "name": "Waves",
          "symbol": "WAVES",
          "slug": "waves",
          "token_address": "Bi4w2UuGRt2jAJFfRb8b3SwDUV5x8krCzX2zZHcRfPNc"
        }
      },
      {
        "id": 1779,
        "name": "Wagerr",
        "symbol": "WGR",
        "slug": "wagerr",
        "is_active": 1,
        "first_historical_data": "2020-04-08T15:08:45.674Z",
        "last_historical_data": "2020-08-18T00:52:09.059Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "WGR-D3D"
        }
      },
      {
        "id": 1782,
        "name": "Ecobit",
        "symbol": "ECOB",
        "slug": "ecobit",
        "is_active": 1,
        "first_historical_data": "2020-03-19T03:41:31.467Z",
        "last_historical_data": "2020-08-18T00:43:06.929Z",
        "platform": {
          "id": 873,
          "name": "NEM",
          "symbol": "XEM",
          "slug": "nem",
          "token_address": "ecobit"
        }
      },
      {
        "id": 1784,
        "name": "Polybius",
        "symbol": "PLBT",
        "slug": "polybius",
        "is_active": 1,
        "first_historical_data": "2020-04-26T05:05:21.440Z",
        "last_historical_data": "2020-08-18T04:45:04.095Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0affa06e7fbe5bc9a764c979aa66e8256a631f02"
        }
      },
      {
        "id": 1785,
        "name": "Gas",
        "symbol": "GAS",
        "slug": "gas",
        "is_active": 1,
        "first_historical_data": "2020-01-30T21:18:05.020Z",
        "last_historical_data": "2020-08-17T20:47:16.014Z",
        "platform": {
          "id": 1376,
          "name": "Neo",
          "symbol": "NEO",
          "slug": "neo",
          "token_address": "602c79718b16e442de58778e148d0b1084e3b2dffd5de6b7b16cee7969282de7"
        }
      },
      {
        "id": 1786,
        "name": "SunContract",
        "symbol": "SNC",
        "slug": "suncontract",
        "is_active": 1,
        "first_historical_data": "2020-02-07T07:33:39.217Z",
        "last_historical_data": "2020-08-18T05:39:56.750Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xF4134146AF2d511Dd5EA8cDB1C4AC88C57D60404"
        }
      },
      {
        "id": 1787,
        "name": "Jetcoin",
        "symbol": "JET",
        "slug": "jetcoin",
        "is_active": 1,
        "first_historical_data": "2020-06-30T01:39:47.033Z",
        "last_historical_data": "2020-08-18T02:39:32.925Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8727c112c712c4a03371ac87a74dd6ab104af768"
        }
      },
      {
        "id": 1788,
        "name": "Metal",
        "symbol": "MTL",
        "slug": "metal",
        "is_active": 1,
        "first_historical_data": "2019-11-26T12:31:35.121Z",
        "last_historical_data": "2020-08-18T16:40:41.682Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xF433089366899D83a9f26A773D59ec7eCF30355e"
        }
      },
      {
        "id": 1789,
        "name": "Populous",
        "symbol": "PPT",
        "slug": "populous",
        "is_active": 1,
        "first_historical_data": "2019-11-13T07:50:44.804Z",
        "last_historical_data": "2020-08-17T19:58:33.514Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd4fa1460f537bb9085d22c7bccb5dd450ef28e3a"
        }
      },
      {
        "id": 1799,
        "name": "Rupee",
        "symbol": "RUP",
        "slug": "rupee",
        "is_active": 1,
        "first_historical_data": "2020-06-21T01:07:07.547Z",
        "last_historical_data": "2020-08-18T16:37:02.957Z",
        "platform": null
      },
      {
        "id": 1803,
        "name": "PeepCoin",
        "symbol": "PCN",
        "slug": "peepcoin",
        "is_active": 1,
        "first_historical_data": "2019-10-25T21:55:43.197Z",
        "last_historical_data": "2020-08-17T18:45:40.882Z",
        "platform": null
      },
      {
        "id": 1807,
        "name": "Santiment Network Token",
        "symbol": "SAN",
        "slug": "santiment",
        "is_active": 1,
        "first_historical_data": "2019-08-30T23:21:02.908Z",
        "last_historical_data": "2020-08-18T01:41:19.563Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x7c5a0ce9267ed19b22f8cae653f198e3e8daf098"
        }
      },
      {
        "id": 1808,
        "name": "OMG Network",
        "symbol": "OMG",
        "slug": "omg",
        "is_active": 1,
        "first_historical_data": "2020-01-21T19:24:02.255Z",
        "last_historical_data": "2020-08-18T03:42:47.025Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd26114cd6EE289AccF82350c8d8487fedB8A0C07"
        }
      },
      {
        "id": 1809,
        "name": "TerraNova",
        "symbol": "TER",
        "slug": "terranova",
        "is_active": 1,
        "first_historical_data": "2019-11-06T04:38:55.306Z",
        "last_historical_data": "2020-08-18T14:25:55.969Z",
        "platform": null
      },
      {
        "id": 1810,
        "name": "CVCoin",
        "symbol": "CVN",
        "slug": "cvcoin",
        "is_active": 1,
        "first_historical_data": "2019-12-27T22:18:20.901Z",
        "last_historical_data": "2020-08-18T11:00:32.788Z",
        "platform": {
          "id": 463,
          "name": "BitShares",
          "symbol": "BTS",
          "slug": "bitshares",
          "token_address": "CVN"
        }
      },
      {
        "id": 1814,
        "name": "Metrix Coin",
        "symbol": "MRX",
        "slug": "metrix-coin",
        "is_active": 1,
        "first_historical_data": "2020-06-19T11:30:15.819Z",
        "last_historical_data": "2020-08-18T12:05:16.015Z",
        "platform": null
      },
      {
        "id": 1816,
        "name": "Civic",
        "symbol": "CVC",
        "slug": "civic",
        "is_active": 1,
        "first_historical_data": "2020-06-16T00:08:13.111Z",
        "last_historical_data": "2020-08-17T22:01:16.366Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x41e5560054824ea6b0732e656e3ad64e20e94e45"
        }
      },
      {
        "id": 1817,
        "name": "Voyager Token",
        "symbol": "VGX",
        "slug": "voyager-token",
        "is_active": 1,
        "first_historical_data": "2019-12-20T23:32:56.525Z",
        "last_historical_data": "2020-08-17T20:22:13.234Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5af2be193a6abca9c8817001f45744777db30756"
        }
      },
      {
        "id": 1824,
        "name": "BitCoal",
        "symbol": "COAL",
        "slug": "bitcoal",
        "is_active": 1,
        "first_historical_data": "2020-01-04T03:47:06.922Z",
        "last_historical_data": "2020-08-17T20:01:46.970Z",
        "platform": null
      },
      {
        "id": 1825,
        "name": "LiteBitcoin",
        "symbol": "LBTC",
        "slug": "litebitcoin",
        "is_active": 1,
        "first_historical_data": "2020-08-13T10:19:07.682Z",
        "last_historical_data": "2020-08-18T05:10:23.326Z",
        "platform": null
      },
      {
        "id": 1826,
        "name": "Particl",
        "symbol": "PART",
        "slug": "particl",
        "is_active": 1,
        "first_historical_data": "2019-11-12T00:53:14.971Z",
        "last_historical_data": "2020-08-18T09:00:55.090Z",
        "platform": null
      },
      {
        "id": 1828,
        "name": "SmartCash",
        "symbol": "SMART",
        "slug": "smartcash",
        "is_active": 1,
        "first_historical_data": "2019-11-22T01:31:50.453Z",
        "last_historical_data": "2020-08-18T12:49:27.536Z",
        "platform": null
      },
      {
        "id": 1830,
        "name": "SkinCoin",
        "symbol": "SKIN",
        "slug": "skincoin",
        "is_active": 1,
        "first_historical_data": "2019-12-24T19:05:46.267Z",
        "last_historical_data": "2020-08-18T02:39:41.272Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2bdc0d42996017fce214b21607a515da41a9e0c5"
        }
      },
      {
        "id": 1831,
        "name": "Bitcoin Cash",
        "symbol": "BCH",
        "slug": "bitcoin-cash",
        "is_active": 1,
        "first_historical_data": "2020-07-23T04:18:04.644Z",
        "last_historical_data": "2020-08-17T20:27:24.708Z",
        "platform": null
      },
      {
        "id": 1832,
        "name": "HarmonyCoin",
        "symbol": "HMC",
        "slug": "harmonycoin-hmc",
        "is_active": 1,
        "first_historical_data": "2020-08-10T11:53:29.734Z",
        "last_historical_data": "2020-08-18T14:19:47.830Z",
        "platform": null
      },
      {
        "id": 1833,
        "name": "ToaCoin",
        "symbol": "TOA",
        "slug": "toacoin",
        "is_active": 1,
        "first_historical_data": "2019-10-14T12:32:01.191Z",
        "last_historical_data": "2020-08-18T16:12:39.989Z",
        "platform": null
      },
      {
        "id": 1834,
        "name": "Pillar",
        "symbol": "PLR",
        "slug": "pillar",
        "is_active": 1,
        "first_historical_data": "2020-04-24T20:08:20.472Z",
        "last_historical_data": "2020-08-17T23:18:07.391Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe3818504c1b32bf1557b16c238b2e01fd3149c17"
        }
      },
      {
        "id": 1836,
        "name": "Signatum",
        "symbol": "SIGT",
        "slug": "signatum",
        "is_active": 1,
        "first_historical_data": "2020-02-23T22:57:59.604Z",
        "last_historical_data": "2020-08-18T07:36:32.974Z",
        "platform": null
      },
      {
        "id": 1838,
        "name": "OracleChain",
        "symbol": "OCT",
        "slug": "oraclechain",
        "is_active": 1,
        "first_historical_data": "2019-12-07T17:04:53.211Z",
        "last_historical_data": "2020-08-18T02:14:17.150Z",
        "platform": {
          "id": 463,
          "name": "BitShares",
          "symbol": "BTS",
          "slug": "bitshares",
          "token_address": "OCT"
        }
      },
      {
        "id": 1839,
        "name": "Binance Coin",
        "symbol": "BNB",
        "slug": "binance-coin",
        "is_active": 1,
        "first_historical_data": "2020-04-11T07:00:11.083Z",
        "last_historical_data": "2020-08-18T15:18:20.302Z",
        "platform": null
      },
      {
        "id": 1841,
        "name": "Primalbase Token",
        "symbol": "PBT",
        "slug": "primalbase",
        "is_active": 1,
        "first_historical_data": "2019-10-16T23:57:49.095Z",
        "last_historical_data": "2020-08-18T04:34:02.689Z",
        "platform": {
          "id": 1274,
          "name": "Waves",
          "symbol": "WAVES",
          "slug": "waves",
          "token_address": "EdDvbhk4wJ1kL6pMCq1V36GbQE2nGE7Metb87zbaY2JL"
        }
      },
      {
        "id": 1843,
        "name": "EmberCoin",
        "symbol": "EMB",
        "slug": "embercoin",
        "is_active": 1,
        "first_historical_data": "2020-07-20T12:51:08.443Z",
        "last_historical_data": "2020-08-18T15:28:45.801Z",
        "platform": null
      },
      {
        "id": 1845,
        "name": "IXT",
        "symbol": "IXT",
        "slug": "ixledger",
        "is_active": 1,
        "first_historical_data": "2020-07-26T11:14:10.199Z",
        "last_historical_data": "2020-08-17T19:18:02.328Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xfca47962d45adfdfd1ab2d972315db4ce7ccf094"
        }
      },
      {
        "id": 1846,
        "name": "GeyserCoin",
        "symbol": "GSR",
        "slug": "geysercoin",
        "is_active": 1,
        "first_historical_data": "2020-07-28T08:44:05.460Z",
        "last_historical_data": "2020-08-18T16:48:51.521Z",
        "platform": null
      },
      {
        "id": 1850,
        "name": "Cream",
        "symbol": "CRM",
        "slug": "cream",
        "is_active": 1,
        "first_historical_data": "2019-10-21T07:17:01.531Z",
        "last_historical_data": "2020-08-18T09:28:49.423Z",
        "platform": null
      },
      {
        "id": 1852,
        "name": "KekCoin",
        "symbol": "KEK",
        "slug": "kekcoin",
        "is_active": 1,
        "first_historical_data": "2019-09-22T18:00:14.272Z",
        "last_historical_data": "2020-08-17T22:13:20.322Z",
        "platform": null
      },
      {
        "id": 1853,
        "name": "OAX",
        "symbol": "OAX",
        "slug": "oax",
        "is_active": 1,
        "first_historical_data": "2020-01-16T17:58:29.809Z",
        "last_historical_data": "2020-08-18T16:39:37.570Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x701c244b988a513c945973defa05de933b23fe1d"
        }
      },
      {
        "id": 1856,
        "name": "district0x",
        "symbol": "DNT",
        "slug": "district0x",
        "is_active": 1,
        "first_historical_data": "2020-05-22T17:47:59.740Z",
        "last_historical_data": "2020-08-18T09:31:29.123Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0abdace70d3790235af448c88547603b945604ea"
        }
      },
      {
        "id": 1861,
        "name": "Stox",
        "symbol": "STX",
        "slug": "stox",
        "is_active": 1,
        "first_historical_data": "2020-04-22T03:03:38.310Z",
        "last_historical_data": "2020-08-18T05:54:08.686Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x006bea43baa3f7a6f765f14f10a1a1b08334ef45"
        }
      },
      {
        "id": 1864,
        "name": "Blox",
        "symbol": "CDT",
        "slug": "blox",
        "is_active": 1,
        "first_historical_data": "2019-10-19T16:11:00.773Z",
        "last_historical_data": "2020-08-18T13:58:28.009Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x177d39ac676ed1c67a2b268ad7f1e58826e5b0af"
        }
      },
      {
        "id": 1865,
        "name": "Wink",
        "symbol": "WINK",
        "slug": "wink",
        "is_active": 1,
        "first_historical_data": "2020-07-15T23:01:01.828Z",
        "last_historical_data": "2020-08-18T16:48:23.173Z",
        "platform": null
      },
      {
        "id": 1866,
        "name": "Bytom",
        "symbol": "BTM",
        "slug": "bytom",
        "is_active": 1,
        "first_historical_data": "2020-06-29T21:59:54.886Z",
        "last_historical_data": "2020-08-17T23:43:44.462Z",
        "platform": null
      },
      {
        "id": 1873,
        "name": "Blocktix",
        "symbol": "TIX",
        "slug": "blocktix",
        "is_active": 1,
        "first_historical_data": "2019-09-17T09:22:16.291Z",
        "last_historical_data": "2020-08-17T18:37:21.843Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xea1f346faf023f974eb5adaf088bbcdf02d761f4"
        }
      },
      {
        "id": 1876,
        "name": "Dentacoin",
        "symbol": "DCN",
        "slug": "dentacoin",
        "is_active": 1,
        "first_historical_data": "2020-04-21T05:00:12.410Z",
        "last_historical_data": "2020-08-18T00:21:58.670Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x08d32b0da63e2c3bcf8019c9c5d849d7a9d791e6"
        }
      },
      {
        "id": 1877,
        "name": "Rupaya",
        "symbol": "RUPX",
        "slug": "rupaya",
        "is_active": 1,
        "first_historical_data": "2020-05-15T03:55:43.832Z",
        "last_historical_data": "2020-08-18T03:53:05.921Z",
        "platform": null
      },
      {
        "id": 1878,
        "name": "Shadow Token",
        "symbol": "SHDW",
        "slug": "shadow-token",
        "is_active": 1,
        "first_historical_data": "2019-11-08T07:30:11.009Z",
        "last_historical_data": "2020-08-18T07:22:08.203Z",
        "platform": {
          "id": 1274,
          "name": "Waves",
          "symbol": "WAVES",
          "slug": "waves",
          "token_address": "ETLzrCpBqTrpyuMGdiVLBPZnUoKwte88oVdJjoFi5R2h"
        }
      },
      {
        "id": 1881,
        "name": "DeepOnion",
        "symbol": "ONION",
        "slug": "deeponion",
        "is_active": 1,
        "first_historical_data": "2019-11-24T20:52:40.106Z",
        "last_historical_data": "2020-08-17T18:21:40.474Z",
        "platform": null
      },
      {
        "id": 1882,
        "name": "BlockCAT",
        "symbol": "CAT",
        "slug": "blockcat",
        "is_active": 1,
        "first_historical_data": "2019-11-21T15:09:00.988Z",
        "last_historical_data": "2020-08-17T22:12:21.958Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x56ba2ee7890461f463f7be02aac3099f6d5811a8"
        }
      },
      {
        "id": 1883,
        "name": "Adshares",
        "symbol": "ADS",
        "slug": "adshares",
        "is_active": 1,
        "first_historical_data": "2019-09-30T08:00:41.648Z",
        "last_historical_data": "2020-08-17T19:08:23.311Z",
        "platform": null
      },
      {
        "id": 1886,
        "name": "Dent",
        "symbol": "DENT",
        "slug": "dent",
        "is_active": 1,
        "first_historical_data": "2020-02-05T10:39:05.777Z",
        "last_historical_data": "2020-08-18T05:41:52.721Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x3597bfd533a99c9aa083587b074434e61eb0a258"
        }
      },
      {
        "id": 1888,
        "name": "InvestFeed",
        "symbol": "IFT",
        "slug": "investfeed",
        "is_active": 1,
        "first_historical_data": "2019-12-29T17:53:38.268Z",
        "last_historical_data": "2020-08-18T12:42:22.318Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x7654915a1b82d6d2d0afc37c52af556ea8983c7e"
        }
      },
      {
        "id": 1894,
        "name": "The ChampCoin",
        "symbol": "TCC",
        "slug": "the-champcoin",
        "is_active": 1,
        "first_historical_data": "2019-12-02T20:26:12.277Z",
        "last_historical_data": "2020-08-18T00:31:06.850Z",
        "platform": null
      },
      {
        "id": 1896,
        "name": "0x",
        "symbol": "ZRX",
        "slug": "0x",
        "is_active": 1,
        "first_historical_data": "2020-04-16T12:26:01.377Z",
        "last_historical_data": "2020-08-17T22:25:22.708Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe41d2489571d322189246dafa5ebde1f4699f498"
        }
      },
      {
        "id": 1899,
        "name": "YOYOW",
        "symbol": "YOYOW",
        "slug": "yoyow",
        "is_active": 1,
        "first_historical_data": "2020-07-30T04:10:15.799Z",
        "last_historical_data": "2020-08-18T00:33:18.794Z",
        "platform": null
      },
      {
        "id": 1902,
        "name": "MyBit",
        "symbol": "MYB",
        "slug": "mybit",
        "is_active": 1,
        "first_historical_data": "2019-08-30T23:09:52.718Z",
        "last_historical_data": "2020-08-18T16:17:58.525Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5d60d8d7ef6d37e16ebabc324de3be57f135e0bc"
        }
      },
      {
        "id": 1903,
        "name": "HyperCash",
        "symbol": "HC",
        "slug": "hypercash",
        "is_active": 1,
        "first_historical_data": "2020-08-14T20:19:50.902Z",
        "last_historical_data": "2020-08-18T15:22:27.661Z",
        "platform": null
      },
      {
        "id": 1905,
        "name": "TrueFlip",
        "symbol": "TFL",
        "slug": "trueflip",
        "is_active": 1,
        "first_historical_data": "2020-06-24T02:56:42.459Z",
        "last_historical_data": "2020-08-17T23:39:19.027Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa7f976c360ebbed4465c2855684d1aae5271efa9"
        }
      },
      {
        "id": 1908,
        "name": "Nebulas",
        "symbol": "NAS",
        "slug": "nebulas-token",
        "is_active": 1,
        "first_historical_data": "2020-01-22T11:10:11.077Z",
        "last_historical_data": "2020-08-18T07:09:39.311Z",
        "platform": null
      },
      {
        "id": 1912,
        "name": "Dalecoin",
        "symbol": "DALC",
        "slug": "dalecoin",
        "is_active": 1,
        "first_historical_data": "2019-12-02T14:31:22.129Z",
        "last_historical_data": "2020-08-18T15:14:09.720Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x07d9e49ea402194bf48a8276dafb16e4ed633317"
        }
      },
      {
        "id": 1916,
        "name": "BiblePay",
        "symbol": "BBP",
        "slug": "biblepay",
        "is_active": 1,
        "first_historical_data": "2020-05-22T02:40:48.883Z",
        "last_historical_data": "2020-08-17T18:44:04.600Z",
        "platform": null
      },
      {
        "id": 1918,
        "name": "Achain",
        "symbol": "ACT",
        "slug": "achain",
        "is_active": 1,
        "first_historical_data": "2020-04-26T03:21:06.487Z",
        "last_historical_data": "2020-08-18T10:03:37.384Z",
        "platform": null
      },
      {
        "id": 1923,
        "name": "Tierion",
        "symbol": "TNT",
        "slug": "tierion",
        "is_active": 1,
        "first_historical_data": "2020-05-22T08:35:45.845Z",
        "last_historical_data": "2020-08-18T12:13:50.809Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x08f5a9235b08173b7569f83645d2c7fb55e8ccd8"
        }
      },
      {
        "id": 1925,
        "name": "Waltonchain",
        "symbol": "WTC",
        "slug": "waltonchain",
        "is_active": 1,
        "first_historical_data": "2019-10-02T04:03:49.678Z",
        "last_historical_data": "2020-08-17T23:08:10.526Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb7cb1c96db6b22b0d3d9536e0108d062bd488f74"
        }
      },
      {
        "id": 1926,
        "name": "BROTHER",
        "symbol": "BRAT",
        "slug": "brat",
        "is_active": 1,
        "first_historical_data": "2020-05-23T09:53:19.918Z",
        "last_historical_data": "2020-08-18T01:57:50.165Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9e77d5a1251b6f7d456722a6eac6d2d5980bd891"
        }
      },
      {
        "id": 1930,
        "name": "Primas",
        "symbol": "PST",
        "slug": "primas",
        "is_active": 1,
        "first_historical_data": "2020-07-10T11:17:54.044Z",
        "last_historical_data": "2020-08-18T03:36:22.301Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5d4abc77b8405ad177d8ac6682d584ecbfd46cec"
        }
      },
      {
        "id": 1931,
        "name": "Opus",
        "symbol": "OPT",
        "slug": "opus",
        "is_active": 1,
        "first_historical_data": "2019-08-24T08:20:56.739Z",
        "last_historical_data": "2020-08-18T15:26:38.716Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4355fc160f74328f9b383df2ec589bb3dfd82ba0"
        }
      },
      {
        "id": 1933,
        "name": "Suretly",
        "symbol": "SUR",
        "slug": "suretly",
        "is_active": 1,
        "first_historical_data": "2020-03-15T05:35:11.470Z",
        "last_historical_data": "2020-08-18T11:13:46.500Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe120c1ecbfdfea7f0a8f0ee30063491e8c26fedf"
        }
      },
      {
        "id": 1934,
        "name": "Loopring",
        "symbol": "LRC",
        "slug": "loopring",
        "is_active": 1,
        "first_historical_data": "2019-10-15T14:00:18.883Z",
        "last_historical_data": "2020-08-18T07:00:33.853Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xbbbbca6a901c926f240b89eacb641d8aec7aeafd"
        }
      },
      {
        "id": 1935,
        "name": "LiteCoin Ultra",
        "symbol": "LTCU",
        "slug": "litecoin-ultra",
        "is_active": 1,
        "first_historical_data": "2019-09-02T16:14:30.806Z",
        "last_historical_data": "2020-08-18T16:42:27.316Z",
        "platform": null
      },
      {
        "id": 1937,
        "name": "Po.et",
        "symbol": "POE",
        "slug": "poet",
        "is_active": 1,
        "first_historical_data": "2020-01-18T16:50:47.420Z",
        "last_historical_data": "2020-08-18T07:14:01.285Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0e0989b1f9b8a38983c2ba8053269ca62ec9b195"
        }
      },
      {
        "id": 1944,
        "name": "Moving Cloud Coin",
        "symbol": "MCC",
        "slug": "moving-cloud-coin",
        "is_active": 1,
        "first_historical_data": "2020-04-09T23:29:22.419Z",
        "last_historical_data": "2020-08-17T19:39:43.634Z",
        "platform": null
      },
      {
        "id": 1947,
        "name": "Monetha",
        "symbol": "MTH",
        "slug": "monetha",
        "is_active": 1,
        "first_historical_data": "2019-11-23T16:22:32.646Z",
        "last_historical_data": "2020-08-18T16:41:34.323Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xaf4dce16da2877f8c9e00544c93b62ac40631f16"
        }
      },
      {
        "id": 1948,
        "name": "Aventus",
        "symbol": "AVT",
        "slug": "aventus",
        "is_active": 1,
        "first_historical_data": "2020-04-12T08:12:30.198Z",
        "last_historical_data": "2020-08-18T16:58:45.898Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0d88ed6e74bbfd96b831231638b66c05571e824f"
        }
      },
      {
        "id": 1949,
        "name": "Agrello",
        "symbol": "DLT",
        "slug": "agrello-delta",
        "is_active": 1,
        "first_historical_data": "2020-07-30T04:02:27.285Z",
        "last_historical_data": "2020-08-18T11:05:32.609Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x07e3c70653548b04f0a75970c1f81b4cbbfb606f"
        }
      },
      {
        "id": 1950,
        "name": "Hiveterminal Token",
        "symbol": "HVN",
        "slug": "hiveterminal-token",
        "is_active": 1,
        "first_historical_data": "2020-03-18T04:40:50.540Z",
        "last_historical_data": "2020-08-18T11:45:21.378Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xC0Eb85285d83217CD7c891702bcbC0FC401E2D9D"
        }
      },
      {
        "id": 1954,
        "name": "Moeda Loyalty Points",
        "symbol": "MDA",
        "slug": "moeda-loyalty-points",
        "is_active": 1,
        "first_historical_data": "2019-11-09T16:48:00.269Z",
        "last_historical_data": "2020-08-18T02:26:32.027Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x51db5ad35c671a87207d88fc11d593ac0c8415bd"
        }
      },
      {
        "id": 1955,
        "name": "Neblio",
        "symbol": "NEBL",
        "slug": "neblio",
        "is_active": 1,
        "first_historical_data": "2020-01-22T13:28:35.681Z",
        "last_historical_data": "2020-08-18T13:31:19.699Z",
        "platform": null
      },
      {
        "id": 1958,
        "name": "TRON",
        "symbol": "TRX",
        "slug": "tron",
        "is_active": 1,
        "first_historical_data": "2019-09-19T09:37:41.283Z",
        "last_historical_data": "2020-08-18T14:22:16.521Z",
        "platform": null
      },
      {
        "id": 1961,
        "name": "imbrex",
        "symbol": "REX",
        "slug": "imbrex",
        "is_active": 1,
        "first_historical_data": "2020-07-04T02:58:27.637Z",
        "last_historical_data": "2020-08-18T02:09:17.053Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf05a9382a4c3f29e2784502754293d88b835109c"
        }
      },
      {
        "id": 1962,
        "name": "BUZZCoin",
        "symbol": "BUZZ",
        "slug": "buzzcoin",
        "is_active": 1,
        "first_historical_data": "2019-09-28T10:08:58.430Z",
        "last_historical_data": "2020-08-18T17:04:14.368Z",
        "platform": null
      },
      {
        "id": 1963,
        "name": "Credo",
        "symbol": "CREDO",
        "slug": "credo",
        "is_active": 1,
        "first_historical_data": "2020-07-04T21:08:01.747Z",
        "last_historical_data": "2020-08-18T16:33:01.240Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4e0603e2a27a30480e5e3a4fe548e29ef12f64be"
        }
      },
      {
        "id": 1966,
        "name": "Decentraland",
        "symbol": "MANA",
        "slug": "decentraland",
        "is_active": 1,
        "first_historical_data": "2019-10-19T00:22:14.675Z",
        "last_historical_data": "2020-08-18T15:02:11.488Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0f5d2fb29fb7d3cfee444a200298f468908cc942"
        }
      },
      {
        "id": 1967,
        "name": "Indorse Token",
        "symbol": "IND",
        "slug": "indorse-token",
        "is_active": 1,
        "first_historical_data": "2020-04-14T20:38:48.984Z",
        "last_historical_data": "2020-08-18T06:12:10.140Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf8e386eda857484f5a12e4b5daa9984e06e73705"
        }
      },
      {
        "id": 1968,
        "name": "XPA",
        "symbol": "XPA",
        "slug": "xpa",
        "is_active": 1,
        "first_historical_data": "2020-05-10T15:23:22.222Z",
        "last_historical_data": "2020-08-18T05:22:13.102Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x90528aeb3a2b736b780fd1b6c478bb7e1d643170"
        }
      },
      {
        "id": 1969,
        "name": "Sociall",
        "symbol": "SCL",
        "slug": "sociall",
        "is_active": 1,
        "first_historical_data": "2020-03-23T22:04:24.217Z",
        "last_historical_data": "2020-08-18T12:21:16.589Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd7631787b4dcc87b1254cfd1e5ce48e96823dee8"
        }
      },
      {
        "id": 1970,
        "name": "ATBCoin",
        "symbol": "ATB",
        "slug": "atbcoin",
        "is_active": 1,
        "first_historical_data": "2020-04-22T06:24:09.338Z",
        "last_historical_data": "2020-08-18T01:11:31.244Z",
        "platform": null
      },
      {
        "id": 1974,
        "name": "Propy",
        "symbol": "PRO",
        "slug": "propy",
        "is_active": 1,
        "first_historical_data": "2020-07-07T06:06:32.836Z",
        "last_historical_data": "2020-08-17T18:07:40.390Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x226bb599a12c826476e3a771454697ea52e9e220"
        }
      },
      {
        "id": 1975,
        "name": "Chainlink",
        "symbol": "LINK",
        "slug": "chainlink",
        "is_active": 1,
        "first_historical_data": "2020-03-20T21:24:10.180Z",
        "last_historical_data": "2020-08-18T01:53:42.042Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x514910771af9ca656af840dff83e8264ecf986ca"
        }
      },
      {
        "id": 1976,
        "name": "Blackmoon",
        "symbol": "BMC",
        "slug": "blackmoon",
        "is_active": 1,
        "first_historical_data": "2020-04-24T05:15:11.048Z",
        "last_historical_data": "2020-08-18T16:37:04.982Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xdf6ef343350780bf8c3410bf062e0c015b1dd671"
        }
      },
      {
        "id": 1981,
        "name": "Billionaire Token",
        "symbol": "XBL",
        "slug": "billionaire-token",
        "is_active": 1,
        "first_historical_data": "2020-04-29T13:22:46.433Z",
        "last_historical_data": "2020-08-18T11:49:56.925Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": ""
        }
      },
      {
        "id": 1982,
        "name": "Kyber Network",
        "symbol": "KNC",
        "slug": "kyber-network",
        "is_active": 1,
        "first_historical_data": "2020-06-24T03:23:21.203Z",
        "last_historical_data": "2020-08-17T18:53:39.267Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xdd974d5c2e2928dea5f71b9825b8b646686bd200"
        }
      },
      {
        "id": 1983,
        "name": "VIBE",
        "symbol": "VIBE",
        "slug": "vibe",
        "is_active": 1,
        "first_historical_data": "2020-07-26T16:33:28.446Z",
        "last_historical_data": "2020-08-18T06:16:03.514Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe8ff5c9c75deb346acac493c463c8950be03dfba"
        }
      },
      {
        "id": 1984,
        "name": "Substratum",
        "symbol": "SUB",
        "slug": "substratum",
        "is_active": 1,
        "first_historical_data": "2019-11-07T07:28:34.360Z",
        "last_historical_data": "2020-08-18T06:39:03.275Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8D75959f1E61EC2571aa72798237101F084DE63a"
        }
      },
      {
        "id": 1985,
        "name": "Chronologic",
        "symbol": "DAY",
        "slug": "chronologic",
        "is_active": 1,
        "first_historical_data": "2020-02-02T23:49:21.568Z",
        "last_historical_data": "2020-08-18T12:55:01.944Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xE814aeE960a85208C3dB542C53E7D4a6C8D5f60F"
        }
      },
      {
        "id": 1991,
        "name": "Rivetz",
        "symbol": "RVT",
        "slug": "rivetz",
        "is_active": 1,
        "first_historical_data": "2020-02-04T01:08:55.486Z",
        "last_historical_data": "2020-08-18T04:58:44.545Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x3d1ba9be9f66b8ee101911bc36d3fb562eac2244"
        }
      },
      {
        "id": 1993,
        "name": "Kin",
        "symbol": "KIN",
        "slug": "kin",
        "is_active": 1,
        "first_historical_data": "2020-01-01T01:03:38.325Z",
        "last_historical_data": "2020-08-18T07:04:01.789Z",
        "platform": null
      },
      {
        "id": 1996,
        "name": "SALT",
        "symbol": "SALT",
        "slug": "salt",
        "is_active": 1,
        "first_historical_data": "2019-09-27T03:42:14.235Z",
        "last_historical_data": "2020-08-17T19:12:16.958Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4156d3342d5c385a87d264f90653733592000581"
        }
      },
      {
        "id": 1998,
        "name": "Ormeus Coin",
        "symbol": "ORMEUS",
        "slug": "ormeus-coin",
        "is_active": 1,
        "first_historical_data": "2019-10-27T12:57:03.611Z",
        "last_historical_data": "2020-08-17T21:22:17.099Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc96df921009b790dffca412375251ed1a2b75c60"
        }
      },
      {
        "id": 2001,
        "name": "ColossusXT",
        "symbol": "COLX",
        "slug": "colossusxt",
        "is_active": 1,
        "first_historical_data": "2019-10-29T21:28:34.502Z",
        "last_historical_data": "2020-08-17T20:06:34.655Z",
        "platform": null
      },
      {
        "id": 2002,
        "name": "TrezarCoin",
        "symbol": "TZC",
        "slug": "trezarcoin",
        "is_active": 1,
        "first_historical_data": "2020-04-13T05:34:01.893Z",
        "last_historical_data": "2020-08-18T16:21:58.191Z",
        "platform": null
      },
      {
        "id": 2006,
        "name": "Cobinhood",
        "symbol": "COB",
        "slug": "cobinhood",
        "is_active": 1,
        "first_historical_data": "2019-12-01T12:44:10.572Z",
        "last_historical_data": "2020-08-18T08:13:44.011Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb2f7eb1f2c37645be61d73953035360e768d81e6"
        }
      },
      {
        "id": 2007,
        "name": "Regalcoin",
        "symbol": "REC",
        "slug": "regalcoin",
        "is_active": 1,
        "first_historical_data": "2020-07-21T13:12:56.122Z",
        "last_historical_data": "2020-08-18T08:38:19.444Z",
        "platform": null
      },
      {
        "id": 2008,
        "name": "MSD",
        "symbol": "MSD",
        "slug": "msd",
        "is_active": 1,
        "first_historical_data": "2020-01-19T10:43:58.664Z",
        "last_historical_data": "2020-08-17T20:33:00.913Z",
        "platform": null
      },
      {
        "id": 2009,
        "name": "Bismuth",
        "symbol": "BIS",
        "slug": "bismuth",
        "is_active": 1,
        "first_historical_data": "2020-08-17T01:10:41.161Z",
        "last_historical_data": "2020-08-18T02:30:31.037Z",
        "platform": null
      },
      {
        "id": 2010,
        "name": "Cardano",
        "symbol": "ADA",
        "slug": "cardano",
        "is_active": 1,
        "first_historical_data": "2019-10-19T09:05:33.462Z",
        "last_historical_data": "2020-08-18T08:31:37.402Z",
        "platform": null
      },
      {
        "id": 2011,
        "name": "Tezos",
        "symbol": "XTZ",
        "slug": "tezos",
        "is_active": 1,
        "first_historical_data": "2020-04-06T11:44:15.392Z",
        "last_historical_data": "2020-08-17T20:54:15.951Z",
        "platform": null
      },
      {
        "id": 2012,
        "name": "Voise",
        "symbol": "VOISE",
        "slug": "voisecom",
        "is_active": 1,
        "first_historical_data": "2020-02-19T11:41:46.910Z",
        "last_historical_data": "2020-08-18T03:25:27.800Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x83eea00d838f92dec4d1475697b9f4d3537b56e3"
        }
      },
      {
        "id": 2013,
        "name": "Infinity Economics",
        "symbol": "XIN",
        "slug": "infinity-economics",
        "is_active": 1,
        "first_historical_data": "2020-03-12T18:05:35.139Z",
        "last_historical_data": "2020-08-17T20:25:43.568Z",
        "platform": null
      },
      {
        "id": 2015,
        "name": "ATMChain",
        "symbol": "ATM",
        "slug": "attention-token-of-media",
        "is_active": 1,
        "first_historical_data": "2020-07-25T03:43:10.704Z",
        "last_historical_data": "2020-08-18T08:55:29.857Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9b11efcaaa1890f6ee52c6bb7cf8153ac5d74139"
        }
      },
      {
        "id": 2017,
        "name": "KickToken",
        "symbol": "KICK",
        "slug": "kick-token",
        "is_active": 1,
        "first_historical_data": "2019-08-29T14:50:40.069Z",
        "last_historical_data": "2020-08-18T11:52:38.338Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc12d1c73ee7dc3615ba4e37e4abfdbddfa38907e"
        }
      },
      {
        "id": 2018,
        "name": "EncryptoTel [ETH]",
        "symbol": "ETT",
        "slug": "encryptotel-eth",
        "is_active": 1,
        "first_historical_data": "2020-01-06T05:03:19.640Z",
        "last_historical_data": "2020-08-18T10:18:17.834Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe0c72452740414d861606a44ccd5ea7f96488278"
        }
      },
      {
        "id": 2019,
        "name": "Viberate",
        "symbol": "VIB",
        "slug": "viberate",
        "is_active": 1,
        "first_historical_data": "2020-01-22T01:11:33.709Z",
        "last_historical_data": "2020-08-18T09:52:24.315Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2c974b2d0ba1716e644c1fc59982a89ddd2ff724"
        }
      },
      {
        "id": 2021,
        "name": "RChain",
        "symbol": "REV",
        "slug": "rchain",
        "is_active": 1,
        "first_historical_data": "2020-06-01T06:34:04.793Z",
        "last_historical_data": "2020-08-18T15:05:51.184Z",
        "platform": null
      },
      {
        "id": 2022,
        "name": "Internxt",
        "symbol": "INXT",
        "slug": "internxt",
        "is_active": 1,
        "first_historical_data": "2020-03-31T10:13:31.359Z",
        "last_historical_data": "2020-08-18T07:06:27.762Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa8006c4ca56f24d6836727d106349320db7fef82"
        }
      },
      {
        "id": 2027,
        "name": "Cryptonex",
        "symbol": "CNX",
        "slug": "cryptonex",
        "is_active": 1,
        "first_historical_data": "2019-12-04T09:47:09.540Z",
        "last_historical_data": "2020-08-18T04:01:40.507Z",
        "platform": null
      },
      {
        "id": 2030,
        "name": "REAL",
        "symbol": "REAL",
        "slug": "real",
        "is_active": 1,
        "first_historical_data": "2020-06-16T15:18:33.145Z",
        "last_historical_data": "2020-08-18T07:42:18.810Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9214ec02cb71cba0ada6896b8da260736a67ab10"
        }
      },
      {
        "id": 2031,
        "name": "Hubii Network",
        "symbol": "HBT",
        "slug": "hubii-network",
        "is_active": 1,
        "first_historical_data": "2020-01-13T10:52:02.130Z",
        "last_historical_data": "2020-08-18T02:57:11.259Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xdd6c68bb32462e01705011a4e2ad1a60740f217f"
        }
      },
      {
        "id": 2032,
        "name": "Crystal Clear ",
        "symbol": "CCT",
        "slug": "crystal-clear",
        "is_active": 1,
        "first_historical_data": "2019-12-09T05:49:42.464Z",
        "last_historical_data": "2020-08-17T18:27:49.721Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x336f646f87d9f6bc6ed42dd46e8b3fd9dbd15c22"
        }
      },
      {
        "id": 2034,
        "name": "Everex",
        "symbol": "EVX",
        "slug": "everex",
        "is_active": 1,
        "first_historical_data": "2019-08-25T08:50:45.656Z",
        "last_historical_data": "2020-08-18T00:50:48.099Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf3db5fa2c66b7af3eb0c0b782510816cbe4813b8"
        }
      },
      {
        "id": 2036,
        "name": "PayPie",
        "symbol": "PPP",
        "slug": "paypie",
        "is_active": 1,
        "first_historical_data": "2019-10-09T15:15:13.833Z",
        "last_historical_data": "2020-08-18T09:25:47.088Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc42209accc14029c1012fb5680d95fbd6036e2a0"
        }
      },
      {
        "id": 2041,
        "name": "BitcoinZ",
        "symbol": "BTCZ",
        "slug": "bitcoinz",
        "is_active": 1,
        "first_historical_data": "2019-11-09T12:30:51.325Z",
        "last_historical_data": "2020-08-17T21:59:34.855Z",
        "platform": null
      },
      {
        "id": 2042,
        "name": "HelloGold",
        "symbol": "HGT",
        "slug": "hellogold",
        "is_active": 1,
        "first_historical_data": "2019-10-05T20:06:32.716Z",
        "last_historical_data": "2020-08-17T23:12:28.280Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xba2184520a1cc49a6159c57e61e1844e085615b6"
        }
      },
      {
        "id": 2043,
        "name": "Cindicator",
        "symbol": "CND",
        "slug": "cindicator",
        "is_active": 1,
        "first_historical_data": "2020-07-14T19:01:16.634Z",
        "last_historical_data": "2020-08-18T08:55:54.231Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd4c435f5b09f855c3317c8524cb1f586e42795fa"
        }
      },
      {
        "id": 2044,
        "name": "Enigma",
        "symbol": "ENG",
        "slug": "enigma",
        "is_active": 1,
        "first_historical_data": "2020-04-17T06:14:01.187Z",
        "last_historical_data": "2020-08-18T06:05:33.284Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf0ee6b27b759c9893ce4f094b49ad28fd15a23e4"
        }
      },
      {
        "id": 2047,
        "name": "Zeusshield",
        "symbol": "ZSC",
        "slug": "zeusshield",
        "is_active": 1,
        "first_historical_data": "2020-04-02T21:39:40.116Z",
        "last_historical_data": "2020-08-18T14:31:53.951Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x7a41e0517a5eca4fdbc7fbeba4d4c47b9ff6dc63"
        }
      },
      {
        "id": 2048,
        "name": "Ethereum Cash",
        "symbol": "ECASH",
        "slug": "ethereumcash",
        "is_active": 1,
        "first_historical_data": "2019-11-20T07:55:33.299Z",
        "last_historical_data": "2020-08-18T14:14:45.738Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5d21ef5f25a985380b65c8e943a0082feda0db84"
        }
      },
      {
        "id": 2051,
        "name": "Authorship",
        "symbol": "ATS",
        "slug": "authorship",
        "is_active": 1,
        "first_historical_data": "2020-04-08T07:32:29.871Z",
        "last_historical_data": "2020-08-17T19:23:24.983Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2daee1aa61d60a252dc80564499a69802853583a"
        }
      },
      {
        "id": 2056,
        "name": "PiplCoin",
        "symbol": "PIPL",
        "slug": "piplcoin",
        "is_active": 1,
        "first_historical_data": "2019-08-25T21:43:37.929Z",
        "last_historical_data": "2020-08-18T07:14:20.901Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe64509f0bf07ce2d29a7ef19a8a9bc065477c1b4"
        }
      },
      {
        "id": 2058,
        "name": "AirSwap",
        "symbol": "AST",
        "slug": "airswap",
        "is_active": 1,
        "first_historical_data": "2019-11-30T06:52:29.342Z",
        "last_historical_data": "2020-08-17T20:30:09.042Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x27054b13b1b798b345b591a4d22e6562d47ea75a"
        }
      },
      {
        "id": 2060,
        "name": "Change",
        "symbol": "CAG",
        "slug": "change",
        "is_active": 1,
        "first_historical_data": "2019-08-30T05:16:38.171Z",
        "last_historical_data": "2020-08-18T01:31:28.461Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x7d4b8cce0591c9044a22ee543533b72e976e36c3"
        }
      },
      {
        "id": 2061,
        "name": "Blockmason Credit Protocol",
        "symbol": "BCPT",
        "slug": "blockmason",
        "is_active": 1,
        "first_historical_data": "2019-11-09T14:05:22.803Z",
        "last_historical_data": "2020-08-18T09:51:43.524Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "BCPT-95A"
        }
      },
      {
        "id": 2062,
        "name": "Aion",
        "symbol": "AION",
        "slug": "aion",
        "is_active": 1,
        "first_historical_data": "2019-11-03T16:31:58.392Z",
        "last_historical_data": "2020-08-18T12:26:24.938Z",
        "platform": null
      },
      {
        "id": 2064,
        "name": "Maecenas",
        "symbol": "ART",
        "slug": "maecenas",
        "is_active": 1,
        "first_historical_data": "2020-05-19T14:09:57.014Z",
        "last_historical_data": "2020-08-18T01:04:08.478Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xfec0cf7fe078a500abf15f1284958f22049c2c7e"
        }
      },
      {
        "id": 2065,
        "name": "XGOX",
        "symbol": "XGOX",
        "slug": "xgox",
        "is_active": 1,
        "first_historical_data": "2019-08-28T02:20:38.628Z",
        "last_historical_data": "2020-08-17T21:57:14.564Z",
        "platform": null
      },
      {
        "id": 2066,
        "name": "Everus",
        "symbol": "EVR",
        "slug": "everus",
        "is_active": 1,
        "first_historical_data": "2020-03-04T23:09:08.038Z",
        "last_historical_data": "2020-08-18T00:03:27.418Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x3137619705b5fc22a3048989f983905e456b59ab"
        }
      },
      {
        "id": 2069,
        "name": "Open Trading Network",
        "symbol": "OTN",
        "slug": "open-trading-network",
        "is_active": 1,
        "first_historical_data": "2019-12-03T07:25:12.723Z",
        "last_historical_data": "2020-08-18T02:47:15.809Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x881ef48211982d01e2cb7092c915e647cd40d85c"
        }
      },
      {
        "id": 2070,
        "name": "DomRaider",
        "symbol": "DRT",
        "slug": "domraider",
        "is_active": 1,
        "first_historical_data": "2019-10-29T05:27:15.089Z",
        "last_historical_data": "2020-08-18T08:48:32.696Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9af4f26941677c706cfecf6d3379ff01bb85d5ab"
        }
      },
      {
        "id": 2071,
        "name": "Request",
        "symbol": "REQ",
        "slug": "request",
        "is_active": 1,
        "first_historical_data": "2020-04-08T05:01:22.332Z",
        "last_historical_data": "2020-08-18T00:52:28.696Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8f8221afbb33998d8584a2b05749ba73c37a938a"
        }
      },
      {
        "id": 2074,
        "name": "Ethereum Gold",
        "symbol": "ETG",
        "slug": "ethereum-gold",
        "is_active": 1,
        "first_historical_data": "2020-06-27T20:11:47.197Z",
        "last_historical_data": "2020-08-18T14:59:38.424Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x28c8d01ff633ea9cd8fc6a451d7457889e698de6"
        }
      },
      {
        "id": 2076,
        "name": "Blue Protocol",
        "symbol": "BLUE",
        "slug": "ethereum-blue",
        "is_active": 1,
        "first_historical_data": "2020-07-19T01:47:23.535Z",
        "last_historical_data": "2020-08-18T16:39:52.679Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x539efe69bcdd21a83efd9122571a64cc25e0282b"
        }
      },
      {
        "id": 2078,
        "name": "LIFE",
        "symbol": "LIFE",
        "slug": "life",
        "is_active": 1,
        "first_historical_data": "2020-06-13T10:13:05.195Z",
        "last_historical_data": "2020-08-18T01:28:03.668Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xff18dbc487b4c2e3222d115952babfda8ba52f5f"
        }
      },
      {
        "id": 2081,
        "name": "Ambrosus",
        "symbol": "AMB",
        "slug": "amber",
        "is_active": 1,
        "first_historical_data": "2020-01-23T13:33:44.083Z",
        "last_historical_data": "2020-08-17T18:38:19.939Z",
        "platform": null
      },
      {
        "id": 2083,
        "name": "Bitcoin Gold",
        "symbol": "BTG",
        "slug": "bitcoin-gold",
        "is_active": 1,
        "first_historical_data": "2020-03-10T16:26:04.226Z",
        "last_historical_data": "2020-08-18T17:40:21.553Z",
        "platform": null
      },
      {
        "id": 2087,
        "name": "KuCoin Shares",
        "symbol": "KCS",
        "slug": "kucoin-shares",
        "is_active": 1,
        "first_historical_data": "2020-05-23T23:44:08.881Z",
        "last_historical_data": "2020-08-18T17:23:05.043Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x039b5649a59967e3e936d7471f9c3700100ee1ab"
        }
      },
      {
        "id": 2088,
        "name": "EXRNchain",
        "symbol": "EXRN",
        "slug": "exrnchain",
        "is_active": 1,
        "first_historical_data": "2019-11-24T10:42:10.094Z",
        "last_historical_data": "2020-08-18T11:11:50.924Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe469c4473af82217b30cf17b10bcdb6c8c796e75"
        }
      },
      {
        "id": 2089,
        "name": "ClearPoll",
        "symbol": "POLL",
        "slug": "clearpoll",
        "is_active": 1,
        "first_historical_data": "2020-07-11T04:59:59.437Z",
        "last_historical_data": "2020-08-18T11:46:41.068Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x705EE96c1c160842C92c1aeCfCFfccc9C412e3D9"
        }
      },
      {
        "id": 2090,
        "name": "LATOKEN",
        "symbol": "LA",
        "slug": "latoken",
        "is_active": 1,
        "first_historical_data": "2020-01-10T23:04:36.732Z",
        "last_historical_data": "2020-08-17T21:14:04.714Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe50365f5d679cb98a1dd62d6f6e58e59321bcddf"
        }
      },
      {
        "id": 2091,
        "name": "Exchange Union",
        "symbol": "XUC",
        "slug": "exchange-union",
        "is_active": 1,
        "first_historical_data": "2020-01-05T04:29:19.104Z",
        "last_historical_data": "2020-08-17T22:45:12.355Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc324a2f6b05880503444451b8b27e6f9e63287cb"
        }
      },
      {
        "id": 2092,
        "name": "NULS",
        "symbol": "NULS",
        "slug": "nuls",
        "is_active": 1,
        "first_historical_data": "2020-04-03T21:50:14.189Z",
        "last_historical_data": "2020-08-18T09:57:43.578Z",
        "platform": null
      },
      {
        "id": 2093,
        "name": "Bitcoin Red",
        "symbol": "BTCRED",
        "slug": "bitcoin-red",
        "is_active": 1,
        "first_historical_data": "2020-01-08T04:56:37.386Z",
        "last_historical_data": "2020-08-18T14:31:07.652Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6aac8cb9861e42bf8259f5abdc6ae3ae89909e11"
        }
      },
      {
        "id": 2094,
        "name": "Paragon",
        "symbol": "PRG",
        "slug": "paragon",
        "is_active": 1,
        "first_historical_data": "2020-07-29T15:57:44.510Z",
        "last_historical_data": "2020-08-17T20:13:16.090Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x7728dFEF5aBd468669EB7f9b48A7f70a501eD29D"
        }
      },
      {
        "id": 2095,
        "name": "BOScoin",
        "symbol": "BOS",
        "slug": "boscoin",
        "is_active": 1,
        "first_historical_data": "2020-02-15T23:28:42.112Z",
        "last_historical_data": "2020-08-17T18:44:46.859Z",
        "platform": null
      },
      {
        "id": 2096,
        "name": "Ripio Credit Network",
        "symbol": "RCN",
        "slug": "ripio-credit-network",
        "is_active": 1,
        "first_historical_data": "2020-05-12T03:02:37.220Z",
        "last_historical_data": "2020-08-18T13:25:19.765Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf970b8e36e23f7fc3fd752eea86f8be8d83375a6"
        }
      },
      {
        "id": 2099,
        "name": "ICON",
        "symbol": "ICX",
        "slug": "icon",
        "is_active": 1,
        "first_historical_data": "2020-01-11T21:24:55.956Z",
        "last_historical_data": "2020-08-17T20:50:19.422Z",
        "platform": null
      },
      {
        "id": 2100,
        "name": "JavaScript Token",
        "symbol": "JS",
        "slug": "javascript-token",
        "is_active": 1,
        "first_historical_data": "2019-10-04T16:55:19.223Z",
        "last_historical_data": "2020-08-18T15:40:53.498Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5046e860ff274fb8c66106b0ffb8155849fb0787"
        }
      },
      {
        "id": 2101,
        "name": "Ethereum Lite",
        "symbol": "ELITE",
        "slug": "ethereum-lite",
        "is_active": 1,
        "first_historical_data": "2019-09-19T14:24:10.230Z",
        "last_historical_data": "2020-08-17T23:33:02.100Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0a76aad21948ea1ef447d26dee91a54370e151e0"
        }
      },
      {
        "id": 2103,
        "name": "Intelligent Trading Foundation",
        "symbol": "ITT",
        "slug": "intelligent-trading-foundation",
        "is_active": 1,
        "first_historical_data": "2019-10-27T23:46:52.463Z",
        "last_historical_data": "2020-08-18T02:33:48.468Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0aef06dcccc531e581f0440059e6ffcc206039ee"
        }
      },
      {
        "id": 2104,
        "name": "iEthereum",
        "symbol": "IETH",
        "slug": "iethereum",
        "is_active": 1,
        "first_historical_data": "2019-10-26T12:26:14.722Z",
        "last_historical_data": "2020-08-18T13:12:21.049Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x859a9c0b44cb7066d956a958b0b82e54c9e44b4b"
        }
      },
      {
        "id": 2105,
        "name": "Pirl",
        "symbol": "PIRL",
        "slug": "pirl",
        "is_active": 1,
        "first_historical_data": "2020-03-10T21:23:59.282Z",
        "last_historical_data": "2020-08-18T10:26:43.923Z",
        "platform": null
      },
      {
        "id": 2107,
        "name": "LUXCoin",
        "symbol": "LUX",
        "slug": "luxcoin",
        "is_active": 1,
        "first_historical_data": "2020-01-13T01:51:00.708Z",
        "last_historical_data": "2020-08-17T20:55:37.416Z",
        "platform": null
      },
      {
        "id": 2110,
        "name": "Dovu",
        "symbol": "DOV",
        "slug": "dovu",
        "is_active": 1,
        "first_historical_data": "2020-07-10T12:23:51.340Z",
        "last_historical_data": "2020-08-18T06:51:30.988Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xac3211a5025414af2866ff09c23fc18bc97e79b1"
        }
      },
      {
        "id": 2112,
        "name": "Red Pulse Phoenix",
        "symbol": "PHX",
        "slug": "red-pulse",
        "is_active": 1,
        "first_historical_data": "2020-02-10T07:05:08.985Z",
        "last_historical_data": "2020-08-18T09:05:45.361Z",
        "platform": {
          "id": 1376,
          "name": "Neo",
          "symbol": "NEO",
          "slug": "neo",
          "token_address": "1578103c13e39df15d0d29826d957e85d770d8c9"
        }
      },
      {
        "id": 2119,
        "name": "BTCMoon",
        "symbol": "BTCM",
        "slug": "btcmoon",
        "is_active": 1,
        "first_historical_data": "2020-07-15T13:46:02.681Z",
        "last_historical_data": "2020-08-18T10:58:51.796Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa9aad2dc3a8315caeee5f458b1d8edc31d8467bd"
        }
      },
      {
        "id": 2120,
        "name": "Etherparty",
        "symbol": "FUEL",
        "slug": "etherparty",
        "is_active": 1,
        "first_historical_data": "2019-08-31T19:05:15.430Z",
        "last_historical_data": "2020-08-18T17:37:29.566Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xea38eaa3c86c8f9b751533ba2e562deb9acded40"
        }
      },
      {
        "id": 2122,
        "name": "Ellaism",
        "symbol": "ELLA",
        "slug": "ellaism",
        "is_active": 1,
        "first_historical_data": "2019-10-06T02:57:52.492Z",
        "last_historical_data": "2020-08-18T08:50:26.492Z",
        "platform": null
      },
      {
        "id": 2126,
        "name": "FlypMe",
        "symbol": "FYP",
        "slug": "flypme",
        "is_active": 1,
        "first_historical_data": "2020-04-28T06:42:17.054Z",
        "last_historical_data": "2020-08-18T02:16:34.556Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8f0921f30555624143d427b340b1156914882c10"
        }
      },
      {
        "id": 2127,
        "name": "eBitcoin",
        "symbol": "EBTC",
        "slug": "ebtcnew",
        "is_active": 1,
        "first_historical_data": "2020-02-04T15:00:20.046Z",
        "last_historical_data": "2020-08-18T05:15:27.123Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xeb7c20027172e5d143fb030d50f91cece2d1485d"
        }
      },
      {
        "id": 2130,
        "name": "Enjin Coin",
        "symbol": "ENJ",
        "slug": "enjin-coin",
        "is_active": 1,
        "first_historical_data": "2019-09-29T00:35:16.365Z",
        "last_historical_data": "2020-08-18T09:27:38.255Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf629cbd94d3791c9250152bd8dfbdf380e2a3b9c"
        }
      },
      {
        "id": 2131,
        "name": "iBTC",
        "symbol": "IBTC",
        "slug": "ibtc",
        "is_active": 1,
        "first_historical_data": "2020-01-02T02:33:16.631Z",
        "last_historical_data": "2020-08-18T00:38:43.475Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb7c4a82936194fee52a4e3d4cec3415f74507532"
        }
      },
      {
        "id": 2132,
        "name": "Power Ledger",
        "symbol": "POWR",
        "slug": "power-ledger",
        "is_active": 1,
        "first_historical_data": "2019-10-20T19:02:43.843Z",
        "last_historical_data": "2020-08-18T03:22:20.541Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x595832f8fc6bf59c85c527fec3740a1b7a361269"
        }
      },
      {
        "id": 2134,
        "name": "Grid+",
        "symbol": "GRID",
        "slug": "grid",
        "is_active": 1,
        "first_historical_data": "2020-03-23T23:58:09.439Z",
        "last_historical_data": "2020-08-18T03:08:39.645Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x12b19d3e2ccc14da04fae33e63652ce469b3f2fd"
        }
      },
      {
        "id": 2135,
        "name": "Revain",
        "symbol": "REV",
        "slug": "revain",
        "is_active": 1,
        "first_historical_data": "2020-07-07T02:24:42.077Z",
        "last_historical_data": "2020-08-18T05:26:36.781Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2ef52Ed7De8c5ce03a4eF0efbe9B7450F2D7Edc9"
        }
      },
      {
        "id": 2136,
        "name": "ATLANT",
        "symbol": "ATL",
        "slug": "atlant",
        "is_active": 1,
        "first_historical_data": "2020-07-04T02:31:06.307Z",
        "last_historical_data": "2020-08-18T12:35:19.980Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x78b7fada55a64dd895d8c8c35779dd8b67fa8a05"
        }
      },
      {
        "id": 2137,
        "name": "Electroneum",
        "symbol": "ETN",
        "slug": "electroneum",
        "is_active": 1,
        "first_historical_data": "2020-02-10T15:09:04.073Z",
        "last_historical_data": "2020-08-18T04:55:20.817Z",
        "platform": null
      },
      {
        "id": 2140,
        "name": "SONO",
        "symbol": "SONO",
        "slug": "altcommunity-coin",
        "is_active": 1,
        "first_historical_data": "2019-09-01T15:35:41.105Z",
        "last_historical_data": "2020-08-17T20:51:33.024Z",
        "platform": null
      },
      {
        "id": 2143,
        "name": "Streamr",
        "symbol": "DATA",
        "slug": "streamr",
        "is_active": 1,
        "first_historical_data": "2020-01-13T08:57:32.899Z",
        "last_historical_data": "2020-08-18T07:25:45.042Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0cf0ee63788a0849fe5297f3407f701e122cc023"
        }
      },
      {
        "id": 2144,
        "name": "SHIELD",
        "symbol": "XSH",
        "slug": "shield-xsh",
        "is_active": 1,
        "first_historical_data": "2020-05-08T17:21:02.384Z",
        "last_historical_data": "2020-08-18T05:32:08.996Z",
        "platform": null
      },
      {
        "id": 2147,
        "name": "ELTCOIN",
        "symbol": "ELTCOIN",
        "slug": "eltcoin",
        "is_active": 1,
        "first_historical_data": "2020-03-22T23:21:27.527Z",
        "last_historical_data": "2020-08-18T14:40:49.655Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x44197a4c44d6a059297caf6be4f7e172bd56caaf"
        }
      },
      {
        "id": 2148,
        "name": "Desire",
        "symbol": "DSR",
        "slug": "desire",
        "is_active": 1,
        "first_historical_data": "2020-04-23T16:13:21.081Z",
        "last_historical_data": "2020-08-18T09:51:15.006Z",
        "platform": null
      },
      {
        "id": 2149,
        "name": "Unikoin Gold",
        "symbol": "UKG",
        "slug": "unikoin-gold",
        "is_active": 1,
        "first_historical_data": "2020-01-11T23:57:59.478Z",
        "last_historical_data": "2020-08-18T15:44:01.675Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x24692791bc444c5cd0b81e3cbcaba4b04acd1f3b"
        }
      },
      {
        "id": 2151,
        "name": "Autonio",
        "symbol": "NIO",
        "slug": "autonio",
        "is_active": 1,
        "first_historical_data": "2019-09-17T21:51:16.380Z",
        "last_historical_data": "2020-08-18T01:07:33.562Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5554e04e76533e1d14c52f05beef6c9d329e1e30"
        }
      },
      {
        "id": 2153,
        "name": "Aeron",
        "symbol": "ARN",
        "slug": "aeron",
        "is_active": 1,
        "first_historical_data": "2020-01-10T15:42:06.604Z",
        "last_historical_data": "2020-08-18T04:39:22.637Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "ARN-71B"
        }
      },
      {
        "id": 2158,
        "name": "Phore",
        "symbol": "PHR",
        "slug": "phore",
        "is_active": 1,
        "first_historical_data": "2019-11-17T23:51:39.599Z",
        "last_historical_data": "2020-08-17T20:43:12.337Z",
        "platform": null
      },
      {
        "id": 2161,
        "name": "Raiden Network Token",
        "symbol": "RDN",
        "slug": "raiden-network-token",
        "is_active": 1,
        "first_historical_data": "2019-12-07T07:59:42.409Z",
        "last_historical_data": "2020-08-18T15:33:04.442Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x255aa6df07540cb5d3d297f0d0d4d84cb52bc8e6"
        }
      },
      {
        "id": 2162,
        "name": "Delphy",
        "symbol": "DPY",
        "slug": "delphy",
        "is_active": 1,
        "first_historical_data": "2020-07-29T15:00:52.062Z",
        "last_historical_data": "2020-08-18T13:55:35.285Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6c2adc2073994fb2ccc5032cc2906fa221e9b391"
        }
      },
      {
        "id": 2165,
        "name": "ERC20",
        "symbol": "ERC20",
        "slug": "erc20",
        "is_active": 1,
        "first_historical_data": "2019-10-10T01:34:46.417Z",
        "last_historical_data": "2020-08-18T06:52:22.588Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc3761eb917cd790b30dad99f6cc5b4ff93c4f9ea"
        }
      },
      {
        "id": 2166,
        "name": "Ties.DB",
        "symbol": "TIE",
        "slug": "tiesdb",
        "is_active": 1,
        "first_historical_data": "2020-06-06T07:48:12.878Z",
        "last_historical_data": "2020-08-18T10:30:00.533Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x999967e2ec8a74b7c8e9db19e039d920b31d39d0"
        }
      },
      {
        "id": 2172,
        "name": "Emphy",
        "symbol": "EPY",
        "slug": "emphy",
        "is_active": 1,
        "first_historical_data": "2020-06-18T06:28:10.832Z",
        "last_historical_data": "2020-08-18T08:02:05.762Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x50ee674689d75c0f88e8f83cfe8c4b69e8fd590d"
        }
      },
      {
        "id": 2175,
        "name": "DecentBet",
        "symbol": "DBET",
        "slug": "decent-bet",
        "is_active": 1,
        "first_historical_data": "2020-03-09T13:59:16.636Z",
        "last_historical_data": "2020-08-18T11:36:54.071Z",
        "platform": {
          "id": 3077,
          "name": "VeChain",
          "symbol": "VET",
          "slug": "vechain",
          "token_address": "0x9b68bfae21df5a510931a262cecf63f41338f264"
        }
      },
      {
        "id": 2178,
        "name": "Upfiring",
        "symbol": "UFR",
        "slug": "upfiring",
        "is_active": 1,
        "first_historical_data": "2020-04-07T13:47:21.297Z",
        "last_historical_data": "2020-08-18T04:27:26.051Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xea097a2b1db00627b2fa17460ad260c016016977"
        }
      },
      {
        "id": 2180,
        "name": "bitJob",
        "symbol": "STU",
        "slug": "bitjob",
        "is_active": 1,
        "first_historical_data": "2019-11-03T18:18:41.607Z",
        "last_historical_data": "2020-08-18T16:57:34.356Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0371a82e4a9d0a4312f3ee2ac9c6958512891372"
        }
      },
      {
        "id": 2181,
        "name": "Genesis Vision",
        "symbol": "GVT",
        "slug": "genesis-vision",
        "is_active": 1,
        "first_historical_data": "2020-06-01T07:48:02.417Z",
        "last_historical_data": "2020-08-18T00:57:10.160Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x103c3a209da59d3e7c4a89307e66521e081cfdf0"
        }
      },
      {
        "id": 2184,
        "name": "Privatix",
        "symbol": "PRIX",
        "slug": "privatix",
        "is_active": 1,
        "first_historical_data": "2020-02-05T08:51:32.179Z",
        "last_historical_data": "2020-08-18T08:52:52.506Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x3adfc4999f77d04c8341bac5f3a76f58dff5b37a"
        }
      },
      {
        "id": 2185,
        "name": "Lethean",
        "symbol": "LTHN",
        "slug": "lethean",
        "is_active": 1,
        "first_historical_data": "2019-12-23T10:03:50.748Z",
        "last_historical_data": "2020-08-18T03:19:36.778Z",
        "platform": null
      },
      {
        "id": 2191,
        "name": "Paypex",
        "symbol": "PAYX",
        "slug": "paypex",
        "is_active": 1,
        "first_historical_data": "2020-01-23T04:16:19.083Z",
        "last_historical_data": "2020-08-18T09:40:57.928Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x62a56a4a2ef4d355d34d10fbf837e747504d38d4"
        }
      },
      {
        "id": 2199,
        "name": "ALQO",
        "symbol": "XLQ",
        "slug": "alqo",
        "is_active": 1,
        "first_historical_data": "2019-11-30T15:02:42.219Z",
        "last_historical_data": "2020-08-18T02:43:01.273Z",
        "platform": null
      },
      {
        "id": 2200,
        "name": "GoByte",
        "symbol": "GBX",
        "slug": "gobyte",
        "is_active": 1,
        "first_historical_data": "2020-05-13T18:16:47.969Z",
        "last_historical_data": "2020-08-18T07:57:27.951Z",
        "platform": null
      },
      {
        "id": 2201,
        "name": "WINCOIN",
        "symbol": "WC",
        "slug": "win-coin",
        "is_active": 1,
        "first_historical_data": "2020-05-18T19:54:01.004Z",
        "last_historical_data": "2020-08-17T22:52:56.769Z",
        "platform": null
      },
      {
        "id": 2204,
        "name": "B2BX",
        "symbol": "B2B",
        "slug": "b2bx",
        "is_active": 1,
        "first_historical_data": "2020-02-15T18:26:27.306Z",
        "last_historical_data": "2020-08-18T04:14:30.879Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5d51fcced3114a8bb5e90cdd0f9d682bcbcc5393"
        }
      },
      {
        "id": 2205,
        "name": "Phantomx",
        "symbol": "PNX",
        "slug": "phantomx",
        "is_active": 1,
        "first_historical_data": "2020-03-12T07:10:19.070Z",
        "last_historical_data": "2020-08-18T11:59:47.066Z",
        "platform": null
      },
      {
        "id": 2208,
        "name": "EncrypGen",
        "symbol": "DNA",
        "slug": "encrypgen",
        "is_active": 1,
        "first_historical_data": "2019-10-29T12:15:18.504Z",
        "last_historical_data": "2020-08-18T11:47:08.670Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x82b0e50478eeafde392d45d1259ed1071b6fda81"
        }
      },
      {
        "id": 2209,
        "name": "Ink",
        "symbol": "INK",
        "slug": "ink",
        "is_active": 1,
        "first_historical_data": "2020-04-21T12:58:15.881Z",
        "last_historical_data": "2020-08-17T19:22:43.247Z",
        "platform": {
          "id": 1684,
          "name": "Qtum",
          "symbol": "QTUM",
          "slug": "qtum",
          "token_address": "fe59cbc1704e89a698571413a81f0de9d8f00c69"
        }
      },
      {
        "id": 2212,
        "name": "Quantstamp",
        "symbol": "QSP",
        "slug": "quantstamp",
        "is_active": 1,
        "first_historical_data": "2020-02-03T17:39:35.917Z",
        "last_historical_data": "2020-08-18T11:27:20.526Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x99ea4db9ee77acd40b119bd1dc4e33e1c070b80d"
        }
      },
      {
        "id": 2213,
        "name": "QASH",
        "symbol": "QASH",
        "slug": "qash",
        "is_active": 1,
        "first_historical_data": "2020-07-25T11:59:50.543Z",
        "last_historical_data": "2020-08-17T18:36:40.597Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x618e75ac90b12c6049ba3b27f5d5f8651b0037f6"
        }
      },
      {
        "id": 2215,
        "name": "Energo",
        "symbol": "TSL",
        "slug": "energo",
        "is_active": 1,
        "first_historical_data": "2020-02-15T23:55:51.923Z",
        "last_historical_data": "2020-08-18T00:22:52.002Z",
        "platform": {
          "id": 1684,
          "name": "Qtum",
          "symbol": "QTUM",
          "slug": "qtum",
          "token_address": "d8dec2b605005749abbf4b060edad3070e23cf5c"
        }
      },
      {
        "id": 2219,
        "name": "SpankChain",
        "symbol": "SPANK",
        "slug": "spankchain",
        "is_active": 1,
        "first_historical_data": "2020-06-12T02:55:00.721Z",
        "last_historical_data": "2020-08-18T10:56:03.258Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x42d6622dece394b54999fbd73d108123806f6a18"
        }
      },
      {
        "id": 2221,
        "name": "VoteCoin",
        "symbol": "VOT",
        "slug": "votecoin",
        "is_active": 1,
        "first_historical_data": "2019-10-22T22:39:26.826Z",
        "last_historical_data": "2020-08-18T13:04:22.036Z",
        "platform": null
      },
      {
        "id": 2222,
        "name": "Bitcoin Diamond",
        "symbol": "BCD",
        "slug": "bitcoin-diamond",
        "is_active": 1,
        "first_historical_data": "2020-08-01T07:52:59.544Z",
        "last_historical_data": "2020-08-18T00:06:04.868Z",
        "platform": null
      },
      {
        "id": 2223,
        "name": "BLOCKv",
        "symbol": "VEE",
        "slug": "blockv",
        "is_active": 1,
        "first_historical_data": "2020-05-14T01:22:46.546Z",
        "last_historical_data": "2020-08-18T16:46:41.389Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x340d2bde5eb28c1eed91b2f790723e3b160613b7"
        }
      },
      {
        "id": 2230,
        "name": "Monkey Project",
        "symbol": "MONK",
        "slug": "monkey-project",
        "is_active": 1,
        "first_historical_data": "2019-10-28T13:21:22.437Z",
        "last_historical_data": "2020-08-18T04:04:52.219Z",
        "platform": null
      },
      {
        "id": 2231,
        "name": "Flixxo",
        "symbol": "FLIXX",
        "slug": "flixxo",
        "is_active": 1,
        "first_historical_data": "2020-04-18T23:57:24.624Z",
        "last_historical_data": "2020-08-18T14:24:20.375Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf04a8ac553fcedb5ba99a64799155826c136b0be"
        }
      },
      {
        "id": 2235,
        "name": "Time New Bank",
        "symbol": "TNB",
        "slug": "time-new-bank",
        "is_active": 1,
        "first_historical_data": "2020-06-26T15:21:21.719Z",
        "last_historical_data": "2020-08-18T15:15:24.751Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf7920b0768ecb20a123fac32311d07d193381d6f"
        }
      },
      {
        "id": 2236,
        "name": "MyWish",
        "symbol": "WISH",
        "slug": "mywish",
        "is_active": 1,
        "first_historical_data": "2020-05-25T11:20:06.335Z",
        "last_historical_data": "2020-08-17T21:51:53.846Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "WISH-2D5"
        }
      },
      {
        "id": 2237,
        "name": "EventChain",
        "symbol": "EVC",
        "slug": "eventchain",
        "is_active": 1,
        "first_historical_data": "2019-10-30T12:17:27.116Z",
        "last_historical_data": "2020-08-18T03:45:40.906Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb62d18dea74045e822352ce4b3ee77319dc5ff2f"
        }
      },
      {
        "id": 2239,
        "name": "Aave",
        "symbol": "LEND",
        "slug": "aave",
        "is_active": 1,
        "first_historical_data": "2019-08-20T18:29:02.005Z",
        "last_historical_data": "2020-08-18T14:36:14.627Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x80fb784b7ed66730e8b1dbd9820afd29931aab03"
        }
      },
      {
        "id": 2240,
        "name": "SoMee.Social",
        "symbol": "ONG",
        "slug": "ongsocial",
        "is_active": 1,
        "first_historical_data": "2020-05-27T20:18:32.050Z",
        "last_historical_data": "2020-08-18T08:14:46.584Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd341d1680eeee3255b8c4c75bcce7eb57f144dae"
        }
      },
      {
        "id": 2241,
        "name": "Ccore",
        "symbol": "CCO",
        "slug": "ccore",
        "is_active": 1,
        "first_historical_data": "2019-12-28T12:25:48.306Z",
        "last_historical_data": "2020-08-17T20:52:36.287Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x679badc551626e01b23ceecefbc9b877ea18fc46"
        }
      },
      {
        "id": 2242,
        "name": "Qbao",
        "symbol": "QBT",
        "slug": "qbao",
        "is_active": 1,
        "first_historical_data": "2019-12-21T01:32:58.494Z",
        "last_historical_data": "2020-08-18T07:38:05.628Z",
        "platform": {
          "id": 1684,
          "name": "Qtum",
          "symbol": "QTUM",
          "slug": "qtum",
          "token_address": "QMUDYjQpjkxZjjfEdKznqsim7VMS5LZzqp"
        }
      },
      {
        "id": 2243,
        "name": "Dragonchain",
        "symbol": "DRGN",
        "slug": "dragonchain",
        "is_active": 1,
        "first_historical_data": "2019-09-01T09:44:48.423Z",
        "last_historical_data": "2020-08-17T19:39:05.534Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x419c4db4b9e25d6db2ad9691ccb832c8d9fda05e"
        }
      },
      {
        "id": 2244,
        "name": "Payfair",
        "symbol": "PFR",
        "slug": "payfair",
        "is_active": 1,
        "first_historical_data": "2020-06-16T23:20:02.338Z",
        "last_historical_data": "2020-08-18T04:19:06.490Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xB41422D5a1d5d5C73c229686935b40F881502785"
        }
      },
      {
        "id": 2245,
        "name": "Presearch",
        "symbol": "PRE",
        "slug": "presearch",
        "is_active": 1,
        "first_historical_data": "2020-07-17T11:39:54.387Z",
        "last_historical_data": "2020-08-18T10:03:19.299Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x88a3e4f35d64aad41a6d4030ac9afe4356cb84fa"
        }
      },
      {
        "id": 2246,
        "name": "CyberMiles",
        "symbol": "CMT",
        "slug": "cybermiles",
        "is_active": 1,
        "first_historical_data": "2019-11-23T22:41:13.352Z",
        "last_historical_data": "2020-08-18T16:49:14.882Z",
        "platform": null
      },
      {
        "id": 2247,
        "name": "BlockCDN",
        "symbol": "BCDN",
        "slug": "blockcdn",
        "is_active": 1,
        "first_historical_data": "2019-10-08T12:47:59.076Z",
        "last_historical_data": "2020-08-17T18:36:11.052Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1e797Ce986C3CFF4472F7D38d5C4aba55DfEFE40"
        }
      },
      {
        "id": 2248,
        "name": "Cappasity",
        "symbol": "CAPP",
        "slug": "cappasity",
        "is_active": 1,
        "first_historical_data": "2020-08-05T23:34:13.919Z",
        "last_historical_data": "2020-08-18T13:08:34.118Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x04f2e7221fdb1b52a68169b25793e51478ff0329"
        }
      },
      {
        "id": 2249,
        "name": "Eroscoin",
        "symbol": "ERO",
        "slug": "eroscoin",
        "is_active": 1,
        "first_historical_data": "2019-10-21T10:56:40.855Z",
        "last_historical_data": "2020-08-18T06:27:12.615Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x74ceda77281b339142a36817fa5f9e29412bab85"
        }
      },
      {
        "id": 2251,
        "name": "IoT Chain",
        "symbol": "ITC",
        "slug": "iot-chain",
        "is_active": 1,
        "first_historical_data": "2020-07-19T02:04:08.518Z",
        "last_historical_data": "2020-08-18T17:01:13.095Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5e6b6d9abad9093fdc861ea1600eba1b355cd940"
        }
      },
      {
        "id": 2255,
        "name": "Social Send",
        "symbol": "SEND",
        "slug": "social-send",
        "is_active": 1,
        "first_historical_data": "2019-09-24T04:57:11.401Z",
        "last_historical_data": "2020-08-17T21:14:48.261Z",
        "platform": null
      },
      {
        "id": 2256,
        "name": "Bonpay",
        "symbol": "BON",
        "slug": "bonpay",
        "is_active": 1,
        "first_historical_data": "2019-08-30T08:51:30.062Z",
        "last_historical_data": "2020-08-18T06:23:25.412Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xcc34366e3842ca1bd36c1f324d15257960fcc801"
        }
      },
      {
        "id": 2257,
        "name": "Nekonium",
        "symbol": "NUKO",
        "slug": "nekonium",
        "is_active": 1,
        "first_historical_data": "2019-12-04T22:15:02.482Z",
        "last_historical_data": "2020-08-18T17:24:06.152Z",
        "platform": null
      },
      {
        "id": 2258,
        "name": "Snovian.Space",
        "symbol": "SNOV",
        "slug": "snov",
        "is_active": 1,
        "first_historical_data": "2020-06-03T02:40:41.860Z",
        "last_historical_data": "2020-08-17T21:07:39.101Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xbdc5bac39dbe132b1e030e898ae3830017d7d969"
        }
      },
      {
        "id": 2260,
        "name": "Datamine",
        "symbol": "DAM",
        "slug": "datamine",
        "is_active": 1,
        "first_historical_data": "2020-06-04T11:33:56.149Z",
        "last_historical_data": "2020-08-18T07:49:12.834Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf80d589b3dbe130c270a69f1a69d050f268786df"
        }
      },
      {
        "id": 2262,
        "name": "COMSA [ETH]",
        "symbol": "CMS",
        "slug": "comsa-eth",
        "is_active": 1,
        "first_historical_data": "2020-01-31T03:36:44.177Z",
        "last_historical_data": "2020-08-18T11:58:11.932Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf83301c5cd1ccbb86f466a6b3c53316ed2f8465a"
        }
      },
      {
        "id": 2266,
        "name": "COMSA [XEM]",
        "symbol": "CMS",
        "slug": "comsa-xem",
        "is_active": 1,
        "first_historical_data": "2019-12-27T01:35:18.323Z",
        "last_historical_data": "2020-08-17T22:53:00.525Z",
        "platform": {
          "id": 873,
          "name": "NEM",
          "symbol": "XEM",
          "slug": "nem",
          "token_address": "comsa"
        }
      },
      {
        "id": 2267,
        "name": "Tael",
        "symbol": "WABI",
        "slug": "tael",
        "is_active": 1,
        "first_historical_data": "2020-06-19T05:42:27.459Z",
        "last_historical_data": "2020-08-18T05:53:19.691Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x286bda1413a2df81731d4930ce2f862a35a609fe"
        }
      },
      {
        "id": 2269,
        "name": "WandX",
        "symbol": "WAND",
        "slug": "wandx",
        "is_active": 1,
        "first_historical_data": "2020-02-04T17:33:21.542Z",
        "last_historical_data": "2020-08-18T11:29:25.412Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x27f610bf36eca0939093343ac28b1534a721dbb4"
        }
      },
      {
        "id": 2271,
        "name": "Verify",
        "symbol": "CRED",
        "slug": "verify",
        "is_active": 1,
        "first_historical_data": "2020-07-09T17:30:08.033Z",
        "last_historical_data": "2020-08-18T09:55:31.963Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x672a1ad4f667fb18a333af13667aa0af1f5b5bdd"
        }
      },
      {
        "id": 2273,
        "name": "Uquid Coin",
        "symbol": "UQC",
        "slug": "uquid-coin",
        "is_active": 1,
        "first_historical_data": "2020-01-12T23:33:01.569Z",
        "last_historical_data": "2020-08-18T03:13:59.233Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd01db73e047855efb414e6202098c4be4cd2423b"
        }
      },
      {
        "id": 2274,
        "name": "MediShares",
        "symbol": "MDS",
        "slug": "medishares",
        "is_active": 1,
        "first_historical_data": "2019-12-11T19:17:19.287Z",
        "last_historical_data": "2020-08-18T05:08:46.893Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x66186008c1050627f979d464eabb258860563dbe"
        }
      },
      {
        "id": 2275,
        "name": "ProChain",
        "symbol": "PRA",
        "slug": "prochain",
        "is_active": 1,
        "first_historical_data": "2020-04-14T10:41:29.640Z",
        "last_historical_data": "2020-08-17T20:59:22.665Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9041fe5b3fdea0f5e4afdc17e75180738d877a01"
        }
      },
      {
        "id": 2276,
        "name": "Ignis",
        "symbol": "IGNIS",
        "slug": "ignis",
        "is_active": 1,
        "first_historical_data": "2019-09-14T07:16:58.863Z",
        "last_historical_data": "2020-08-18T17:39:04.927Z",
        "platform": {
          "id": 1320,
          "name": "Ardor",
          "symbol": "ARDR",
          "slug": "ardor",
          "token_address": ""
        }
      },
      {
        "id": 2277,
        "name": "SmartMesh",
        "symbol": "SMT",
        "slug": "smartmesh",
        "is_active": 1,
        "first_historical_data": "2019-08-31T10:13:18.664Z",
        "last_historical_data": "2020-08-17T18:54:15.541Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x55f93985431fc9304077687a35a1ba103dc1e081"
        }
      },
      {
        "id": 2278,
        "name": "HollyWoodCoin",
        "symbol": "HWC",
        "slug": "hollywoodcoin",
        "is_active": 1,
        "first_historical_data": "2020-04-11T20:17:44.344Z",
        "last_historical_data": "2020-08-18T05:22:05.584Z",
        "platform": null
      },
      {
        "id": 2279,
        "name": "Playkey",
        "symbol": "PKT",
        "slug": "playkey",
        "is_active": 1,
        "first_historical_data": "2020-02-12T04:26:34.931Z",
        "last_historical_data": "2020-08-18T12:48:51.085Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2604fa406be957e542beb89e6754fcde6815e83f"
        }
      },
      {
        "id": 2280,
        "name": "Filecoin [Futures]",
        "symbol": "FIL",
        "slug": "filecoin",
        "is_active": 1,
        "first_historical_data": "2020-02-03T13:23:12.275Z",
        "last_historical_data": "2020-08-18T03:04:43.593Z",
        "platform": null
      },
      {
        "id": 2281,
        "name": "BitcoinX",
        "symbol": "BCX",
        "slug": "bitcoinx",
        "is_active": 1,
        "first_historical_data": "2020-02-22T13:54:37.531Z",
        "last_historical_data": "2020-08-17T19:36:51.842Z",
        "platform": null
      },
      {
        "id": 2282,
        "name": "Super Bitcoin",
        "symbol": "SBTC",
        "slug": "super-bitcoin",
        "is_active": 1,
        "first_historical_data": "2019-09-29T00:11:06.690Z",
        "last_historical_data": "2020-08-18T10:55:32.997Z",
        "platform": null
      },
      {
        "id": 2283,
        "name": "Datum",
        "symbol": "DAT",
        "slug": "datum",
        "is_active": 1,
        "first_historical_data": "2019-11-18T01:29:05.743Z",
        "last_historical_data": "2020-08-18T02:04:22.580Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x81c9151de0c8bafcd325a57e3db5a5df1cebf79c"
        }
      },
      {
        "id": 2286,
        "name": "MicroMoney",
        "symbol": "AMM",
        "slug": "micromoney",
        "is_active": 1,
        "first_historical_data": "2019-09-25T08:48:05.751Z",
        "last_historical_data": "2020-08-18T07:59:00.742Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8b1f49491477e0fb46a29fef53f1ea320d13c349"
        }
      },
      {
        "id": 2287,
        "name": "LockTrip",
        "symbol": "LOC",
        "slug": "lockchain",
        "is_active": 1,
        "first_historical_data": "2020-05-24T13:36:38.298Z",
        "last_historical_data": "2020-08-18T10:13:16.893Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5e3346444010135322268a4630d2ed5f8d09446c"
        }
      },
      {
        "id": 2288,
        "name": "Worldcore",
        "symbol": "WRC",
        "slug": "worldcore",
        "is_active": 1,
        "first_historical_data": "2019-10-24T07:32:59.684Z",
        "last_historical_data": "2020-08-18T08:36:46.105Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x72adadb447784dd7ab1f472467750fc485e4cb2d"
        }
      },
      {
        "id": 2289,
        "name": "Gifto",
        "symbol": "GTO",
        "slug": "gifto",
        "is_active": 1,
        "first_historical_data": "2019-10-02T14:27:12.798Z",
        "last_historical_data": "2020-08-18T09:44:48.692Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "GTO-908"
        }
      },
      {
        "id": 2290,
        "name": "YENTEN",
        "symbol": "YTN",
        "slug": "yenten",
        "is_active": 1,
        "first_historical_data": "2019-09-14T09:09:46.342Z",
        "last_historical_data": "2020-08-18T12:28:33.159Z",
        "platform": null
      },
      {
        "id": 2291,
        "name": "Genaro Network",
        "symbol": "GNX",
        "slug": "genaro-network",
        "is_active": 1,
        "first_historical_data": "2020-05-25T05:11:54.873Z",
        "last_historical_data": "2020-08-18T02:54:34.762Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6ec8a24cabdc339a06a172f8223ea557055adaa5"
        }
      },
      {
        "id": 2293,
        "name": "United Bitcoin",
        "symbol": "UBTC",
        "slug": "united-bitcoin",
        "is_active": 1,
        "first_historical_data": "2019-10-25T01:23:18.502Z",
        "last_historical_data": "2020-08-18T04:08:44.203Z",
        "platform": null
      },
      {
        "id": 2295,
        "name": "Starbase",
        "symbol": "STAR",
        "slug": "starbase",
        "is_active": 1,
        "first_historical_data": "2019-09-15T23:24:33.011Z",
        "last_historical_data": "2020-08-17T19:34:48.620Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf70a642bd387f94380ffb90451c2c81d4eb82cbc"
        }
      },
      {
        "id": 2296,
        "name": "OST",
        "symbol": "OST",
        "slug": "ost",
        "is_active": 1,
        "first_historical_data": "2020-02-11T03:37:47.304Z",
        "last_historical_data": "2020-08-18T15:19:56.980Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2c4e8f2d746113d0696ce89b35f0d8bf88e0aeca"
        }
      },
      {
        "id": 2297,
        "name": "StormX",
        "symbol": "STMX",
        "slug": "stormx",
        "is_active": 1,
        "first_historical_data": "2020-04-18T20:50:44.325Z",
        "last_historical_data": "2020-08-18T06:00:28.491Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xbE9375C6a420D2eEB258962efB95551A5b722803"
        }
      },
      {
        "id": 2298,
        "name": "Dynamic Trading Rights",
        "symbol": "DTR",
        "slug": "dynamic-trading-rights",
        "is_active": 1,
        "first_historical_data": "2020-07-25T06:02:50.696Z",
        "last_historical_data": "2020-08-18T04:09:37.689Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd234bf2410a0009df9c3c63b610c09738f18ccd7"
        }
      },
      {
        "id": 2299,
        "name": "aelf",
        "symbol": "ELF",
        "slug": "aelf",
        "is_active": 1,
        "first_historical_data": "2020-04-21T12:16:15.595Z",
        "last_historical_data": "2020-08-18T17:41:56.233Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xbf2179859fc6d5bee9bf9158632dc51678a4100e"
        }
      },
      {
        "id": 2300,
        "name": "WAX",
        "symbol": "WAXP",
        "slug": "wax",
        "is_active": 1,
        "first_historical_data": "2020-05-17T00:36:35.416Z",
        "last_historical_data": "2020-08-18T12:34:34.434Z",
        "platform": null
      },
      {
        "id": 2303,
        "name": "MediBloc",
        "symbol": "MED",
        "slug": "medibloc",
        "is_active": 1,
        "first_historical_data": "2020-06-21T03:38:24.193Z",
        "last_historical_data": "2020-08-18T11:36:22.222Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "MEDB-87E"
        }
      },
      {
        "id": 2305,
        "name": "NAGA",
        "symbol": "NGC",
        "slug": "naga",
        "is_active": 1,
        "first_historical_data": "2020-03-11T06:27:54.422Z",
        "last_historical_data": "2020-08-18T17:27:31.618Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x72dd4b6bd852a3aa172be4d6c5a6dbec588cf131"
        }
      },
      {
        "id": 2306,
        "name": "Bread",
        "symbol": "BRD",
        "slug": "bread",
        "is_active": 1,
        "first_historical_data": "2020-06-09T21:40:04.266Z",
        "last_historical_data": "2020-08-18T12:55:13.240Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x558ec3152e2eb2174905cd19aea4e34a23de9ad6"
        }
      },
      {
        "id": 2307,
        "name": "Bibox Token",
        "symbol": "BIX",
        "slug": "bibox-token",
        "is_active": 1,
        "first_historical_data": "2019-11-07T06:49:45.093Z",
        "last_historical_data": "2020-08-17T19:16:00.194Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb3104b4b9da82025e8b9f8fb28b3553ce2f67069"
        }
      },
      {
        "id": 2309,
        "name": "SophiaTX",
        "symbol": "SPHTX",
        "slug": "sophiatx",
        "is_active": 1,
        "first_historical_data": "2020-03-10T03:18:38.454Z",
        "last_historical_data": "2020-08-17T21:14:53.744Z",
        "platform": null
      },
      {
        "id": 2310,
        "name": "Bounty0x",
        "symbol": "BNTY",
        "slug": "bounty0x",
        "is_active": 1,
        "first_historical_data": "2020-04-05T23:39:04.306Z",
        "last_historical_data": "2020-08-18T02:45:06.449Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd2d6158683aee4cc838067727209a0aaf4359de3"
        }
      },
      {
        "id": 2311,
        "name": "ACE (TokenStars)",
        "symbol": "ACE",
        "slug": "ace",
        "is_active": 1,
        "first_historical_data": "2020-02-20T10:06:30.416Z",
        "last_historical_data": "2020-08-18T13:49:16.596Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x06147110022b768ba8f99a8f385df11a151a9cc8"
        }
      },
      {
        "id": 2312,
        "name": "DIMCOIN",
        "symbol": "DIM",
        "slug": "dimcoin",
        "is_active": 1,
        "first_historical_data": "2020-01-20T22:40:20.380Z",
        "last_historical_data": "2020-08-18T09:07:12.062Z",
        "platform": {
          "id": 873,
          "name": "NEM",
          "symbol": "XEM",
          "slug": "nem",
          "token_address": ""
        }
      },
      {
        "id": 2313,
        "name": "SIRIN LABS Token",
        "symbol": "SRN",
        "slug": "sirin-labs-token",
        "is_active": 1,
        "first_historical_data": "2020-04-16T15:27:42.250Z",
        "last_historical_data": "2020-08-18T06:05:41.936Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x68d57c9a1c35f63e2c83ee8e49a64e9d70528d25"
        }
      },
      {
        "id": 2314,
        "name": "Cryptopay",
        "symbol": "CPAY",
        "slug": "cryptopay",
        "is_active": 1,
        "first_historical_data": "2019-10-18T08:19:05.309Z",
        "last_historical_data": "2020-08-17T19:34:33.678Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0ebb614204e47c09b6c3feb9aaecad8ee060e23e"
        }
      },
      {
        "id": 2315,
        "name": "HTMLCOIN",
        "symbol": "HTML",
        "slug": "html-coin",
        "is_active": 1,
        "first_historical_data": "2020-06-08T22:02:32.712Z",
        "last_historical_data": "2020-08-18T03:32:44.145Z",
        "platform": null
      },
      {
        "id": 2316,
        "name": "DeepBrain Chain",
        "symbol": "DBC",
        "slug": "deepbrain-chain",
        "is_active": 1,
        "first_historical_data": "2020-01-29T18:38:49.463Z",
        "last_historical_data": "2020-08-17T23:17:39.355Z",
        "platform": {
          "id": 1376,
          "name": "Neo",
          "symbol": "NEO",
          "slug": "neo",
          "token_address": "b951ecbbc5fe37a9c280a76cb0ce0014827294cf"
        }
      },
      {
        "id": 2318,
        "name": "Neumark",
        "symbol": "NEU",
        "slug": "neumark",
        "is_active": 1,
        "first_historical_data": "2020-01-15T10:37:28.266Z",
        "last_historical_data": "2020-08-18T05:02:02.384Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa823e6722006afe99e91c30ff5295052fe6b8e32"
        }
      },
      {
        "id": 2319,
        "name": "QCash",
        "symbol": "QC",
        "slug": "qcash",
        "is_active": 1,
        "first_historical_data": "2020-03-16T21:36:57.510Z",
        "last_historical_data": "2020-08-18T08:06:47.620Z",
        "platform": {
          "id": 1684,
          "name": "Qtum",
          "symbol": "QTUM",
          "slug": "qtum",
          "token_address": "f2033ede578e17fa6231047265010445bca8cf1c"
        }
      },
      {
        "id": 2320,
        "name": "Utrust",
        "symbol": "UTK",
        "slug": "utrust",
        "is_active": 1,
        "first_historical_data": "2020-02-22T01:32:36.117Z",
        "last_historical_data": "2020-08-18T15:20:24.397Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x70a72833d6bf7f508c8224ce59ea1ef3d0ea3a38"
        }
      },
      {
        "id": 2321,
        "name": "QLC Chain",
        "symbol": "QLC",
        "slug": "qlink",
        "is_active": 1,
        "first_historical_data": "2019-12-01T00:49:45.300Z",
        "last_historical_data": "2020-08-18T16:52:29.037Z",
        "platform": {
          "id": 1376,
          "name": "Neo",
          "symbol": "NEO",
          "slug": "neo",
          "token_address": "0d821bd7b6d53f5c2b40e217c6defc8bbe896cf5"
        }
      },
      {
        "id": 2323,
        "name": "HEROcoin",
        "symbol": "PLAY",
        "slug": "herocoin",
        "is_active": 1,
        "first_historical_data": "2019-08-21T10:53:46.762Z",
        "last_historical_data": "2020-08-17T19:37:38.960Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe477292f1b3268687a29376116b0ed27a9c76170"
        }
      },
      {
        "id": 2324,
        "name": "BigONE Token",
        "symbol": "ONE",
        "slug": "bigone-token",
        "is_active": 1,
        "first_historical_data": "2020-03-19T13:56:39.657Z",
        "last_historical_data": "2020-08-18T11:48:05.224Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0396340f16bbec973280ab053efc3f208fa37795"
        }
      },
      {
        "id": 2325,
        "name": "Matryx",
        "symbol": "MTX",
        "slug": "matryx",
        "is_active": 1,
        "first_historical_data": "2020-07-18T12:15:31.554Z",
        "last_historical_data": "2020-08-18T08:59:27.662Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0af44e2784637218dd1d32a322d44e603a8f0c6a"
        }
      },
      {
        "id": 2329,
        "name": "Hyper Pay",
        "symbol": "HPY",
        "slug": "hyper-pay",
        "is_active": 1,
        "first_historical_data": "2019-12-31T20:13:55.090Z",
        "last_historical_data": "2020-08-18T02:28:33.341Z",
        "platform": {
          "id": 1684,
          "name": "Qtum",
          "symbol": "QTUM",
          "slug": "qtum",
          "token_address": "f2703e93f87b846a7aacec1247beaec1c583daa4"
        }
      },
      {
        "id": 2330,
        "name": "Pylon Network",
        "symbol": "PYLNT",
        "slug": "pylon-network",
        "is_active": 1,
        "first_historical_data": "2020-03-26T06:09:20.134Z",
        "last_historical_data": "2020-08-18T13:34:00.376Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x7703c35cffdc5cda8d27aa3df2f9ba6964544b6e"
        }
      },
      {
        "id": 2332,
        "name": "STRAKS",
        "symbol": "STAK",
        "slug": "straks",
        "is_active": 1,
        "first_historical_data": "2020-02-23T16:33:17.856Z",
        "last_historical_data": "2020-08-18T16:49:23.931Z",
        "platform": null
      },
      {
        "id": 2334,
        "name": "BitClave",
        "symbol": "CAT",
        "slug": "bitclave",
        "is_active": 1,
        "first_historical_data": "2019-09-02T10:07:27.140Z",
        "last_historical_data": "2020-08-18T08:48:12.888Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1234567461d3f8db7496581774bd869c83d51c93"
        }
      },
      {
        "id": 2335,
        "name": "Lightning Bitcoin",
        "symbol": "LBTC",
        "slug": "lightning-bitcoin",
        "is_active": 1,
        "first_historical_data": "2019-09-27T08:37:27.420Z",
        "last_historical_data": "2020-08-17T23:15:52.423Z",
        "platform": null
      },
      {
        "id": 2336,
        "name": "Game.com",
        "symbol": "GTC",
        "slug": "game",
        "is_active": 1,
        "first_historical_data": "2019-10-16T04:39:24.080Z",
        "last_historical_data": "2020-08-18T08:49:21.259Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb70835d7822ebb9426b56543e391846c107bd32c"
        }
      },
      {
        "id": 2337,
        "name": "Lamden",
        "symbol": "TAU",
        "slug": "lamden",
        "is_active": 1,
        "first_historical_data": "2020-02-28T15:33:16.952Z",
        "last_historical_data": "2020-08-18T02:54:26.962Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc27a2f05fa577a83ba0fdb4c38443c0718356501"
        }
      },
      {
        "id": 2340,
        "name": "Bloom",
        "symbol": "BLT",
        "slug": "bloomtoken",
        "is_active": 1,
        "first_historical_data": "2020-05-15T11:46:12.946Z",
        "last_historical_data": "2020-08-18T13:35:31.849Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x107c4504cd79c5d2696ea0030a8dd4e92601b82e"
        }
      },
      {
        "id": 2341,
        "name": "SwftCoin",
        "symbol": "SWFTC",
        "slug": "swftcoin",
        "is_active": 1,
        "first_historical_data": "2019-12-09T18:46:59.836Z",
        "last_historical_data": "2020-08-18T03:09:20.864Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0bb217e40f8a5cb79adf04e1aab60e5abd0dfc1e"
        }
      },
      {
        "id": 2342,
        "name": "Covesting",
        "symbol": "COV",
        "slug": "covesting",
        "is_active": 1,
        "first_historical_data": "2020-08-14T01:37:25.593Z",
        "last_historical_data": "2020-08-18T03:06:36.266Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe2fb6529ef566a080e6d23de0bd351311087d567"
        }
      },
      {
        "id": 2343,
        "name": "CanYaCoin",
        "symbol": "CAN",
        "slug": "canyacoin",
        "is_active": 1,
        "first_historical_data": "2019-12-08T22:39:39.868Z",
        "last_historical_data": "2020-08-17T20:27:05.287Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "CAN-677"
        }
      },
      {
        "id": 2344,
        "name": "AppCoins",
        "symbol": "APPC",
        "slug": "appcoins",
        "is_active": 1,
        "first_historical_data": "2020-01-10T17:10:33.432Z",
        "last_historical_data": "2020-08-17T19:07:56.339Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1a7a8bd9106f2b8d977e08582dc7d24c723ab0db"
        }
      },
      {
        "id": 2345,
        "name": "High Performance Blockchain",
        "symbol": "HPB",
        "slug": "high-performance-blockchain",
        "is_active": 1,
        "first_historical_data": "2019-08-20T11:19:03.498Z",
        "last_historical_data": "2020-08-17T23:46:12.854Z",
        "platform": null
      },
      {
        "id": 2346,
        "name": "WaykiChain",
        "symbol": "WICC",
        "slug": "waykichain",
        "is_active": 1,
        "first_historical_data": "2019-09-24T17:17:20.521Z",
        "last_historical_data": "2020-08-18T03:35:01.258Z",
        "platform": null
      },
      {
        "id": 2348,
        "name": "Measurable Data Token",
        "symbol": "MDT",
        "slug": "measurable-data-token",
        "is_active": 1,
        "first_historical_data": "2020-02-13T18:45:33.994Z",
        "last_historical_data": "2020-08-18T09:38:48.436Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x814e0908b12a99fecf5bc101bb5d0b8b5cdf7d26"
        }
      },
      {
        "id": 2349,
        "name": "Mixin",
        "symbol": "XIN",
        "slug": "mixin",
        "is_active": 1,
        "first_historical_data": "2019-10-28T11:05:06.608Z",
        "last_historical_data": "2020-08-17T18:08:33.676Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa974c709cfb4566686553a20790685a47aceaa33"
        }
      },
      {
        "id": 2352,
        "name": "Coinlancer",
        "symbol": "CL",
        "slug": "coinlancer",
        "is_active": 1,
        "first_historical_data": "2019-09-15T13:20:19.348Z",
        "last_historical_data": "2020-08-18T05:08:07.586Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe81d72d14b1516e68ac3190a46c93302cc8ed60f"
        }
      },
      {
        "id": 2354,
        "name": "GET Protocol",
        "symbol": "GET",
        "slug": "get-protocol",
        "is_active": 1,
        "first_historical_data": "2020-06-07T21:52:36.461Z",
        "last_historical_data": "2020-08-18T00:34:31.674Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8a854288a5976036a725879164ca3e91d30c6a1b"
        }
      },
      {
        "id": 2356,
        "name": "CFun",
        "symbol": "CFUN",
        "slug": "cfun",
        "is_active": 1,
        "first_historical_data": "2019-12-29T10:56:29.929Z",
        "last_historical_data": "2020-08-18T10:02:53.456Z",
        "platform": {
          "id": 1684,
          "name": "Qtum",
          "symbol": "QTUM",
          "slug": "qtum",
          "token_address": "8b9500e2b789e002c1d0e744bd0ac7aa60dbffcc"
        }
      },
      {
        "id": 2357,
        "name": "AI Doctor",
        "symbol": "AIDOC",
        "slug": "aidoc",
        "is_active": 1,
        "first_historical_data": "2020-06-09T12:16:49.468Z",
        "last_historical_data": "2020-08-18T17:29:38.133Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x584b44853680ee34a0f337b712a8f66d816df151"
        }
      },
      {
        "id": 2358,
        "name": "Content and AD Network",
        "symbol": "CAN",
        "slug": "content-and-ad-network",
        "is_active": 1,
        "first_historical_data": "2020-03-31T01:09:35.246Z",
        "last_historical_data": "2020-08-18T08:31:30.607Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5f3789907b35dce5605b00c0be0a7ecdbfa8a841"
        }
      },
      {
        "id": 2359,
        "name": "Polis",
        "symbol": "POLIS",
        "slug": "polis",
        "is_active": 1,
        "first_historical_data": "2019-11-14T17:28:17.656Z",
        "last_historical_data": "2020-08-18T01:16:49.708Z",
        "platform": null
      },
      {
        "id": 2363,
        "name": "Zap",
        "symbol": "ZAP",
        "slug": "zap",
        "is_active": 1,
        "first_historical_data": "2020-05-14T20:37:10.716Z",
        "last_historical_data": "2020-08-18T09:03:15.586Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6781a0f84c7e9e846dcb84a9a5bd49333067b104"
        }
      },
      {
        "id": 2364,
        "name": "TokenClub",
        "symbol": "TCT",
        "slug": "tokenclub",
        "is_active": 1,
        "first_historical_data": "2020-03-22T20:53:00.102Z",
        "last_historical_data": "2020-08-17T22:34:03.438Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4824a7b64e3966b0133f4f4ffb1b9d6beb75fff7"
        }
      },
      {
        "id": 2366,
        "name": "FairGame",
        "symbol": "FAIR",
        "slug": "fairgame",
        "is_active": 1,
        "first_historical_data": "2020-06-26T22:44:44.561Z",
        "last_historical_data": "2020-08-18T06:51:42.720Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9b20dabcec77f6289113e61893f7beefaeb1990a"
        }
      },
      {
        "id": 2367,
        "name": "Aigang",
        "symbol": "AIX",
        "slug": "aigang",
        "is_active": 1,
        "first_historical_data": "2019-09-14T03:24:33.738Z",
        "last_historical_data": "2020-08-18T16:32:32.620Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1063ce524265d5a3a624f4914acd573dd89ce988"
        }
      },
      {
        "id": 2369,
        "name": "Insolar",
        "symbol": "XNS",
        "slug": "insolar",
        "is_active": 1,
        "first_historical_data": "2020-01-06T05:31:30.435Z",
        "last_historical_data": "2020-08-18T05:10:54.458Z",
        "platform": null
      },
      {
        "id": 2370,
        "name": "Bitcoin God",
        "symbol": "GOD",
        "slug": "bitcoin-god",
        "is_active": 1,
        "first_historical_data": "2019-11-07T00:54:37.589Z",
        "last_historical_data": "2020-08-18T15:27:48.106Z",
        "platform": null
      },
      {
        "id": 2371,
        "name": "United Traders Token",
        "symbol": "UTT",
        "slug": "uttoken",
        "is_active": 1,
        "first_historical_data": "2020-03-16T13:31:23.717Z",
        "last_historical_data": "2020-08-18T16:51:18.018Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x16f812be7fff02caf662b85d5d58a5da6572d4df"
        }
      },
      {
        "id": 2372,
        "name": "CDX Network",
        "symbol": "CDX",
        "slug": "cdx-network",
        "is_active": 1,
        "first_historical_data": "2019-10-26T17:39:40.802Z",
        "last_historical_data": "2020-08-18T10:06:06.552Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6fff3806bbac52a20e0d79bc538d527f6a22c96b"
        }
      },
      {
        "id": 2374,
        "name": "BitDegree",
        "symbol": "BDG",
        "slug": "bitdegree",
        "is_active": 1,
        "first_historical_data": "2020-02-09T20:29:36.065Z",
        "last_historical_data": "2020-08-18T16:43:38.540Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1961b3331969ed52770751fc718ef530838b6dee"
        }
      },
      {
        "id": 2375,
        "name": "QunQun",
        "symbol": "QUN",
        "slug": "qunqun",
        "is_active": 1,
        "first_historical_data": "2020-03-02T17:26:32.288Z",
        "last_historical_data": "2020-08-18T12:50:44.470Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x264dc2dedcdcbb897561a57cba5085ca416fb7b4"
        }
      },
      {
        "id": 2376,
        "name": "TopChain",
        "symbol": "TOPC",
        "slug": "topchain",
        "is_active": 1,
        "first_historical_data": "2019-09-27T20:11:40.713Z",
        "last_historical_data": "2020-08-18T05:23:32.754Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1b6c5864375b34af3ff5bd2e5f40bc425b4a8d79"
        }
      },
      {
        "id": 2377,
        "name": "Leverj",
        "symbol": "LEV",
        "slug": "leverj",
        "is_active": 1,
        "first_historical_data": "2020-03-06T05:58:06.068Z",
        "last_historical_data": "2020-08-17T21:50:57.527Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0f4ca92660efad97a9a70cb0fe969c755439772c"
        }
      },
      {
        "id": 2379,
        "name": "Kcash",
        "symbol": "KCASH",
        "slug": "kcash",
        "is_active": 1,
        "first_historical_data": "2020-06-12T06:49:48.860Z",
        "last_historical_data": "2020-08-18T12:16:38.615Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x32d74896f05204d1b6ae7b0a3cebd7fc0cd8f9c7"
        }
      },
      {
        "id": 2380,
        "name": "ATN",
        "symbol": "ATN",
        "slug": "atn",
        "is_active": 1,
        "first_historical_data": "2020-08-01T13:38:17.570Z",
        "last_historical_data": "2020-08-17T21:33:41.632Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x461733c17b0755ca5649b6db08b3e213fcf22546"
        }
      },
      {
        "id": 2381,
        "name": "Spectre.ai Dividend Token",
        "symbol": "SXDT",
        "slug": "spectre-dividend",
        "is_active": 1,
        "first_historical_data": "2020-01-18T23:16:34.363Z",
        "last_historical_data": "2020-08-18T17:29:06.036Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x12b306fa98f4cbb8d4457fdff3a0a0a56f07ccdf"
        }
      },
      {
        "id": 2382,
        "name": "Spectre.ai Utility Token",
        "symbol": "SXUT",
        "slug": "spectre-utility",
        "is_active": 1,
        "first_historical_data": "2019-10-29T12:28:50.907Z",
        "last_historical_data": "2020-08-18T00:14:22.931Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2c82c73d5b34aa015989462b2948cd616a37641f"
        }
      },
      {
        "id": 2383,
        "name": "Jingtum Tech",
        "symbol": "SWTC",
        "slug": "jingtum-tech",
        "is_active": 1,
        "first_historical_data": "2019-10-30T23:00:53.906Z",
        "last_historical_data": "2020-08-18T07:19:58.061Z",
        "platform": null
      },
      {
        "id": 2386,
        "name": "KZ Cash",
        "symbol": "KZC",
        "slug": "kz-cash",
        "is_active": 1,
        "first_historical_data": "2020-03-12T00:14:25.161Z",
        "last_historical_data": "2020-08-17T20:40:57.707Z",
        "platform": null
      },
      {
        "id": 2387,
        "name": "Bitcoin Atom",
        "symbol": "BCA",
        "slug": "bitcoin-atom",
        "is_active": 1,
        "first_historical_data": "2020-05-02T15:17:54.818Z",
        "last_historical_data": "2020-08-18T12:04:53.310Z",
        "platform": null
      },
      {
        "id": 2391,
        "name": "EchoLink",
        "symbol": "EKO",
        "slug": "echolink",
        "is_active": 1,
        "first_historical_data": "2020-02-16T18:03:22.944Z",
        "last_historical_data": "2020-08-18T10:11:09.919Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa6a840e50bcaa50da017b91a0d86b8b2d41156ee"
        }
      },
      {
        "id": 2392,
        "name": "Bottos",
        "symbol": "BTO",
        "slug": "bottos",
        "is_active": 1,
        "first_historical_data": "2019-12-29T05:34:53.071Z",
        "last_historical_data": "2020-08-18T09:40:33.712Z",
        "platform": null
      },
      {
        "id": 2394,
        "name": "Telcoin",
        "symbol": "TEL",
        "slug": "telcoin",
        "is_active": 1,
        "first_historical_data": "2020-06-03T17:03:43.427Z",
        "last_historical_data": "2020-08-18T08:34:42.398Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x85e076361cc813a908ff672f9bad1541474402b2"
        }
      },
      {
        "id": 2395,
        "name": "Ignition",
        "symbol": "IC",
        "slug": "ignition",
        "is_active": 1,
        "first_historical_data": "2019-11-21T22:28:38.886Z",
        "last_historical_data": "2020-08-18T15:19:00.720Z",
        "platform": null
      },
      {
        "id": 2396,
        "name": "WETH",
        "symbol": "WETH",
        "slug": "weth",
        "is_active": 1,
        "first_historical_data": "2019-09-08T18:15:09.699Z",
        "last_historical_data": "2020-08-18T13:48:05.552Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2"
        }
      },
      {
        "id": 2398,
        "name": "Selfkey",
        "symbol": "KEY",
        "slug": "selfkey",
        "is_active": 1,
        "first_historical_data": "2020-02-21T17:18:46.642Z",
        "last_historical_data": "2020-08-18T04:46:47.250Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4cc19356f2d37338b9802aa8e8fc58b0373296e7"
        }
      },
      {
        "id": 2399,
        "name": "INT Chain",
        "symbol": "INT",
        "slug": "int-chain",
        "is_active": 1,
        "first_historical_data": "2020-01-05T09:11:29.097Z",
        "last_historical_data": "2020-08-18T06:54:22.734Z",
        "platform": null
      },
      {
        "id": 2400,
        "name": "OneRoot Network",
        "symbol": "RNT",
        "slug": "oneroot-network",
        "is_active": 1,
        "first_historical_data": "2020-02-21T20:16:02.667Z",
        "last_historical_data": "2020-08-18T00:25:04.097Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xff603f43946a3a28df5e6a73172555d8c8b02386"
        }
      },
      {
        "id": 2402,
        "name": "Sense",
        "symbol": "SENSE",
        "slug": "sense",
        "is_active": 1,
        "first_historical_data": "2019-09-19T17:10:46.795Z",
        "last_historical_data": "2020-08-18T17:29:31.836Z",
        "platform": {
          "id": 1765,
          "name": "EOS",
          "symbol": "EOS",
          "slug": "eos",
          "token_address": ""
        }
      },
      {
        "id": 2403,
        "name": "MOAC",
        "symbol": "MOAC",
        "slug": "moac",
        "is_active": 1,
        "first_historical_data": "2020-08-07T14:48:51.882Z",
        "last_historical_data": "2020-08-18T09:23:59.684Z",
        "platform": null
      },
      {
        "id": 2404,
        "name": "TOKYO",
        "symbol": "TOKC",
        "slug": "tokyo",
        "is_active": 1,
        "first_historical_data": "2019-09-01T01:22:43.526Z",
        "last_historical_data": "2020-08-17T18:56:23.527Z",
        "platform": null
      },
      {
        "id": 2405,
        "name": "IOST",
        "symbol": "IOST",
        "slug": "iostoken",
        "is_active": 1,
        "first_historical_data": "2019-12-29T20:38:45.130Z",
        "last_historical_data": "2020-08-18T13:56:32.199Z",
        "platform": null
      },
      {
        "id": 2406,
        "name": "InvestDigital",
        "symbol": "IDT",
        "slug": "investdigital",
        "is_active": 1,
        "first_historical_data": "2020-06-04T03:58:01.140Z",
        "last_historical_data": "2020-08-17T21:52:23.491Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x02c4c78c462e32cca4a90bc499bf411fb7bc6afb"
        }
      },
      {
        "id": 2407,
        "name": "AICHAIN",
        "symbol": "AIT",
        "slug": "aichain",
        "is_active": 1,
        "first_historical_data": "2020-08-08T18:43:08.387Z",
        "last_historical_data": "2020-08-18T03:32:53.342Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x79650799e7899a802cb96c0bc33a6a8d4ce4936c"
        }
      },
      {
        "id": 2408,
        "name": "Qube",
        "symbol": "QUBE",
        "slug": "qube",
        "is_active": 1,
        "first_historical_data": "2019-08-25T07:11:26.712Z",
        "last_historical_data": "2020-08-18T05:31:34.310Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x57838ff342f36a1ec18224981ea8715a4667fb3a"
        }
      },
      {
        "id": 2410,
        "name": "SpaceChain",
        "symbol": "SPC",
        "slug": "spacechain",
        "is_active": 1,
        "first_historical_data": "2020-05-09T13:25:39.893Z",
        "last_historical_data": "2020-08-18T10:45:12.685Z",
        "platform": {
          "id": 1684,
          "name": "Qtum",
          "symbol": "QTUM",
          "slug": "qtum",
          "token_address": "0x8069080a922834460c3a092fb2c1510224dc066b"
        }
      },
      {
        "id": 2411,
        "name": "Galactrum",
        "symbol": "ORE",
        "slug": "galactrum",
        "is_active": 1,
        "first_historical_data": "2020-04-13T23:45:44.722Z",
        "last_historical_data": "2020-08-18T09:42:30.822Z",
        "platform": null
      },
      {
        "id": 2413,
        "name": "Ethouse",
        "symbol": "HORSE",
        "slug": "ethouse",
        "is_active": 1,
        "first_historical_data": "2019-10-26T14:51:55.183Z",
        "last_historical_data": "2020-08-18T06:08:28.941Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5b0751713b2527d7f002c0c4e2a37e1219610a6b"
        }
      },
      {
        "id": 2414,
        "name": "RealChain",
        "symbol": "RCT",
        "slug": "realchain",
        "is_active": 1,
        "first_historical_data": "2019-12-28T15:14:23.474Z",
        "last_historical_data": "2020-08-18T11:51:30.846Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x13f25cd52b21650caa8225c9942337d914c9b030"
        }
      },
      {
        "id": 2415,
        "name": "ArbitrageCT",
        "symbol": "ARCT",
        "slug": "arbitragect",
        "is_active": 1,
        "first_historical_data": "2019-10-07T02:22:24.368Z",
        "last_historical_data": "2020-08-18T12:47:50.132Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1245ef80f4d9e02ed9425375e8f649b9221b31d8"
        }
      },
      {
        "id": 2416,
        "name": "THETA",
        "symbol": "THETA",
        "slug": "theta",
        "is_active": 1,
        "first_historical_data": "2020-02-23T07:57:25.997Z",
        "last_historical_data": "2020-08-18T17:22:01.395Z",
        "platform": null
      },
      {
        "id": 2418,
        "name": "Maverick Chain",
        "symbol": "MVC",
        "slug": "maverick-chain",
        "is_active": 1,
        "first_historical_data": "2020-03-13T16:30:31.180Z",
        "last_historical_data": "2020-08-18T11:03:55.892Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb17df9a3b09583a9bdcf757d6367171476d4d8a3"
        }
      },
      {
        "id": 2419,
        "name": "Profile Utility Token",
        "symbol": "PUT",
        "slug": "profile-utility-token",
        "is_active": 1,
        "first_historical_data": "2019-08-24T21:14:53.242Z",
        "last_historical_data": "2020-08-18T03:29:35.149Z",
        "platform": {
          "id": 1684,
          "name": "Qtum",
          "symbol": "QTUM",
          "slug": "qtum",
          "token_address": "4060e21ac01b5c5d2a3f01cecd7cbf820f50be95"
        }
      },
      {
        "id": 2421,
        "name": "VouchForMe",
        "symbol": "IPL",
        "slug": "insurepal",
        "is_active": 1,
        "first_historical_data": "2019-12-22T01:23:58.143Z",
        "last_historical_data": "2020-08-18T07:17:51.293Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x64cdf819d3e75ac8ec217b3496d7ce167be42e80"
        }
      },
      {
        "id": 2422,
        "name": "IDEX Membership",
        "symbol": "IDXM",
        "slug": "idex-membership",
        "is_active": 1,
        "first_historical_data": "2019-12-29T00:57:56.270Z",
        "last_historical_data": "2020-08-18T02:29:23.951Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xcc13fc627effd6e35d2d2706ea3c4d7396c610ea"
        }
      },
      {
        "id": 2424,
        "name": "SingularityNET",
        "symbol": "AGI",
        "slug": "singularitynet",
        "is_active": 1,
        "first_historical_data": "2019-12-06T20:01:14.796Z",
        "last_historical_data": "2020-08-18T14:44:53.292Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8eb24319393716668d768dcec29356ae9cffe285"
        }
      },
      {
        "id": 2426,
        "name": "ShareX",
        "symbol": "SEXC",
        "slug": "sharex",
        "is_active": 1,
        "first_historical_data": "2019-12-12T09:32:55.044Z",
        "last_historical_data": "2020-08-17T21:53:08.846Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2567c677473d110d75a8360c35309e63b1d52429"
        }
      },
      {
        "id": 2427,
        "name": "ChatCoin",
        "symbol": "CHAT",
        "slug": "chatcoin",
        "is_active": 1,
        "first_historical_data": "2020-05-20T15:08:00.497Z",
        "last_historical_data": "2020-08-17T21:34:22.272Z",
        "platform": null
      },
      {
        "id": 2428,
        "name": "Scry.info",
        "symbol": "DDD",
        "slug": "scryinfo",
        "is_active": 1,
        "first_historical_data": "2020-02-15T16:45:34.355Z",
        "last_historical_data": "2020-08-17T21:46:48.285Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9f5f3cfd7a32700c93f971637407ff17b91c7342"
        }
      },
      {
        "id": 2429,
        "name": "Mobius",
        "symbol": "MOBI",
        "slug": "mobius",
        "is_active": 1,
        "first_historical_data": "2020-06-02T01:14:05.681Z",
        "last_historical_data": "2020-08-18T09:28:15.001Z",
        "platform": {
          "id": 512,
          "name": "Stellar",
          "symbol": "XLM",
          "slug": "stellar",
          "token_address": "GA6HCMBLTZS5VYYBCATRBRZ3BZJMAFUDKYYF6AH6MVCMGWMRDNSWJPIH"
        }
      },
      {
        "id": 2430,
        "name": "Hydro Protocol",
        "symbol": "HOT",
        "slug": "hydro-protocol",
        "is_active": 1,
        "first_historical_data": "2020-03-08T07:11:38.248Z",
        "last_historical_data": "2020-08-18T00:00:48.328Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9af839687f6c94542ac5ece2e317daae355493a1"
        }
      },
      {
        "id": 2432,
        "name": "StarChain",
        "symbol": "STC",
        "slug": "starchain",
        "is_active": 1,
        "first_historical_data": "2020-06-07T13:01:33.031Z",
        "last_historical_data": "2020-08-18T14:41:06.244Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8f136cc8bef1fea4a7b71aa2301ff1a52f084384"
        }
      },
      {
        "id": 2434,
        "name": "Maggie",
        "symbol": "MAG",
        "slug": "maggie",
        "is_active": 1,
        "first_historical_data": "2020-01-04T02:01:38.526Z",
        "last_historical_data": "2020-08-18T01:04:52.365Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x647f274b3a7248d6cf51b35f08e7e7fd6edfb271"
        }
      },
      {
        "id": 2436,
        "name": "RefToken",
        "symbol": "REF",
        "slug": "reftoken",
        "is_active": 1,
        "first_historical_data": "2019-11-26T15:28:37.713Z",
        "last_historical_data": "2020-08-18T11:16:28.001Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x89303500a7abfb178b274fd89f2469c264951e1f"
        }
      },
      {
        "id": 2437,
        "name": "YEE",
        "symbol": "YEE",
        "slug": "yee",
        "is_active": 1,
        "first_historical_data": "2020-04-26T00:50:47.038Z",
        "last_historical_data": "2020-08-17T18:12:02.899Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x922105fad8153f516bcfb829f56dc097a0e1d705"
        }
      },
      {
        "id": 2438,
        "name": "Acute Angle Cloud",
        "symbol": "AAC",
        "slug": "acute-angle-cloud",
        "is_active": 1,
        "first_historical_data": "2020-08-14T16:45:57.316Z",
        "last_historical_data": "2020-08-18T16:14:19.121Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe75ad3aab14e4b0df8c5da4286608dabb21bd864"
        }
      },
      {
        "id": 2439,
        "name": "SelfSell",
        "symbol": "SSC",
        "slug": "selfsell",
        "is_active": 1,
        "first_historical_data": "2020-07-14T02:01:08.263Z",
        "last_historical_data": "2020-08-18T12:26:01.719Z",
        "platform": null
      },
      {
        "id": 2440,
        "name": "Read",
        "symbol": "READ",
        "slug": "read",
        "is_active": 1,
        "first_historical_data": "2019-09-04T15:04:47.415Z",
        "last_historical_data": "2020-08-18T07:11:37.752Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x13d0bf45e5f319fa0b58900807049f23cae7c40d"
        }
      },
      {
        "id": 2441,
        "name": "Molecular Future",
        "symbol": "MOF",
        "slug": "molecular-future",
        "is_active": 1,
        "first_historical_data": "2020-03-04T12:25:49.813Z",
        "last_historical_data": "2020-08-17T20:06:59.880Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x653430560be843c4a3d143d0110e896c2ab8ac0d"
        }
      },
      {
        "id": 2443,
        "name": "Trinity Network Credit",
        "symbol": "TNC",
        "slug": "trinity-network-credit",
        "is_active": 1,
        "first_historical_data": "2019-11-17T07:03:05.171Z",
        "last_historical_data": "2020-08-17T18:21:41.023Z",
        "platform": {
          "id": 1376,
          "name": "Neo",
          "symbol": "NEO",
          "slug": "neo",
          "token_address": "08e8c4400f1af2c20c28e0018f29535eb85d15b6"
        }
      },
      {
        "id": 2444,
        "name": "CRYPTO20",
        "symbol": "C20",
        "slug": "c20",
        "is_active": 1,
        "first_historical_data": "2020-06-23T10:21:09.016Z",
        "last_historical_data": "2020-08-18T14:30:02.123Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x26e75307fc0c021472feb8f727839531f112f317"
        }
      },
      {
        "id": 2446,
        "name": "DATA",
        "symbol": "DTA",
        "slug": "data",
        "is_active": 1,
        "first_historical_data": "2020-06-02T21:46:07.992Z",
        "last_historical_data": "2020-08-18T12:20:25.989Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x69b148395ce0015c13e36bffbad63f49ef874e03"
        }
      },
      {
        "id": 2447,
        "name": "Crypterium",
        "symbol": "CRPT",
        "slug": "crpt",
        "is_active": 1,
        "first_historical_data": "2020-04-30T06:15:45.353Z",
        "last_historical_data": "2020-08-18T10:32:32.162Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x80a7e048f37a50500351c204cb407766fa3bae7f"
        }
      },
      {
        "id": 2448,
        "name": "SparksPay",
        "symbol": "SPK",
        "slug": "sparkspay",
        "is_active": 1,
        "first_historical_data": "2019-12-19T05:50:01.495Z",
        "last_historical_data": "2020-08-18T04:46:34.598Z",
        "platform": null
      },
      {
        "id": 2450,
        "name": "carVertical",
        "symbol": "CV",
        "slug": "carvertical",
        "is_active": 1,
        "first_historical_data": "2020-05-09T06:01:18.407Z",
        "last_historical_data": "2020-08-18T17:18:28.420Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xda6cb58a0d0c01610a29c5a65c303e13e885887c"
        }
      },
      {
        "id": 2452,
        "name": "Tokenbox",
        "symbol": "TBX",
        "slug": "tokenbox",
        "is_active": 1,
        "first_historical_data": "2019-08-22T19:20:08.959Z",
        "last_historical_data": "2020-08-17T23:11:45.143Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x3a92bd396aef82af98ebc0aa9030d25a23b11c6b"
        }
      },
      {
        "id": 2453,
        "name": "EDUCare",
        "symbol": "EKT",
        "slug": "educare",
        "is_active": 1,
        "first_historical_data": "2020-07-21T10:19:37.398Z",
        "last_historical_data": "2020-08-18T12:19:08.903Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4ecdb6385f3db3847f9c4a9bf3f9917bb27a5452"
        }
      },
      {
        "id": 2454,
        "name": "UnlimitedIP",
        "symbol": "UIP",
        "slug": "unlimitedip",
        "is_active": 1,
        "first_historical_data": "2020-02-03T13:40:17.054Z",
        "last_historical_data": "2020-08-18T05:22:37.523Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4290563c2d7c255b5eec87f2d3bd10389f991d68"
        }
      },
      {
        "id": 2455,
        "name": "PressOne",
        "symbol": "PRS",
        "slug": "pressone",
        "is_active": 1,
        "first_historical_data": "2020-03-01T08:04:06.309Z",
        "last_historical_data": "2020-08-18T10:10:38.205Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe0d95530820aafc51b1d98023aa1ff000b78d8b2"
        }
      },
      {
        "id": 2457,
        "name": "TrueChain",
        "symbol": "TRUE",
        "slug": "truechain",
        "is_active": 1,
        "first_historical_data": "2020-04-07T06:31:33.727Z",
        "last_historical_data": "2020-08-17T19:47:38.627Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "TRUE-D84"
        }
      },
      {
        "id": 2458,
        "name": "Odyssey",
        "symbol": "OCN",
        "slug": "odyssey",
        "is_active": 1,
        "first_historical_data": "2019-09-15T22:03:51.808Z",
        "last_historical_data": "2020-08-18T01:38:16.723Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4092678e4e78230f46a1534c0fbc8fa39780892b"
        }
      },
      {
        "id": 2459,
        "name": "indaHash",
        "symbol": "IDH",
        "slug": "indahash",
        "is_active": 1,
        "first_historical_data": "2020-01-24T17:32:06.772Z",
        "last_historical_data": "2020-08-18T06:23:31.080Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5136c98a80811c3f46bdda8b5c4555cfd9f812f0"
        }
      },
      {
        "id": 2460,
        "name": "Qbic",
        "symbol": "QBIC",
        "slug": "qbic",
        "is_active": 1,
        "first_historical_data": "2020-02-10T03:11:42.815Z",
        "last_historical_data": "2020-08-17T21:14:37.855Z",
        "platform": null
      },
      {
        "id": 2461,
        "name": "Peerguess",
        "symbol": "GUESS",
        "slug": "guess",
        "is_active": 1,
        "first_historical_data": "2020-03-24T16:26:16.088Z",
        "last_historical_data": "2020-08-18T01:33:58.453Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xbdcfbf5c4d91abc0bc9709c7286d00063c0e6f22"
        }
      },
      {
        "id": 2462,
        "name": "AidCoin",
        "symbol": "AID",
        "slug": "aidcoin",
        "is_active": 1,
        "first_historical_data": "2020-07-12T10:07:03.704Z",
        "last_historical_data": "2020-08-18T08:58:48.346Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x37e8789bb9996cac9156cd5f5fd32599e6b91289"
        }
      },
      {
        "id": 2464,
        "name": "Devery",
        "symbol": "EVE",
        "slug": "devery",
        "is_active": 1,
        "first_historical_data": "2020-04-08T14:06:48.192Z",
        "last_historical_data": "2020-08-18T08:05:27.894Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x923108a439c4e8c2315c4f6521e5ce95b44e9b4c"
        }
      },
      {
        "id": 2465,
        "name": "Blockport",
        "symbol": "BPT",
        "slug": "blockport",
        "is_active": 1,
        "first_historical_data": "2020-08-17T07:21:37.248Z",
        "last_historical_data": "2020-08-18T11:56:11.944Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x327682779bab2bf4d1337e8974ab9de8275a7ca8"
        }
      },
      {
        "id": 2466,
        "name": "AXPR",
        "symbol": "AXPR",
        "slug": "paybx",
        "is_active": 1,
        "first_historical_data": "2020-04-01T21:21:08.738Z",
        "last_historical_data": "2020-08-18T03:08:09.031Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc39e626a04c5971d770e319760d7926502975e47"
        }
      },
      {
        "id": 2467,
        "name": "OriginTrail",
        "symbol": "TRAC",
        "slug": "origintrail",
        "is_active": 1,
        "first_historical_data": "2020-02-18T06:34:46.998Z",
        "last_historical_data": "2020-08-18T15:39:58.930Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xaa7a9ca87d3694b5755f213b5d04094b8d0f0a6f"
        }
      },
      {
        "id": 2468,
        "name": "LinkEye",
        "symbol": "LET",
        "slug": "linkeye",
        "is_active": 1,
        "first_historical_data": "2020-08-09T16:30:54.656Z",
        "last_historical_data": "2020-08-18T07:18:40.362Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xfa3118b34522580c35ae27f6cf52da1dbb756288"
        }
      },
      {
        "id": 2469,
        "name": "Zilliqa",
        "symbol": "ZIL",
        "slug": "zilliqa",
        "is_active": 1,
        "first_historical_data": "2020-01-13T02:18:35.062Z",
        "last_historical_data": "2020-08-17T20:37:28.952Z",
        "platform": null
      },
      {
        "id": 2470,
        "name": "CoinMeet",
        "symbol": "MEET",
        "slug": "coinmeet",
        "is_active": 1,
        "first_historical_data": "2020-01-14T10:08:35.398Z",
        "last_historical_data": "2020-08-17T23:16:22.053Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x7f121d4EC6c2C07eB6BC7989d91d2d4fF654c068"
        }
      },
      {
        "id": 2471,
        "name": "Smartlands Network",
        "symbol": "SLT",
        "slug": "smartlands-network",
        "is_active": 1,
        "first_historical_data": "2020-03-20T05:31:39.559Z",
        "last_historical_data": "2020-08-17T21:53:50.905Z",
        "platform": {
          "id": 512,
          "name": "Stellar",
          "symbol": "XLM",
          "slug": "stellar",
          "token_address": "GCKA6K5PCQ6PNF5RQBF7PQDJWRHO6UOGFMRLK3DYHDOI244V47XKQ4GP"
        }
      },
      {
        "id": 2472,
        "name": "Fortuna",
        "symbol": "FOTA",
        "slug": "fortuna",
        "is_active": 1,
        "first_historical_data": "2019-09-19T02:03:14.512Z",
        "last_historical_data": "2020-08-18T17:35:39.534Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4270bb238f6dd8b1c3ca01f96ca65b2647c06d3c"
        }
      },
      {
        "id": 2473,
        "name": "All Sports",
        "symbol": "SOC",
        "slug": "all-sports",
        "is_active": 1,
        "first_historical_data": "2020-07-08T23:38:36.034Z",
        "last_historical_data": "2020-08-18T12:02:32.371Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2d0e95bd4795d7ace0da3c0ff7b706a5970eb9d3"
        }
      },
      {
        "id": 2474,
        "name": "Matrix AI Network",
        "symbol": "MAN",
        "slug": "matrix-ai-network",
        "is_active": 1,
        "first_historical_data": "2020-04-12T20:35:37.423Z",
        "last_historical_data": "2020-08-17T19:05:25.054Z",
        "platform": null
      },
      {
        "id": 2475,
        "name": "Garlicoin",
        "symbol": "GRLC",
        "slug": "garlicoin",
        "is_active": 1,
        "first_historical_data": "2019-08-29T16:50:29.785Z",
        "last_historical_data": "2020-08-17T21:02:09.141Z",
        "platform": null
      },
      {
        "id": 2476,
        "name": "Ruff",
        "symbol": "RUFF",
        "slug": "ruff",
        "is_active": 1,
        "first_historical_data": "2020-02-19T09:40:59.982Z",
        "last_historical_data": "2020-08-18T11:15:05.786Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf278c1ca969095ffddded020290cf8b5c424ace2"
        }
      },
      {
        "id": 2477,
        "name": "Nework",
        "symbol": "NKC",
        "slug": "nework",
        "is_active": 1,
        "first_historical_data": "2019-09-24T02:26:32.134Z",
        "last_historical_data": "2020-08-18T12:49:54.012Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5a1a29dbb6ad6153db764568c1289076bc876df6"
        }
      },
      {
        "id": 2478,
        "name": "CoinFi",
        "symbol": "COFI",
        "slug": "coinfi",
        "is_active": 1,
        "first_historical_data": "2020-01-11T15:30:44.793Z",
        "last_historical_data": "2020-08-18T03:47:23.983Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x3136ef851592acf49ca4c825131e364170fa32b3"
        }
      },
      {
        "id": 2479,
        "name": "Equal",
        "symbol": "EQL",
        "slug": "equal",
        "is_active": 1,
        "first_historical_data": "2020-03-31T00:20:09.108Z",
        "last_historical_data": "2020-08-18T03:05:02.155Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "EQL-586"
        }
      },
      {
        "id": 2480,
        "name": "HalalChain",
        "symbol": "HLC",
        "slug": "qitmeer",
        "is_active": 1,
        "first_historical_data": "2020-03-01T04:40:56.787Z",
        "last_historical_data": "2020-08-18T09:11:40.794Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x58c69ed6cd6887c0225d1fccecc055127843c69b"
        }
      },
      {
        "id": 2481,
        "name": "Zeepin",
        "symbol": "ZPT",
        "slug": "zeepin",
        "is_active": 1,
        "first_historical_data": "2019-08-23T02:02:24.829Z",
        "last_historical_data": "2020-08-18T07:50:09.354Z",
        "platform": null
      },
      {
        "id": 2482,
        "name": "CPChain",
        "symbol": "CPC",
        "slug": "cpchain",
        "is_active": 1,
        "first_historical_data": "2020-08-01T12:10:37.201Z",
        "last_historical_data": "2020-08-18T04:45:33.805Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "CPC-FED"
        }
      },
      {
        "id": 2483,
        "name": "OceanChain",
        "symbol": "OC",
        "slug": "oceanchain",
        "is_active": 1,
        "first_historical_data": "2020-04-02T02:42:16.558Z",
        "last_historical_data": "2020-08-18T17:17:01.989Z",
        "platform": null
      },
      {
        "id": 2484,
        "name": "Hi Mutual Society",
        "symbol": "HMC",
        "slug": "hi-mutual-society",
        "is_active": 1,
        "first_historical_data": "2019-10-20T02:12:46.365Z",
        "last_historical_data": "2020-08-18T01:02:03.400Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xAa0bb10CEc1fa372eb3Abc17C933FC6ba863DD9E"
        }
      },
      {
        "id": 2488,
        "name": "ValueChain",
        "symbol": "VLC",
        "slug": "valuechain",
        "is_active": 1,
        "first_historical_data": "2020-08-15T17:43:37.132Z",
        "last_historical_data": "2020-08-18T16:39:44.485Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8f7b0b40e27e357540f90f187d90ce06366ac5a5"
        }
      },
      {
        "id": 2489,
        "name": "BitWhite",
        "symbol": "BTW",
        "slug": "bitwhite",
        "is_active": 1,
        "first_historical_data": "2020-04-08T07:56:31.540Z",
        "last_historical_data": "2020-08-18T05:01:04.632Z",
        "platform": null
      },
      {
        "id": 2490,
        "name": "CargoX",
        "symbol": "CXO",
        "slug": "cargox",
        "is_active": 1,
        "first_historical_data": "2020-08-13T00:59:24.280Z",
        "last_historical_data": "2020-08-18T12:16:16.214Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb6ee9668771a79be7967ee29a63d4184f8097143"
        }
      },
      {
        "id": 2491,
        "name": "CaixaPay",
        "symbol": "CXP",
        "slug": "caixapay",
        "is_active": 1,
        "first_historical_data": "2019-12-16T22:45:40.518Z",
        "last_historical_data": "2020-08-18T11:22:31.999Z",
        "platform": null
      },
      {
        "id": 2492,
        "name": "Elastos",
        "symbol": "ELA",
        "slug": "elastos",
        "is_active": 1,
        "first_historical_data": "2020-04-04T06:53:33.405Z",
        "last_historical_data": "2020-08-18T17:07:34.916Z",
        "platform": null
      },
      {
        "id": 2493,
        "name": "STK",
        "symbol": "STK",
        "slug": "stk",
        "is_active": 1,
        "first_historical_data": "2020-07-30T10:03:20.990Z",
        "last_historical_data": "2020-08-18T10:07:13.872Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xae73b38d1c9a8b274127ec30160a4927c4d71824"
        }
      },
      {
        "id": 2495,
        "name": "PARETO Rewards",
        "symbol": "PARETO",
        "slug": "pareto-rewards",
        "is_active": 1,
        "first_historical_data": "2019-10-23T01:21:18.580Z",
        "last_historical_data": "2020-08-18T15:32:30.875Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xea5f88e54d982cbb0c441cde4e79bc305e5b43bc"
        }
      },
      {
        "id": 2496,
        "name": "Polymath",
        "symbol": "POLY",
        "slug": "polymath-network",
        "is_active": 1,
        "first_historical_data": "2020-04-02T15:32:50.968Z",
        "last_historical_data": "2020-08-18T01:43:23.320Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9992ec3cf6a55b00978cddf2b27bc6882d88d1ec"
        }
      },
      {
        "id": 2497,
        "name": "Medicalchain",
        "symbol": "MTN",
        "slug": "medical-chain",
        "is_active": 1,
        "first_historical_data": "2020-01-20T05:24:38.894Z",
        "last_historical_data": "2020-08-18T02:06:16.202Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x41dbecc1cdc5517c6f76f6a6e836adbee2754de3"
        }
      },
      {
        "id": 2498,
        "name": "Jibrel Network",
        "symbol": "JNT",
        "slug": "jibrel-network",
        "is_active": 1,
        "first_historical_data": "2020-04-23T20:13:26.464Z",
        "last_historical_data": "2020-08-18T14:35:09.178Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa5fd1a791c4dfcaacc963d4f73c6ae5824149ea7"
        }
      },
      {
        "id": 2499,
        "name": "SwissBorg",
        "symbol": "CHSB",
        "slug": "swissborg",
        "is_active": 1,
        "first_historical_data": "2019-10-29T16:44:26.435Z",
        "last_historical_data": "2020-08-18T15:43:12.023Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xba9d4199fab4f26efe3551d490e3821486f135ba"
        }
      },
      {
        "id": 2500,
        "name": "Zilla",
        "symbol": "ZLA",
        "slug": "zilla",
        "is_active": 1,
        "first_historical_data": "2019-10-27T12:16:39.761Z",
        "last_historical_data": "2020-08-18T04:06:50.481Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xfd8971d5e8e1740ce2d0a84095fca4de729d0c16"
        }
      },
      {
        "id": 2501,
        "name": "adbank",
        "symbol": "ADB",
        "slug": "adbank",
        "is_active": 1,
        "first_historical_data": "2020-03-30T16:42:51.222Z",
        "last_historical_data": "2020-08-18T09:31:00.599Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2baac9330cf9ac479d819195794d79ad0c7616e3"
        }
      },
      {
        "id": 2502,
        "name": "Huobi Token",
        "symbol": "HT",
        "slug": "huobi-token",
        "is_active": 1,
        "first_historical_data": "2019-12-16T23:29:23.246Z",
        "last_historical_data": "2020-08-17T17:59:05.239Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6f259637dcd74c767781e37bc6133cd6a68aa161"
        }
      },
      {
        "id": 2503,
        "name": "DMarket",
        "symbol": "DMT",
        "slug": "dmarket",
        "is_active": 1,
        "first_historical_data": "2020-03-05T12:54:54.153Z",
        "last_historical_data": "2020-08-18T00:08:36.787Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2ccbff3a042c68716ed2a2cb0c544a9f1d1935e1"
        }
      },
      {
        "id": 2504,
        "name": "Iungo",
        "symbol": "ING",
        "slug": "iungo",
        "is_active": 1,
        "first_historical_data": "2020-04-12T14:07:39.978Z",
        "last_historical_data": "2020-08-17T23:44:53.249Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x24ddff6d8b8a42d835af3b440de91f3386554aa4"
        }
      },
      {
        "id": 2505,
        "name": "Bluzelle",
        "symbol": "BLZ",
        "slug": "bluzelle",
        "is_active": 1,
        "first_historical_data": "2020-03-15T08:02:26.739Z",
        "last_historical_data": "2020-08-18T10:35:23.947Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5732046a883704404f284ce41ffadd5b007fd668"
        }
      },
      {
        "id": 2506,
        "name": "Swarm",
        "symbol": "SWM",
        "slug": "swarm-fund",
        "is_active": 1,
        "first_historical_data": "2019-12-24T07:13:05.541Z",
        "last_historical_data": "2020-08-18T08:46:03.325Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x3505f494c3f0fed0b594e01fa41dd3967645ca39"
        }
      },
      {
        "id": 2507,
        "name": "THEKEY",
        "symbol": "TKY",
        "slug": "thekey",
        "is_active": 1,
        "first_historical_data": "2020-04-20T15:52:23.132Z",
        "last_historical_data": "2020-08-18T01:53:57.729Z",
        "platform": {
          "id": 1376,
          "name": "Neo",
          "symbol": "NEO",
          "slug": "neo",
          "token_address": "132947096727c84c7f9e076c90f08fec3bc17f18"
        }
      },
      {
        "id": 2509,
        "name": "EtherSportz",
        "symbol": "ESZ",
        "slug": "ethersportz",
        "is_active": 1,
        "first_historical_data": "2020-05-15T21:14:55.673Z",
        "last_historical_data": "2020-08-18T14:24:48.332Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe8a1df958be379045e2b46a31a98b93a2ecdfded"
        }
      },
      {
        "id": 2510,
        "name": "Datawallet",
        "symbol": "DXT",
        "slug": "datawallet",
        "is_active": 1,
        "first_historical_data": "2020-04-12T19:03:34.646Z",
        "last_historical_data": "2020-08-18T11:57:23.821Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8db54ca569d3019a2ba126d03c37c44b5ef81ef6"
        }
      },
      {
        "id": 2511,
        "name": "WePower",
        "symbol": "WPR",
        "slug": "wepower",
        "is_active": 1,
        "first_historical_data": "2020-03-04T02:26:26.210Z",
        "last_historical_data": "2020-08-18T16:30:44.420Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4CF488387F035FF08c371515562CBa712f9015d4"
        }
      },
      {
        "id": 2512,
        "name": "UNIVERSAL CASH",
        "symbol": "UCASH",
        "slug": "ucash",
        "is_active": 1,
        "first_historical_data": "2019-08-29T06:50:34.087Z",
        "last_historical_data": "2020-08-17T18:34:01.624Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x92e52a1a235d9a103d970901066ce910aacefd37"
        }
      },
      {
        "id": 2513,
        "name": "GoldMint",
        "symbol": "MNTP",
        "slug": "goldmint",
        "is_active": 1,
        "first_historical_data": "2020-02-04T00:24:58.628Z",
        "last_historical_data": "2020-08-17T22:47:31.169Z",
        "platform": null
      },
      {
        "id": 2516,
        "name": "MktCoin",
        "symbol": "MLM",
        "slug": "mktcoin",
        "is_active": 1,
        "first_historical_data": "2020-05-17T17:25:01.567Z",
        "last_historical_data": "2020-08-18T14:03:16.480Z",
        "platform": null
      },
      {
        "id": 2517,
        "name": "Animation Vision Cash",
        "symbol": "AVH",
        "slug": "animation-vision-cash",
        "is_active": 1,
        "first_historical_data": "2019-09-16T22:45:52.130Z",
        "last_historical_data": "2020-08-18T04:38:57.661Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd7cddd45629934c2f6ed3b63217bd8085d7c14a8"
        }
      },
      {
        "id": 2518,
        "name": "LOCIcoin",
        "symbol": "LOCI",
        "slug": "locicoin",
        "is_active": 1,
        "first_historical_data": "2020-06-19T20:54:52.244Z",
        "last_historical_data": "2020-08-18T03:08:52.968Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9c23d67aea7b95d80942e3836bcdf7e708a747c2"
        }
      },
      {
        "id": 2521,
        "name": "BioCoin",
        "symbol": "BIO",
        "slug": "biocoin",
        "is_active": 1,
        "first_historical_data": "2019-10-02T15:56:31.017Z",
        "last_historical_data": "2020-08-18T05:43:02.531Z",
        "platform": null
      },
      {
        "id": 2524,
        "name": "Universa",
        "symbol": "UTNP",
        "slug": "universa",
        "is_active": 1,
        "first_historical_data": "2020-06-23T12:25:41.828Z",
        "last_historical_data": "2020-08-17T18:47:40.822Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9e3319636e2126e3c0bc9e3134AEC5e1508A46c7"
        }
      },
      {
        "id": 2525,
        "name": "Alphacat",
        "symbol": "ACAT",
        "slug": "alphacat",
        "is_active": 1,
        "first_historical_data": "2019-09-25T10:40:41.324Z",
        "last_historical_data": "2020-08-17T20:45:59.685Z",
        "platform": {
          "id": 1376,
          "name": "Neo",
          "symbol": "NEO",
          "slug": "neo",
          "token_address": "7f86d61ff377f1b12e589a5907152b57e2ad9a7a"
        }
      },
      {
        "id": 2526,
        "name": "Envion",
        "symbol": "EVN",
        "slug": "envion",
        "is_active": 1,
        "first_historical_data": "2020-06-07T18:12:43.873Z",
        "last_historical_data": "2020-08-18T05:09:34.862Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd780ae2bf04cd96e577d3d014762f831d97129d0"
        }
      },
      {
        "id": 2528,
        "name": "Dether",
        "symbol": "DTH",
        "slug": "dether",
        "is_active": 1,
        "first_historical_data": "2019-12-13T07:17:49.365Z",
        "last_historical_data": "2020-08-17T21:52:07.532Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5adc961d6ac3f7062d2ea45fefb8d8167d44b190"
        }
      },
      {
        "id": 2529,
        "name": "Cashaa",
        "symbol": "CAS",
        "slug": "cashaa",
        "is_active": 1,
        "first_historical_data": "2019-11-19T20:21:38.602Z",
        "last_historical_data": "2020-08-18T01:21:30.502Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "CAS-167"
        }
      },
      {
        "id": 2530,
        "name": "Fusion",
        "symbol": "FSN",
        "slug": "fusion",
        "is_active": 1,
        "first_historical_data": "2020-04-18T02:37:10.609Z",
        "last_historical_data": "2020-08-18T10:10:31.526Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "FSN-E14"
        }
      },
      {
        "id": 2533,
        "name": "Restart Energy MWAT",
        "symbol": "MWAT",
        "slug": "restart-energy-mwat",
        "is_active": 1,
        "first_historical_data": "2020-05-23T14:06:46.690Z",
        "last_historical_data": "2020-08-18T09:43:55.371Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6425c6be902d692ae2db752b3c268afadb099d3b"
        }
      },
      {
        "id": 2535,
        "name": "Edge",
        "symbol": "DADI",
        "slug": "edge",
        "is_active": 1,
        "first_historical_data": "2020-04-15T21:32:58.602Z",
        "last_historical_data": "2020-08-18T00:31:21.949Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xfb2f26f266fb2805a387230f2aa0a331b4d96fba"
        }
      },
      {
        "id": 2536,
        "name": "Neurotoken",
        "symbol": "NTK",
        "slug": "neurotoken",
        "is_active": 1,
        "first_historical_data": "2019-09-08T15:14:45.809Z",
        "last_historical_data": "2020-08-17T21:11:39.632Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x69beab403438253f13b6e92db91f7fb849258263"
        }
      },
      {
        "id": 2537,
        "name": "Gems ",
        "symbol": "GEM",
        "slug": "gems-protocol",
        "is_active": 1,
        "first_historical_data": "2020-07-24T13:29:46.775Z",
        "last_historical_data": "2020-08-18T07:55:10.533Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc7bba5b765581efb2cdd2679db5bea9ee79b201f"
        }
      },
      {
        "id": 2538,
        "name": "Nectar",
        "symbol": "NEC",
        "slug": "nectar",
        "is_active": 1,
        "first_historical_data": "2020-04-01T10:54:11.805Z",
        "last_historical_data": "2020-08-17T18:31:14.036Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xcc80c051057b774cd75067dc48f8987c4eb97a5e"
        }
      },
      {
        "id": 2539,
        "name": "Ren",
        "symbol": "REN",
        "slug": "ren",
        "is_active": 1,
        "first_historical_data": "2019-09-20T11:54:15.893Z",
        "last_historical_data": "2020-08-17T20:50:37.080Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x408e41876cccdc0f92210600ef50372656052a38"
        }
      },
      {
        "id": 2540,
        "name": "Litecoin Cash",
        "symbol": "LCC",
        "slug": "litecoin-cash",
        "is_active": 1,
        "first_historical_data": "2020-06-10T12:19:16.482Z",
        "last_historical_data": "2020-08-17T23:31:02.272Z",
        "platform": null
      },
      {
        "id": 2541,
        "name": "Storiqa",
        "symbol": "STQ",
        "slug": "storiqa",
        "is_active": 1,
        "first_historical_data": "2020-08-16T19:46:03.827Z",
        "last_historical_data": "2020-08-18T03:29:51.905Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5c3a228510d246b78a3765c20221cbf3082b44a4"
        }
      },
      {
        "id": 2542,
        "name": "Tidex Token",
        "symbol": "TDX",
        "slug": "tidex-token",
        "is_active": 1,
        "first_historical_data": "2020-04-24T09:08:38.636Z",
        "last_historical_data": "2020-08-17T21:13:23.830Z",
        "platform": {
          "id": 1274,
          "name": "Waves",
          "symbol": "WAVES",
          "slug": "waves",
          "token_address": "3QvxP6YFBKpWJSMAfYtL8Niv8KmmKsnpb9uQwQpg8QN2"
        }
      },
      {
        "id": 2544,
        "name": "Nucleus Vision",
        "symbol": "NCASH",
        "slug": "nucleus-vision",
        "is_active": 1,
        "first_historical_data": "2020-07-11T10:13:35.643Z",
        "last_historical_data": "2020-08-18T12:43:52.139Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x809826cceab68c387726af962713b64cb5cb3cca"
        }
      },
      {
        "id": 2545,
        "name": "Arcblock",
        "symbol": "ABT",
        "slug": "arcblock",
        "is_active": 1,
        "first_historical_data": "2019-10-08T07:19:50.076Z",
        "last_historical_data": "2020-08-17T23:29:49.803Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb98d4c97425d9908e66e53a6fdf673acca0be986"
        }
      },
      {
        "id": 2546,
        "name": "Remme",
        "symbol": "REM",
        "slug": "remme",
        "is_active": 1,
        "first_historical_data": "2019-11-02T06:44:46.550Z",
        "last_historical_data": "2020-08-17T23:56:34.241Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x83984d6142934bb535793a82adb0a46ef0f66b6d"
        }
      },
      {
        "id": 2547,
        "name": "Experty",
        "symbol": "EXY",
        "slug": "experty",
        "is_active": 1,
        "first_historical_data": "2020-06-30T18:20:09.468Z",
        "last_historical_data": "2020-08-18T03:03:01.573Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5c743a35e903f6c584514ec617acee0611cf44f3"
        }
      },
      {
        "id": 2548,
        "name": "POA",
        "symbol": "POA",
        "slug": "poa",
        "is_active": 1,
        "first_historical_data": "2020-04-12T18:06:16.843Z",
        "last_historical_data": "2020-08-18T03:32:18.433Z",
        "platform": null
      },
      {
        "id": 2549,
        "name": "Ink Protocol",
        "symbol": "XNK",
        "slug": "ink-protocol",
        "is_active": 1,
        "first_historical_data": "2019-11-22T07:08:06.567Z",
        "last_historical_data": "2020-08-18T12:34:14.094Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xbc86727e770de68b1060c91f6bb6945c73e10388"
        }
      },
      {
        "id": 2551,
        "name": "Bezop",
        "symbol": "BEZ",
        "slug": "bezop",
        "is_active": 1,
        "first_historical_data": "2019-10-31T03:48:13.216Z",
        "last_historical_data": "2020-08-18T13:27:34.579Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8a1e3930fde1f151471c368fdbb39f3f63a65b55"
        }
      },
      {
        "id": 2552,
        "name": "IHT Real Estate Protocol",
        "symbol": "IHT",
        "slug": "iht-real-estate-protocol",
        "is_active": 1,
        "first_historical_data": "2020-02-01T11:59:49.963Z",
        "last_historical_data": "2020-08-18T10:39:52.536Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xeda8b016efa8b1161208cf041cd86972eee0f31e"
        }
      },
      {
        "id": 2553,
        "name": "Refereum",
        "symbol": "RFR",
        "slug": "refereum",
        "is_active": 1,
        "first_historical_data": "2019-12-11T10:12:47.130Z",
        "last_historical_data": "2020-08-17T20:29:16.093Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd0929d411954c47438dc1d871dd6081f5c5e149c"
        }
      },
      {
        "id": 2554,
        "name": "Lympo",
        "symbol": "LYM",
        "slug": "lympo",
        "is_active": 1,
        "first_historical_data": "2019-10-20T19:24:14.087Z",
        "last_historical_data": "2020-08-18T15:45:46.507Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc690f7c7fcffa6a82b79fab7508c466fefdfc8c5"
        }
      },
      {
        "id": 2556,
        "name": "Credits",
        "symbol": "CS",
        "slug": "credits",
        "is_active": 1,
        "first_historical_data": "2020-01-01T23:42:23.018Z",
        "last_historical_data": "2020-08-18T05:53:17.540Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x46b9ad944d1059450da1163511069c718f699d31"
        }
      },
      {
        "id": 2557,
        "name": "Bee Token",
        "symbol": "BEE",
        "slug": "bee-token",
        "is_active": 1,
        "first_historical_data": "2020-07-24T01:54:51.051Z",
        "last_historical_data": "2020-08-18T09:57:16.962Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4d8fc1453a0f359e99c9675954e656d80d996fbf"
        }
      },
      {
        "id": 2558,
        "name": "Insights Network",
        "symbol": "INSTAR",
        "slug": "insights-network",
        "is_active": 1,
        "first_historical_data": "2019-11-25T03:45:35.664Z",
        "last_historical_data": "2020-08-18T07:31:53.128Z",
        "platform": null
      },
      {
        "id": 2559,
        "name": "Cube",
        "symbol": "AUTO",
        "slug": "cube",
        "is_active": 1,
        "first_historical_data": "2019-12-25T08:33:07.030Z",
        "last_historical_data": "2020-08-17T23:51:35.544Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x622dffcc4e83c64ba959530a5a5580687a57581b"
        }
      },
      {
        "id": 2561,
        "name": "BitTube",
        "symbol": "TUBE",
        "slug": "bit-tube",
        "is_active": 1,
        "first_historical_data": "2020-03-12T00:55:49.315Z",
        "last_historical_data": "2020-08-18T14:06:47.478Z",
        "platform": null
      },
      {
        "id": 2562,
        "name": "Education Ecosystem",
        "symbol": "LEDU",
        "slug": "education-ecosystem",
        "is_active": 1,
        "first_historical_data": "2020-05-06T05:09:52.785Z",
        "last_historical_data": "2020-08-18T09:19:51.512Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc741f06082aa47f93729070ad0dd95e223bda091"
        }
      },
      {
        "id": 2563,
        "name": "TrueUSD",
        "symbol": "TUSD",
        "slug": "trueusd",
        "is_active": 1,
        "first_historical_data": "2020-03-28T05:05:58.862Z",
        "last_historical_data": "2020-08-18T17:39:28.631Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "TUSDB-888"
        }
      },
      {
        "id": 2564,
        "name": "HOQU",
        "symbol": "HQX",
        "slug": "hoqu",
        "is_active": 1,
        "first_historical_data": "2020-06-22T09:26:45.358Z",
        "last_historical_data": "2020-08-18T03:37:25.716Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1b957dc4aefeed3b4a2351a6a6d5cbfbba0cecfa"
        }
      },
      {
        "id": 2565,
        "name": "StarterCoin",
        "symbol": "STAC",
        "slug": "startercoin",
        "is_active": 1,
        "first_historical_data": "2019-10-22T02:42:53.534Z",
        "last_historical_data": "2020-08-18T17:44:42.113Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9a005c9a89bd72a4bd27721e7a09a3c11d2b03c4"
        }
      },
      {
        "id": 2566,
        "name": "Ontology",
        "symbol": "ONT",
        "slug": "ontology",
        "is_active": 1,
        "first_historical_data": "2019-11-28T12:56:23.658Z",
        "last_historical_data": "2020-08-18T09:19:29.948Z",
        "platform": null
      },
      {
        "id": 2567,
        "name": "DATx",
        "symbol": "DATX",
        "slug": "datx",
        "is_active": 1,
        "first_historical_data": "2020-07-14T04:12:22.022Z",
        "last_historical_data": "2020-08-18T10:48:45.017Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xabbbb6447b68ffd6141da77c18c7b5876ed6c5ab"
        }
      },
      {
        "id": 2568,
        "name": "JET8",
        "symbol": "J8T",
        "slug": "jet8",
        "is_active": 1,
        "first_historical_data": "2019-10-12T04:38:44.493Z",
        "last_historical_data": "2020-08-18T00:26:05.771Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0d262e5dc4a06a0f1c90ce79c7a60c09dfc884e4"
        }
      },
      {
        "id": 2569,
        "name": "CoinPoker",
        "symbol": "CHP",
        "slug": "coinpoker",
        "is_active": 1,
        "first_historical_data": "2020-01-09T03:51:27.095Z",
        "last_historical_data": "2020-08-17T23:24:56.540Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf3db7560e820834658b590c96234c333cd3d5e5e"
        }
      },
      {
        "id": 2570,
        "name": "TomoChain",
        "symbol": "TOMO",
        "slug": "tomochain",
        "is_active": 1,
        "first_historical_data": "2020-01-02T20:59:03.067Z",
        "last_historical_data": "2020-08-17T18:07:05.597Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "TOMOB-4BC"
        }
      },
      {
        "id": 2571,
        "name": "Graft",
        "symbol": "GRFT",
        "slug": "graft",
        "is_active": 1,
        "first_historical_data": "2020-04-08T11:59:49.889Z",
        "last_historical_data": "2020-08-18T16:13:41.321Z",
        "platform": null
      },
      {
        "id": 2572,
        "name": "BABB",
        "symbol": "BAX",
        "slug": "babb",
        "is_active": 1,
        "first_historical_data": "2020-05-17T09:57:44.617Z",
        "last_historical_data": "2020-08-17T20:52:36.120Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9a0242b7a33dacbe40edb927834f96eb39f8fbcb"
        }
      },
      {
        "id": 2573,
        "name": "Electrify.Asia",
        "symbol": "ELEC",
        "slug": "electrifyasia",
        "is_active": 1,
        "first_historical_data": "2019-11-08T22:55:26.198Z",
        "last_historical_data": "2020-08-18T04:06:25.859Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd49ff13661451313ca1553fd6954bd1d9b6e02b9"
        }
      },
      {
        "id": 2575,
        "name": "Bitcoin Private",
        "symbol": "BTCP",
        "slug": "bitcoin-private",
        "is_active": 1,
        "first_historical_data": "2020-04-02T10:47:31.181Z",
        "last_historical_data": "2020-08-18T02:51:05.444Z",
        "platform": null
      },
      {
        "id": 2576,
        "name": "Tokenomy",
        "symbol": "TEN",
        "slug": "tokenomy",
        "is_active": 1,
        "first_historical_data": "2020-03-20T13:04:19.530Z",
        "last_historical_data": "2020-08-18T16:26:46.063Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xdd16ec0f66e54d453e6756713e533355989040e4"
        }
      },
      {
        "id": 2577,
        "name": "Ravencoin",
        "symbol": "RVN",
        "slug": "ravencoin",
        "is_active": 1,
        "first_historical_data": "2019-12-10T18:19:44.175Z",
        "last_historical_data": "2020-08-18T01:43:19.964Z",
        "platform": null
      },
      {
        "id": 2578,
        "name": "TE-FOOD",
        "symbol": "TFD",
        "slug": "te-food",
        "is_active": 1,
        "first_historical_data": "2019-09-08T22:19:30.204Z",
        "last_historical_data": "2020-08-18T08:10:59.707Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe5f166c0d8872b68790061317bb6cca04582c912"
        }
      },
      {
        "id": 2579,
        "name": "ShipChain",
        "symbol": "SHIP",
        "slug": "shipchain",
        "is_active": 1,
        "first_historical_data": "2020-03-20T16:29:45.831Z",
        "last_historical_data": "2020-08-18T06:57:27.211Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe25b0bba01dc5630312b6a21927e578061a13f55"
        }
      },
      {
        "id": 2580,
        "name": "Leadcoin",
        "symbol": "LDC",
        "slug": "leadcoin",
        "is_active": 1,
        "first_historical_data": "2019-09-24T14:47:19.163Z",
        "last_historical_data": "2020-08-18T02:29:34.174Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x5102791ca02fc3595398400bfe0e33d7b6c82267"
        }
      },
      {
        "id": 2582,
        "name": "LALA World",
        "symbol": "LALA",
        "slug": "lala-world",
        "is_active": 1,
        "first_historical_data": "2020-01-30T12:15:46.793Z",
        "last_historical_data": "2020-08-17T20:59:21.675Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xfd107b473ab90e8fbd89872144a3dc92c40fa8c9"
        }
      },
      {
        "id": 2583,
        "name": "Octoin Coin",
        "symbol": "OCC",
        "slug": "octoin-coin",
        "is_active": 1,
        "first_historical_data": "2019-09-21T04:18:44.832Z",
        "last_historical_data": "2020-08-18T13:59:01.554Z",
        "platform": null
      },
      {
        "id": 2584,
        "name": "Debitum",
        "symbol": "DEB",
        "slug": "debitum-network",
        "is_active": 1,
        "first_historical_data": "2019-10-08T10:28:29.244Z",
        "last_historical_data": "2020-08-18T04:33:36.736Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x151202c9c18e495656f372281f493eb7698961d5"
        }
      },
      {
        "id": 2585,
        "name": "Centrality",
        "symbol": "CENNZ",
        "slug": "centrality",
        "is_active": 1,
        "first_historical_data": "2019-09-25T02:27:24.883Z",
        "last_historical_data": "2020-08-18T05:27:37.245Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1122b6a0e00dce0563082b6e2953f3a943855c1f"
        }
      },
      {
        "id": 2586,
        "name": "Synthetix Network Token",
        "symbol": "SNX",
        "slug": "synthetix-network-token",
        "is_active": 1,
        "first_historical_data": "2020-08-07T01:15:04.269Z",
        "last_historical_data": "2020-08-18T15:46:25.882Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc011a73ee8576fb46f5e1c5751ca3b9fe0af2a6f"
        }
      },
      {
        "id": 2588,
        "name": "Loom Network",
        "symbol": "LOOM",
        "slug": "loom-network",
        "is_active": 1,
        "first_historical_data": "2019-12-07T13:43:21.078Z",
        "last_historical_data": "2020-08-18T12:13:56.099Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa4e8c3ec456107ea67d3075bf9e3df3a75823db0"
        }
      },
      {
        "id": 2589,
        "name": "Guaranteed Ethurance Token Extra",
        "symbol": "GETX",
        "slug": "guaranteed-ethurance-token-extra",
        "is_active": 1,
        "first_historical_data": "2020-05-22T00:38:12.478Z",
        "last_historical_data": "2020-08-17T19:21:55.513Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x07a58629aaf3e1a0d07d8f43114b76bd5eee3b91"
        }
      },
      {
        "id": 2591,
        "name": "Dropil",
        "symbol": "DROP",
        "slug": "dropil",
        "is_active": 1,
        "first_historical_data": "2020-07-29T08:00:19.087Z",
        "last_historical_data": "2020-08-17T22:52:59.261Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4672bad527107471cb5067a887f4656d585a8a31"
        }
      },
      {
        "id": 2592,
        "name": "Banca",
        "symbol": "BANCA",
        "slug": "banca",
        "is_active": 1,
        "first_historical_data": "2020-04-07T09:52:04.197Z",
        "last_historical_data": "2020-08-18T10:03:26.518Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x998b3b82bc9dba173990be7afb772788b5acb8bd"
        }
      },
      {
        "id": 2593,
        "name": "Dragon Coins",
        "symbol": "DRG",
        "slug": "dragon-coins",
        "is_active": 1,
        "first_historical_data": "2019-11-20T11:08:57.882Z",
        "last_historical_data": "2020-08-18T02:30:46.531Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x814f67fa286f7572b041d041b1d99b432c9155ee"
        }
      },
      {
        "id": 2595,
        "name": "NANJCOIN",
        "symbol": "NANJ",
        "slug": "nanjcoin",
        "is_active": 1,
        "first_historical_data": "2019-10-26T22:45:38.176Z",
        "last_historical_data": "2020-08-18T15:23:32.582Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xffe02ee4c69edf1b340fcad64fbd6b37a7b9e265"
        }
      },
      {
        "id": 2596,
        "name": "CK USD",
        "symbol": "CKUSD",
        "slug": "ckusd",
        "is_active": 1,
        "first_historical_data": "2020-01-24T15:00:05.215Z",
        "last_historical_data": "2020-08-18T05:12:32.998Z",
        "platform": null
      },
      {
        "id": 2597,
        "name": "UpToken",
        "symbol": "UP",
        "slug": "uptoken",
        "is_active": 1,
        "first_historical_data": "2020-02-07T20:17:31.056Z",
        "last_historical_data": "2020-08-18T05:24:35.216Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6ba460ab75cd2c56343b3517ffeba60748654d26"
        }
      },
      {
        "id": 2601,
        "name": "1World",
        "symbol": "1WO",
        "slug": "1world",
        "is_active": 1,
        "first_historical_data": "2019-11-03T15:36:17.999Z",
        "last_historical_data": "2020-08-18T05:40:18.637Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xfdbc1adc26f0f8f8606a5d63b7d3a3cd21c22b23"
        }
      },
      {
        "id": 2602,
        "name": "NaPoleonX",
        "symbol": "NPX",
        "slug": "napoleonx",
        "is_active": 1,
        "first_historical_data": "2020-05-31T17:31:28.271Z",
        "last_historical_data": "2020-08-18T07:04:00.210Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x28b5e12cce51f15594b0b91d5b5adaa70f684a02"
        }
      },
      {
        "id": 2603,
        "name": "Pundi X",
        "symbol": "NPXS",
        "slug": "pundi-x",
        "is_active": 1,
        "first_historical_data": "2020-02-24T04:13:56.987Z",
        "last_historical_data": "2020-08-18T16:25:00.186Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa15c7ebe1f07caf6bff097d8a589fb8ac49ae5b3"
        }
      },
      {
        "id": 2604,
        "name": "BitGreen",
        "symbol": "BITG",
        "slug": "bitgreen",
        "is_active": 1,
        "first_historical_data": "2020-08-16T01:17:38.157Z",
        "last_historical_data": "2020-08-18T03:11:24.211Z",
        "platform": null
      },
      {
        "id": 2605,
        "name": "BnkToTheFuture",
        "symbol": "BFT",
        "slug": "bnktothefuture",
        "is_active": 1,
        "first_historical_data": "2020-08-08T14:58:37.083Z",
        "last_historical_data": "2020-08-18T09:33:00.559Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x01ff50f8b7f74e4f00580d9596cd3d0d6d6e326f"
        }
      },
      {
        "id": 2606,
        "name": "Wanchain",
        "symbol": "WAN",
        "slug": "wanchain",
        "is_active": 1,
        "first_historical_data": "2020-06-30T01:42:02.502Z",
        "last_historical_data": "2020-08-18T07:03:21.428Z",
        "platform": null
      },
      {
        "id": 2607,
        "name": "AMLT",
        "symbol": "AMLT",
        "slug": "amlt",
        "is_active": 1,
        "first_historical_data": "2020-04-05T23:35:44.025Z",
        "last_historical_data": "2020-08-18T00:37:48.778Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xca0e7269600d353f70b14ad118a49575455c0f2f"
        }
      },
      {
        "id": 2608,
        "name": "Mithril",
        "symbol": "MITH",
        "slug": "mithril",
        "is_active": 1,
        "first_historical_data": "2019-08-25T02:19:12.329Z",
        "last_historical_data": "2020-08-18T08:39:48.312Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "MITH-C76"
        }
      },
      {
        "id": 2609,
        "name": "Lendroid Support Token",
        "symbol": "LST",
        "slug": "lendroid-support-token",
        "is_active": 1,
        "first_historical_data": "2019-09-17T10:11:42.568Z",
        "last_historical_data": "2020-08-17T23:55:46.265Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4de2573e27e648607b50e1cfff921a33e4a34405"
        }
      },
      {
        "id": 2610,
        "name": "Peculium",
        "symbol": "PCL",
        "slug": "peculium",
        "is_active": 1,
        "first_historical_data": "2020-01-20T12:29:24.170Z",
        "last_historical_data": "2020-08-17T18:50:36.178Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0f02e27745e3b6e9e1310d19469e2b5d7b5ec99a"
        }
      },
      {
        "id": 2611,
        "name": "Spectiv",
        "symbol": "SIG",
        "slug": "signal-token",
        "is_active": 1,
        "first_historical_data": "2019-10-20T20:37:48.382Z",
        "last_historical_data": "2020-08-18T11:45:55.715Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6888a16ea9792c15a4dcf2f6c623d055c8ede792"
        }
      },
      {
        "id": 2612,
        "name": "BitRent",
        "symbol": "RNTB",
        "slug": "bitrent",
        "is_active": 1,
        "first_historical_data": "2020-05-22T17:18:44.503Z",
        "last_historical_data": "2020-08-18T08:11:17.504Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1fe70be734e473e5721ea57c8b5b01e6caa52686"
        }
      },
      {
        "id": 2614,
        "name": "BlitzPredict",
        "symbol": "XBP",
        "slug": "blitzpredict",
        "is_active": 1,
        "first_historical_data": "2020-02-29T16:17:50.984Z",
        "last_historical_data": "2020-08-18T03:43:21.141Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x28dee01d53fed0edf5f6e310bf8ef9311513ae40"
        }
      },
      {
        "id": 2615,
        "name": "Blocklancer",
        "symbol": "LNC",
        "slug": "blocklancer",
        "is_active": 1,
        "first_historical_data": "2020-02-16T09:53:21.427Z",
        "last_historical_data": "2020-08-17T22:25:41.094Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x63e634330a20150dbb61b15648bc73855d6ccf07"
        }
      },
      {
        "id": 2616,
        "name": "Stipend",
        "symbol": "SPD",
        "slug": "stipend",
        "is_active": 1,
        "first_historical_data": "2020-01-12T10:02:20.796Z",
        "last_historical_data": "2020-08-17T20:51:34.992Z",
        "platform": null
      },
      {
        "id": 2617,
        "name": "IP Exchange",
        "symbol": "IPSX",
        "slug": "ip-exchange",
        "is_active": 1,
        "first_historical_data": "2019-12-13T11:03:08.844Z",
        "last_historical_data": "2020-08-17T19:56:59.786Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x001f0aa5da15585e5b2305dbab2bac425ea71007"
        }
      },
      {
        "id": 2618,
        "name": "StockChain",
        "symbol": "SCC",
        "slug": "stockchain",
        "is_active": 1,
        "first_historical_data": "2019-09-09T04:24:43.117Z",
        "last_historical_data": "2020-08-18T14:38:23.443Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x355a458d555151d3b27f94227960ade1504e526a"
        }
      },
      {
        "id": 2619,
        "name": "BitStation",
        "symbol": "BSTN",
        "slug": "bitstation",
        "is_active": 1,
        "first_historical_data": "2019-11-24T16:31:00.226Z",
        "last_historical_data": "2020-08-17T20:21:08.986Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x2f8472dd7ecf7ca760c8f6b45db20ca7cf52f8d7"
        }
      },
      {
        "id": 2620,
        "name": "Switcheo",
        "symbol": "SWTH",
        "slug": "switcheo",
        "is_active": 1,
        "first_historical_data": "2020-02-29T12:09:55.931Z",
        "last_historical_data": "2020-08-18T03:47:45.667Z",
        "platform": {
          "id": 1376,
          "name": "Neo",
          "symbol": "NEO",
          "slug": "neo",
          "token_address": "ab38352559b8b203bde5fddfa0b07d8b2525e132"
        }
      },
      {
        "id": 2621,
        "name": "Sentient Coin",
        "symbol": "SEN",
        "slug": "consensus",
        "is_active": 1,
        "first_historical_data": "2019-09-01T03:34:48.206Z",
        "last_historical_data": "2020-08-18T07:17:49.804Z",
        "platform": null
      },
      {
        "id": 2624,
        "name": "Sentinel Chain",
        "symbol": "SENC",
        "slug": "sentinel-chain",
        "is_active": 1,
        "first_historical_data": "2020-05-31T19:15:37.689Z",
        "last_historical_data": "2020-08-18T14:07:52.291Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa13f0743951b4f6e3e3aa039f682e17279f52bc3"
        }
      },
      {
        "id": 2626,
        "name": "Friendz",
        "symbol": "FDZ",
        "slug": "friends",
        "is_active": 1,
        "first_historical_data": "2020-05-05T18:54:01.968Z",
        "last_historical_data": "2020-08-17T18:41:29.609Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x23352036e911a22cfc692b5e2e196692658aded9"
        }
      },
      {
        "id": 2627,
        "name": "TokenPay",
        "symbol": "TPAY",
        "slug": "tokenpay",
        "is_active": 1,
        "first_historical_data": "2020-02-05T00:13:44.831Z",
        "last_historical_data": "2020-08-18T15:06:25.446Z",
        "platform": null
      },
      {
        "id": 2628,
        "name": "Rentberry",
        "symbol": "BERRY",
        "slug": "rentberry",
        "is_active": 1,
        "first_historical_data": "2020-05-26T05:18:53.658Z",
        "last_historical_data": "2020-08-17T22:17:12.541Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6aeb95f06cda84ca345c2de0f3b7f96923a44f4c"
        }
      },
      {
        "id": 2629,
        "name": "Scala",
        "symbol": "XLA",
        "slug": "scala",
        "is_active": 1,
        "first_historical_data": "2020-04-06T18:05:48.944Z",
        "last_historical_data": "2020-08-18T03:19:23.790Z",
        "platform": null
      },
      {
        "id": 2630,
        "name": "PolySwarm",
        "symbol": "NCT",
        "slug": "polyswarm",
        "is_active": 1,
        "first_historical_data": "2020-01-13T11:56:02.419Z",
        "last_historical_data": "2020-08-18T02:33:32.819Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9e46a38f5daabe8683e10793b06749eef7d733d1"
        }
      },
      {
        "id": 2631,
        "name": "ODEM",
        "symbol": "ODE",
        "slug": "odem",
        "is_active": 1,
        "first_historical_data": "2019-09-20T16:01:07.506Z",
        "last_historical_data": "2020-08-18T01:42:47.825Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xbf52f2ab39e26e0951d2a02b49b7702abe30406a"
        }
      },
      {
        "id": 2633,
        "name": "Stakenet",
        "symbol": "XSN",
        "slug": "stakenet",
        "is_active": 1,
        "first_historical_data": "2020-08-01T17:36:07.488Z",
        "last_historical_data": "2020-08-18T12:02:58.862Z",
        "platform": null
      },
      {
        "id": 2634,
        "name": "XinFin Network",
        "symbol": "XDCE",
        "slug": "xinfin-network",
        "is_active": 1,
        "first_historical_data": "2020-02-10T07:28:22.626Z",
        "last_historical_data": "2020-08-18T08:45:07.902Z",
        "platform": null
      },
      {
        "id": 2635,
        "name": "TokenDesk",
        "symbol": "TDS",
        "slug": "tokendesk",
        "is_active": 1,
        "first_historical_data": "2019-09-20T23:05:38.375Z",
        "last_historical_data": "2020-08-17T18:47:16.877Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6295ab2be04a617747481b292c390bfca592cf28"
        }
      },
      {
        "id": 2638,
        "name": "Cortex",
        "symbol": "CTXC",
        "slug": "cortex",
        "is_active": 1,
        "first_historical_data": "2020-02-05T01:01:40.363Z",
        "last_historical_data": "2020-08-18T05:26:08.439Z",
        "platform": null
      },
      {
        "id": 2641,
        "name": "Apex",
        "symbol": "CPX",
        "slug": "apex",
        "is_active": 1,
        "first_historical_data": "2020-01-24T08:27:02.496Z",
        "last_historical_data": "2020-08-18T14:48:42.096Z",
        "platform": {
          "id": 1376,
          "name": "Neo",
          "symbol": "NEO",
          "slug": "neo",
          "token_address": "45d493a6f73fa5f404244a5fb8472fc014ca5885"
        }
      },
      {
        "id": 2642,
        "name": "CyberVein",
        "symbol": "CVT",
        "slug": "cybervein",
        "is_active": 1,
        "first_historical_data": "2019-10-07T01:37:52.666Z",
        "last_historical_data": "2020-08-18T09:21:07.826Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xbe428c3867f05dea2a89fc76a102b544eac7f772"
        }
      },
      {
        "id": 2643,
        "name": "Sentinel",
        "symbol": "SENT",
        "slug": "sentinel",
        "is_active": 1,
        "first_historical_data": "2020-03-02T07:36:00.652Z",
        "last_historical_data": "2020-08-18T03:20:17.587Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa44e5137293e855b1b7bc7e2c6f8cd796ffcb037"
        }
      },
      {
        "id": 2644,
        "name": "eosDAC",
        "symbol": "EOSDAC",
        "slug": "eosdac",
        "is_active": 1,
        "first_historical_data": "2020-03-16T19:38:46.440Z",
        "last_historical_data": "2020-08-18T16:38:48.448Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x7e9e431a0b8c4d532c745b1043c7fa29a48d4fba"
        }
      },
      {
        "id": 2645,
        "name": "U Network",
        "symbol": "UUU",
        "slug": "u-network",
        "is_active": 1,
        "first_historical_data": "2020-06-29T04:52:51.163Z",
        "last_historical_data": "2020-08-17T23:20:42.142Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x3543638ed4a9006e4840b105944271bcea15605d"
        }
      },
      {
        "id": 2646,
        "name": "AdHive",
        "symbol": "ADH",
        "slug": "adhive",
        "is_active": 1,
        "first_historical_data": "2020-01-09T00:32:38.474Z",
        "last_historical_data": "2020-08-18T09:16:24.626Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe69a353b3152dd7b706ff7dd40fe1d18b7802d31"
        }
      },
      {
        "id": 2648,
        "name": "Bitsum",
        "symbol": "BSM",
        "slug": "bitsum",
        "is_active": 1,
        "first_historical_data": "2020-01-25T04:06:32.402Z",
        "last_historical_data": "2020-08-18T01:18:09.469Z",
        "platform": null
      },
      {
        "id": 2649,
        "name": "DeviantCoin",
        "symbol": "DEV",
        "slug": "deviantcoin",
        "is_active": 1,
        "first_historical_data": "2020-02-14T18:18:09.610Z",
        "last_historical_data": "2020-08-18T04:18:13.112Z",
        "platform": null
      },
      {
        "id": 2650,
        "name": "CommerceBlock",
        "symbol": "CBT",
        "slug": "commerceblock",
        "is_active": 1,
        "first_historical_data": "2020-06-26T23:06:15.265Z",
        "last_historical_data": "2020-08-17T20:54:56.464Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x076c97e1c869072ee22f8c91978c99b4bcb02591"
        }
      },
      {
        "id": 2653,
        "name": "Auctus",
        "symbol": "AUC",
        "slug": "auctus",
        "is_active": 1,
        "first_historical_data": "2020-04-28T23:53:39.751Z",
        "last_historical_data": "2020-08-18T00:12:24.382Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc12d099be31567add4e4e4d0d45691c3f58f5663"
        }
      },
      {
        "id": 2654,
        "name": "Budbo",
        "symbol": "BUBO",
        "slug": "budbo",
        "is_active": 1,
        "first_historical_data": "2020-01-22T01:23:24.033Z",
        "last_historical_data": "2020-08-18T14:48:04.086Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xccbf21ba6ef00802ab06637896b799f7101f54a2"
        }
      },
      {
        "id": 2655,
        "name": "Monero Classic",
        "symbol": "XMC",
        "slug": "monero-classic",
        "is_active": 1,
        "first_historical_data": "2019-10-29T10:01:16.936Z",
        "last_historical_data": "2020-08-18T00:09:13.577Z",
        "platform": null
      },
      {
        "id": 2656,
        "name": "Daneel",
        "symbol": "DAN",
        "slug": "daneel",
        "is_active": 1,
        "first_historical_data": "2019-11-30T18:06:29.540Z",
        "last_historical_data": "2020-08-18T07:39:32.118Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9b70740e708a083c6ff38df52297020f5dfaa5ee"
        }
      },
      {
        "id": 2658,
        "name": "SyncFab",
        "symbol": "MFG",
        "slug": "syncfab",
        "is_active": 1,
        "first_historical_data": "2019-11-26T20:50:32.190Z",
        "last_historical_data": "2020-08-17T22:57:06.411Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6710c63432a2de02954fc0f851db07146a6c0312"
        }
      },
      {
        "id": 2660,
        "name": "Aditus",
        "symbol": "ADI",
        "slug": "aditus",
        "is_active": 1,
        "first_historical_data": "2020-04-17T08:06:49.523Z",
        "last_historical_data": "2020-08-18T10:29:12.737Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "ADI-6BB"
        }
      },
      {
        "id": 2661,
        "name": "Tripio",
        "symbol": "TRIO",
        "slug": "tripio",
        "is_active": 1,
        "first_historical_data": "2020-05-06T16:04:23.121Z",
        "last_historical_data": "2020-08-18T08:07:57.991Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8b40761142b9aa6dc8964e61d0585995425c3d94"
        }
      },
      {
        "id": 2662,
        "name": "Haven Protocol",
        "symbol": "XHV",
        "slug": "haven-protocol",
        "is_active": 1,
        "first_historical_data": "2020-02-19T14:35:47.606Z",
        "last_historical_data": "2020-08-18T14:36:03.392Z",
        "platform": null
      },
      {
        "id": 2663,
        "name": "StarCoin",
        "symbol": "KST",
        "slug": "starcointv",
        "is_active": 1,
        "first_historical_data": "2020-08-15T12:53:59.622Z",
        "last_historical_data": "2020-08-18T03:32:52.229Z",
        "platform": null
      },
      {
        "id": 2664,
        "name": "CryCash",
        "symbol": "CRC",
        "slug": "crycash",
        "is_active": 1,
        "first_historical_data": "2020-01-02T04:53:24.425Z",
        "last_historical_data": "2020-08-18T00:41:48.013Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf41e5fbc2f6aac200dd8619e121ce1f05d150077"
        }
      },
      {
        "id": 2665,
        "name": "Dero",
        "symbol": "DERO",
        "slug": "dero",
        "is_active": 1,
        "first_historical_data": "2020-03-06T12:07:31.531Z",
        "last_historical_data": "2020-08-17T22:44:00.338Z",
        "platform": null
      },
      {
        "id": 2666,
        "name": "Effect.AI",
        "symbol": "EFX",
        "slug": "effect-ai",
        "is_active": 1,
        "first_historical_data": "2019-12-19T04:08:53.021Z",
        "last_historical_data": "2020-08-18T09:47:35.010Z",
        "platform": {
          "id": 1765,
          "name": "EOS",
          "symbol": "EOS",
          "slug": "eos",
          "token_address": "acbc532904b6b51b5ea6d19b803d78af70e7e6f9"
        }
      },
      {
        "id": 2667,
        "name": "FintruX Network",
        "symbol": "FTX",
        "slug": "fintrux-network",
        "is_active": 1,
        "first_historical_data": "2019-12-04T15:47:28.699Z",
        "last_historical_data": "2020-08-18T04:12:08.609Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd559f20296ff4895da39b5bd9add54b442596a61"
        }
      },
      {
        "id": 2669,
        "name": "MARK.SPACE",
        "symbol": "MRK",
        "slug": "mark-space",
        "is_active": 1,
        "first_historical_data": "2020-07-29T12:10:01.578Z",
        "last_historical_data": "2020-08-17T22:57:16.398Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf453b5b9d4e0b5c62ffb256bb2378cc2bc8e8a89"
        }
      },
      {
        "id": 2670,
        "name": "Pixie Coin",
        "symbol": "PXC",
        "slug": "pixie-coin",
        "is_active": 1,
        "first_historical_data": "2019-09-26T23:40:34.840Z",
        "last_historical_data": "2020-08-18T02:57:32.285Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc27c95350ecd634c80df89db0f10cd5c24b7b11f"
        }
      },
      {
        "id": 2672,
        "name": "SRCOIN",
        "symbol": "SRCOIN",
        "slug": "srcoin",
        "is_active": 1,
        "first_historical_data": "2020-06-08T13:08:54.937Z",
        "last_historical_data": "2020-08-18T01:12:34.793Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xef8cf79c21637fc6f951bcac348915508a639a41"
        }
      },
      {
        "id": 2673,
        "name": "WeOwn",
        "symbol": "CHX",
        "slug": "we-own",
        "is_active": 1,
        "first_historical_data": "2020-01-19T14:16:58.366Z",
        "last_historical_data": "2020-08-18T14:41:55.368Z",
        "platform": null
      },
      {
        "id": 2674,
        "name": "Masari",
        "symbol": "MSR",
        "slug": "masari",
        "is_active": 1,
        "first_historical_data": "2019-12-08T23:45:07.348Z",
        "last_historical_data": "2020-08-18T04:06:20.088Z",
        "platform": null
      },
      {
        "id": 2675,
        "name": "Dock",
        "symbol": "DOCK",
        "slug": "dock",
        "is_active": 1,
        "first_historical_data": "2019-10-23T21:47:30.065Z",
        "last_historical_data": "2020-08-18T06:21:52.945Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe5dada80aa6477e85d09747f2842f7993d0df71c"
        }
      },
      {
        "id": 2676,
        "name": "PHI Token",
        "symbol": "PHI",
        "slug": "phi-token",
        "is_active": 1,
        "first_historical_data": "2020-08-05T05:45:54.296Z",
        "last_historical_data": "2020-08-18T07:36:33.699Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x13c2fab6354d3790d8ece4f0f1a3280b4a25ad96"
        }
      },
      {
        "id": 2678,
        "name": "TraDove B2BCoin",
        "symbol": "BBC",
        "slug": "b2bcoin",
        "is_active": 1,
        "first_historical_data": "2019-09-15T21:58:45.324Z",
        "last_historical_data": "2020-08-18T08:56:57.250Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe7d3e4413e29ae35b0893140f4500965c74365e5"
        }
      },
      {
        "id": 2679,
        "name": "Decentralized Machine Learning",
        "symbol": "DML",
        "slug": "decentralized-machine-learning",
        "is_active": 1,
        "first_historical_data": "2020-03-15T12:32:46.426Z",
        "last_historical_data": "2020-08-18T02:00:03.951Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xbcdfe338d55c061c084d81fd793ded00a27f226d"
        }
      },
      {
        "id": 2681,
        "name": "Origami",
        "symbol": "ORI",
        "slug": "origami",
        "is_active": 1,
        "first_historical_data": "2019-08-29T19:35:12.943Z",
        "last_historical_data": "2020-08-18T03:24:27.494Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd2fa8f92ea72abb35dbd6deca57173d22db2ba49"
        }
      },
      {
        "id": 2682,
        "name": "Holo",
        "symbol": "HOT",
        "slug": "holo",
        "is_active": 1,
        "first_historical_data": "2020-01-20T07:20:46.099Z",
        "last_historical_data": "2020-08-18T13:07:29.247Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6c6ee5e31d828de241282b9606c8e98ea48526e2"
        }
      },
      {
        "id": 2685,
        "name": "Zebi Token",
        "symbol": "ZEBI",
        "slug": "zebi-token",
        "is_active": 1,
        "first_historical_data": "2019-10-11T00:13:56.221Z",
        "last_historical_data": "2020-08-18T12:44:05.916Z",
        "platform": null
      },
      {
        "id": 2686,
        "name": "Lendingblock",
        "symbol": "LND",
        "slug": "lendingblock",
        "is_active": 1,
        "first_historical_data": "2019-08-29T19:16:56.333Z",
        "last_historical_data": "2020-08-18T11:58:18.461Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0947b0e6d821378805c9598291385ce7c791a6b2"
        }
      },
      {
        "id": 2687,
        "name": "Proxeus",
        "symbol": "XES",
        "slug": "proxeus",
        "is_active": 1,
        "first_historical_data": "2019-10-14T11:15:52.372Z",
        "last_historical_data": "2020-08-18T02:38:43.128Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xa017ac5fac5941f95010b12570b812c974469c2c"
        }
      },
      {
        "id": 2688,
        "name": "Vipstar Coin",
        "symbol": "VIPS",
        "slug": "vipstar-coin",
        "is_active": 1,
        "first_historical_data": "2020-02-15T15:22:59.291Z",
        "last_historical_data": "2020-08-17T22:23:41.946Z",
        "platform": null
      },
      {
        "id": 2689,
        "name": "Rublix",
        "symbol": "RBLX",
        "slug": "rublix",
        "is_active": 1,
        "first_historical_data": "2020-03-05T03:09:58.513Z",
        "last_historical_data": "2020-08-17T19:53:29.729Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xFc2C4D8f95002C14eD0a7aA65102Cac9e5953b5E"
        }
      },
      {
        "id": 2690,
        "name": "Biotron",
        "symbol": "BTRN",
        "slug": "biotron",
        "is_active": 1,
        "first_historical_data": "2020-04-30T13:46:13.491Z",
        "last_historical_data": "2020-08-18T11:12:13.311Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x03c780cd554598592b97b7256ddaad759945b125"
        }
      },
      {
        "id": 2691,
        "name": "Penta",
        "symbol": "PNT",
        "slug": "penta",
        "is_active": 1,
        "first_historical_data": "2020-04-17T18:56:43.344Z",
        "last_historical_data": "2020-08-18T12:20:22.566Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x53066cddbc0099eb6c96785d9b3df2aaeede5da3"
        }
      },
      {
        "id": 2692,
        "name": "Nebula AI",
        "symbol": "NBAI",
        "slug": "nebula-ai",
        "is_active": 1,
        "first_historical_data": "2019-09-30T02:41:53.255Z",
        "last_historical_data": "2020-08-17T21:23:11.823Z",
        "platform": null
      },
      {
        "id": 2693,
        "name": "Loopring [NEO]",
        "symbol": "LRN",
        "slug": "loopring-neo",
        "is_active": 1,
        "first_historical_data": "2020-06-09T23:51:50.277Z",
        "last_historical_data": "2020-08-18T05:36:05.022Z",
        "platform": {
          "id": 1376,
          "name": "Neo",
          "symbol": "NEO",
          "slug": "neo",
          "token_address": "06fa8be9b6609d963e8fc63977b9f8dc5f10895f"
        }
      },
      {
        "id": 2694,
        "name": "Nexo",
        "symbol": "NEXO",
        "slug": "nexo",
        "is_active": 1,
        "first_historical_data": "2019-10-25T12:12:52.672Z",
        "last_historical_data": "2020-08-18T00:25:35.151Z",
        "platform": {
          "id": 1839,
          "name": "Binance Coin",
          "symbol": "BNB",
          "slug": "binance-coin",
          "token_address": "NEXO-A84"
        }
      },
      {
        "id": 2695,
        "name": "TrueVett",
        "symbol": "VME",
        "slug": "truevett",
        "is_active": 1,
        "first_historical_data": "2020-01-16T01:44:21.043Z",
        "last_historical_data": "2020-08-18T15:14:53.660Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xc343f099d3e41aa5c1b59470450e21e92e2d840b"
        }
      },
      {
        "id": 2696,
        "name": "DAEX",
        "symbol": "DAX",
        "slug": "daex",
        "is_active": 1,
        "first_historical_data": "2020-08-11T11:15:46.650Z",
        "last_historical_data": "2020-08-17T22:55:07.849Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x0b4bdc478791897274652dc15ef5c135cae61e60"
        }
      },
      {
        "id": 2698,
        "name": "Hydro",
        "symbol": "HYDRO",
        "slug": "hydrogen",
        "is_active": 1,
        "first_historical_data": "2019-12-23T01:56:18.501Z",
        "last_historical_data": "2020-08-18T09:04:08.789Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xebbdf302c940c6bfd49c6b165f457fdb324649bc"
        }
      },
      {
        "id": 2699,
        "name": "Sharder",
        "symbol": "SS",
        "slug": "sharder",
        "is_active": 1,
        "first_historical_data": "2019-10-28T12:45:04.935Z",
        "last_historical_data": "2020-08-18T01:00:46.595Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xbbff862d906e348e9946bfb2132ecb157da3d4b4"
        }
      },
      {
        "id": 2700,
        "name": "Celsius",
        "symbol": "CEL",
        "slug": "celsius",
        "is_active": 1,
        "first_historical_data": "2019-12-26T04:26:30.712Z",
        "last_historical_data": "2020-08-18T03:35:47.942Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xaaaebe6fe48e54f431b0c390cfaf0b017d09d42d"
        }
      },
      {
        "id": 2702,
        "name": "Bitcoin Interest",
        "symbol": "BCI",
        "slug": "bitcoin-interest",
        "is_active": 1,
        "first_historical_data": "2020-02-16T09:07:03.015Z",
        "last_historical_data": "2020-08-17T19:24:58.957Z",
        "platform": null
      },
      {
        "id": 2703,
        "name": "BetterBetting",
        "symbol": "BETR",
        "slug": "betterbetting",
        "is_active": 1,
        "first_historical_data": "2019-11-18T10:35:54.890Z",
        "last_historical_data": "2020-08-18T09:44:28.556Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x763186eb8d4856d536ed4478302971214febc6a9"
        }
      },
      {
        "id": 2704,
        "name": "Transcodium",
        "symbol": "TNS",
        "slug": "transcodium",
        "is_active": 1,
        "first_historical_data": "2020-05-27T22:56:56.754Z",
        "last_historical_data": "2020-08-18T08:50:11.033Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xb0280743b44bf7db4b6be482b2ba7b75e5da096c"
        }
      },
      {
        "id": 2705,
        "name": "Amon",
        "symbol": "AMN",
        "slug": "amon",
        "is_active": 1,
        "first_historical_data": "2019-10-01T04:33:54.042Z",
        "last_historical_data": "2020-08-18T15:29:17.000Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x737f98ac8ca59f2c68ad658e3c3d8c8963e40a4c"
        }
      },
      {
        "id": 2707,
        "name": "FLIP",
        "symbol": "FLP",
        "slug": "flip",
        "is_active": 1,
        "first_historical_data": "2019-11-29T05:29:58.378Z",
        "last_historical_data": "2020-08-17T23:11:47.593Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x3a1bda28adb5b0a812a7cf10a1950c920f79bcd3"
        }
      },
      {
        "id": 2708,
        "name": "Crowd Machine",
        "symbol": "CMCT",
        "slug": "crowd-machine",
        "is_active": 1,
        "first_historical_data": "2019-10-10T13:11:47.403Z",
        "last_historical_data": "2020-08-18T13:56:39.035Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x47bc01597798dcd7506dcca36ac4302fc93a8cfb"
        }
      },
      {
        "id": 2709,
        "name": "Morpheus Labs",
        "symbol": "MITX",
        "slug": "morpheus-labs",
        "is_active": 1,
        "first_historical_data": "2019-11-07T07:11:17.062Z",
        "last_historical_data": "2020-08-18T14:20:44.183Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4a527d8fc13c5203ab24ba0944f4cb14658d1db6"
        }
      },
      {
        "id": 2711,
        "name": "DOC.COM",
        "symbol": "MTC",
        "slug": "doc-com",
        "is_active": 1,
        "first_historical_data": "2019-11-12T22:15:22.600Z",
        "last_historical_data": "2020-08-18T17:05:55.204Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x905e337c6c8645263d3521205aa37bf4d034e745"
        }
      },
      {
        "id": 2712,
        "name": "MyToken",
        "symbol": "MT",
        "slug": "mytoken",
        "is_active": 1,
        "first_historical_data": "2020-06-28T21:04:04.055Z",
        "last_historical_data": "2020-08-18T07:05:13.283Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x9b4e2b4b13d125238aa0480dd42b4f6fc71b37cc"
        }
      },
      {
        "id": 2713,
        "name": "KEY",
        "symbol": "KEY",
        "slug": "key",
        "is_active": 1,
        "first_historical_data": "2020-05-01T12:46:57.517Z",
        "last_historical_data": "2020-08-18T08:35:25.200Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4cd988afbad37289baaf53c13e98e2bd46aaea8c"
        }
      },
      {
        "id": 2714,
        "name": "Nexty",
        "symbol": "NTY",
        "slug": "nexty",
        "is_active": 1,
        "first_historical_data": "2020-01-11T18:51:54.253Z",
        "last_historical_data": "2020-08-18T03:06:47.069Z",
        "platform": null
      },
      {
        "id": 2715,
        "name": "ConnectJob",
        "symbol": "CJT",
        "slug": "connectjob",
        "is_active": 1,
        "first_historical_data": "2020-06-21T12:17:58.640Z",
        "last_historical_data": "2020-08-18T00:04:20.891Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x3abdff32f76b42e7635bdb7e425f0231a5f3ab17"
        }
      },
      {
        "id": 2717,
        "name": "BoutsPro",
        "symbol": "BOUTS",
        "slug": "boutspro",
        "is_active": 1,
        "first_historical_data": "2019-10-28T10:07:10.936Z",
        "last_historical_data": "2020-08-18T15:51:08.078Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x139d9397274bb9e2c29a9aa8aa0b5874d30d62e3"
        }
      },
      {
        "id": 2718,
        "name": "PAL Network",
        "symbol": "PAL",
        "slug": "pal-network",
        "is_active": 1,
        "first_historical_data": "2020-07-17T09:41:12.291Z",
        "last_historical_data": "2020-08-18T05:47:04.453Z",
        "platform": null
      },
      {
        "id": 2719,
        "name": "Cybereits",
        "symbol": "CRE",
        "slug": "cybereits",
        "is_active": 1,
        "first_historical_data": "2020-04-14T23:03:23.851Z",
        "last_historical_data": "2020-08-18T15:42:21.697Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x61f33da40594cec1e3dc900faf99f861d01e2e7d"
        }
      },
      {
        "id": 2720,
        "name": "Parkgene",
        "symbol": "GENE",
        "slug": "parkgene",
        "is_active": 1,
        "first_historical_data": "2020-07-11T23:23:45.017Z",
        "last_historical_data": "2020-08-18T12:00:32.919Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x6dd4e4aad29a40edd6a409b9c1625186c9855b4d"
        }
      },
      {
        "id": 2721,
        "name": "APR Coin",
        "symbol": "APR",
        "slug": "apr-coin",
        "is_active": 1,
        "first_historical_data": "2020-05-20T12:49:09.213Z",
        "last_historical_data": "2020-08-18T07:41:52.103Z",
        "platform": null
      },
      {
        "id": 2722,
        "name": "AC3",
        "symbol": "AC3",
        "slug": "ac3",
        "is_active": 1,
        "first_historical_data": "2019-10-10T08:13:51.496Z",
        "last_historical_data": "2020-08-17T19:18:40.004Z",
        "platform": {
          "id": 2099,
          "name": "ICON",
          "symbol": "ICX",
          "slug": "icon",
          "token_address": ""
        }
      },
      {
        "id": 2723,
        "name": "FuzeX",
        "symbol": "FXT",
        "slug": "fuzex",
        "is_active": 1,
        "first_historical_data": "2020-06-03T04:57:49.137Z",
        "last_historical_data": "2020-08-18T00:46:14.260Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1829aa045e21e0d59580024a951db48096e01782"
        }
      },
      {
        "id": 2724,
        "name": "Zippie",
        "symbol": "ZIPT",
        "slug": "zippie",
        "is_active": 1,
        "first_historical_data": "2020-01-08T16:58:35.776Z",
        "last_historical_data": "2020-08-18T14:59:54.960Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xedd7c94fd7b4971b916d15067bc454b9e1bad980"
        }
      },
      {
        "id": 2725,
        "name": "Skrumble Network",
        "symbol": "SKM",
        "slug": "skrumble-network",
        "is_active": 1,
        "first_historical_data": "2019-08-26T20:42:40.421Z",
        "last_historical_data": "2020-08-18T04:03:33.123Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xd99b8a7fa48e25cce83b81812220a3e03bf64e5f"
        }
      },
      {
        "id": 2726,
        "name": "DAOstack",
        "symbol": "GEN",
        "slug": "daostack",
        "is_active": 1,
        "first_historical_data": "2020-02-06T20:28:38.239Z",
        "last_historical_data": "2020-08-17T21:58:02.923Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x543ff227f64aa17ea132bf9886cab5db55dcaddf"
        }
      },
      {
        "id": 2727,
        "name": "Bezant",
        "symbol": "BZNT",
        "slug": "bezant",
        "is_active": 1,
        "first_historical_data": "2019-09-16T01:47:46.814Z",
        "last_historical_data": "2020-08-18T07:27:52.131Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xe1aee98495365fc179699c1bb3e761fa716bee62"
        }
      },
      {
        "id": 2728,
        "name": "Winding Tree",
        "symbol": "LIF",
        "slug": "winding-tree",
        "is_active": 1,
        "first_historical_data": "2019-10-13T04:00:10.549Z",
        "last_historical_data": "2020-08-18T16:37:43.287Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xeb9951021698b42e4399f9cbb6267aa35f82d59d"
        }
      },
      {
        "id": 2729,
        "name": "TEAM (TokenStars)",
        "symbol": "TEAM",
        "slug": "tokenstars",
        "is_active": 1,
        "first_historical_data": "2019-08-23T07:50:57.720Z",
        "last_historical_data": "2020-08-18T03:15:43.451Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1c79ab32c66acaa1e9e81952b8aaa581b43e54e7"
        }
      },
      {
        "id": 2731,
        "name": "Utrum",
        "symbol": "OOT",
        "slug": "utrum",
        "is_active": 1,
        "first_historical_data": "2020-03-31T05:42:50.672Z",
        "last_historical_data": "2020-08-17T19:56:20.717Z",
        "platform": null
      },
      {
        "id": 2732,
        "name": "Aston",
        "symbol": "ATX",
        "slug": "aston",
        "is_active": 1,
        "first_historical_data": "2020-04-24T09:08:10.758Z",
        "last_historical_data": "2020-08-18T06:19:50.143Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x1a0f2ab46ec630f9fd638029027b552afa64b94c"
        }
      },
      {
        "id": 2733,
        "name": "Freyrchain",
        "symbol": "FREC",
        "slug": "freyrchain",
        "is_active": 1,
        "first_historical_data": "2020-06-01T21:25:00.671Z",
        "last_historical_data": "2020-08-18T16:32:34.075Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x17e67d1cb4e349b9ca4bc3e17c7df2a397a7bb64"
        }
      },
      {
        "id": 2734,
        "name": "EduCoin",
        "symbol": "EDU",
        "slug": "edu-coin",
        "is_active": 1,
        "first_historical_data": "2020-01-29T03:29:16.706Z",
        "last_historical_data": "2020-08-17T21:23:08.655Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0xf263292e14d9d8ecd55b58dad1f1df825a874b7c"
        }
      },
      {
        "id": 2735,
        "name": "Content Neutrality Network",
        "symbol": "CNN",
        "slug": "content-neutrality-network",
        "is_active": 1,
        "first_historical_data": "2019-09-12T07:50:04.433Z",
        "last_historical_data": "2020-08-18T15:07:17.118Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x8713d26637cf49e1b6b4a7ce57106aabc9325343"
        }
      },
      {
        "id": 2737,
        "name": "Global Social Chain",
        "symbol": "GSC",
        "slug": "global-social-chain",
        "is_active": 1,
        "first_historical_data": "2019-09-10T19:48:40.648Z",
        "last_historical_data": "2020-08-18T12:31:49.576Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x228ba514309ffdf03a81a205a6d040e429d6e80c"
        }
      },
      {
        "id": 2739,
        "name": "Digix Gold Token",
        "symbol": "DGX",
        "slug": "digix-gold-token",
        "is_active": 1,
        "first_historical_data": "2020-04-22T00:59:46.757Z",
        "last_historical_data": "2020-08-18T01:23:45.335Z",
        "platform": {
          "id": 1027,
          "name": "Ethereum",
          "symbol": "ETH",
          "slug": "ethereum",
          "token_address": "0x4f3afec4e5a3f2a6a1a411def7d7dfe50ee057bf"
        }
      }
    ]
  };

  export default mockMapResponse;